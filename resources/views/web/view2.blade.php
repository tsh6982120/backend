@extends('web.layout')
@section('style')
    <link rel="stylesheet" href="{{asset('css/web/kq2.css')}}">
    <link href="{{asset('assets-kq1/font-awesome-all.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-kq1/util.css')}}">
    <link href="{{asset('assets-kq1/search_style_v12.css')}}" rel="stylesheet">
    {{--    <script async="" src="{{asset('assets-kq1/fbevents.js')}}"></script>--}}
    {{--    <script src="{{asset('assets-kq1/sdk.js')}}" async="" crossorigin="anonymous"></script>--}}
    <script src="{{asset('assets-kq1/jQuery3-1-1.min.js')}}"></script>
    <style>
        canvas {
            margin: 0 auto;
        }

        tr:hover td {
            background: none;
        }

        td.bd {
            width: 50px;
            height: 40px;
            font-size: 13px;
        }

        table{
            max-width: 300px;
        }

        table.result {
            border-collapse: collapse;
            font-size: 13px;
            margin: auto;
            font-weight: bold;
        }

        table.result td.tt {
            border: 1px solid #0c5460;
            width: 200px;
            height: 40px;
            font-size: 12px;
        }

        table.result td.tn {
            border: 1px solid #0c5460;
            width: 60px;
            height: 40px;
            font-size: 13px;
        }

        table .red {
            color: red;
        }

        td.border {
            border: 1px solid #0c5460;
        }

        td.border-t {
            border-top: 1px solid #0c5460;
        }

        td.border-r {
            border-right: 1px solid #0c5460;
        }

        td.border-b {
            border-bottom: 1px solid #0c5460;
        }

        td.border-l {
            border-left: 1px solid #0c5460;
        }
    </style>


@endsection
@section('content')
    <main style="min-height: 100vh; padding-top: 100px;">
        <div class="tsh-kq-2-container" style="margin-bottom: 100px;">
            <section class="content-wrapper" unselectable="on">



                <div id="result-content"><div class="content-container greeting-lifepath-container animated fadeIn anim active m-t-20" style="animation-play-state: paused; display: block;">
                        <p class="content-title fadeIn" style="opacity: 1;"><span style="opacity: 1;">BÁO CÁO THẦN SỐ HỌC</span></p>
                        <p class="content-subtitle fadeIn" style="opacity: 1;"><span style="opacity: 1;"><b style="color: rgb(247, 213, 26);">{{$view->name}}</b><br><b style="color: rgb(247, 213, 26);">{{$view->birthday}}</b></span></p>
                    </div><div class="initial-carousel-container content-container carousel-container" style="display: none">
                        <div class="signs-carousel">
                            <div class="sign-container">
                                <p class="sign-bg"></p>
                                <p class="sign-image"></p>
                                <p class="sign-title"></p>
                            </div>
                        </div>
                    </div><div class="content-container p-10" id="content-more" style=""><div class="container" style="padding: 0px 5px;"><div class="p10 text-white">Bạn có thể chia sẻ cho mọi người về bản báo cáo này bằng link sau: <br><input value="https://xem.tracuuthansohoc.com/?view=1568843ydqm9GHdk" style="padding: 0px 3px;width: 240px;border-radius: 4px;outline: none;box-shadow: none;border: none;"> <button class="btn btn-primary btn-copy" style="margin-left:5px;padding:1px 2px;vertical-align: middle;font-size: 12px;" data-clipboard-text="https://xem.tracuuthansohoc.com/?view=1568843ydqm9GHdk">Copy</button></div><div class="text-center m-t-10" style="background: #f6f6f6;padding: 10px;border-radius: 10px;">
                                <p class="text-success m-b-8 font-weight-bold">Bạn đang sử dụng lượt tra VIP. Nội dung luận giải phía dưới rất dài, bạn có thể tải về dưới dạng file để in ra giấy hoặc để đọc lại sau này!</p>
                                <div class="m-b-8"><button class="btn btn-success pdfDownload" code="1568843ydqm9GHdk"><i class="fas fa-download"></i> Tải file để lưu lại</button></div>
                                <div class="m-b-8 fs-10 font-italic">File báo cáo có dung lượng tương đối lớn và bạn cần chút thời gian để tải xuống</div>
{{--                                <p class="text-left m-b-8">Ngoài Thần số học, bạn có thể tra cứu Lá Số Tử Vi của bạn tại <a href="https://xem.tracuutuvi.com/?from=1" target="_blank">tracuutuvi.com</a>.</p>--}}
                            </div><div class="text-left m-t-10 box-bg" id="chukyvanso"><p class="font-weight-bold m-b-1" style="font-size:15px">1. {{getIndicatorName(array_key_first($contents['lifeCycleNumberContent']))??''}}</p>

                                <p id="chukyvanso_des_1" class="font-italic m-b-8 m-t-1 description truncate" style="display: inline-block;">Biểu đồ này cho biết bạn đang ở đâu trong chu kỳ vận số của mình. Theo thần số học, chu kỳ phát triển của đời người sẽ lặp lại mỗi 9 năm. Với mỗi năm có số cá nhân là 1, cuộc đời lại bắt đầu một chu kỳ mới với xuất phát cao hơn chu kỳ trước. Ảnh hưởng của biểu đồ này sẽ thể hiện mạnh nhất trong giai đoạn từ mốc đỉnh cao đầu tiên đến mốc đỉnh cao cuối cùng (Xem ở mục <a href="#kimtuthap" class="text-primary">Kim tự tháp thần số</a>).<br> Đoạn biểu đồ đi lên cho thấy giai đoạn cuộc sống sẽ có nhiều cơ hội, thay đổi hoặc phát triển từ bên ngoài.<br> Đoạn biểu đồ đi xuống cho thấy giai đoạn cuộc sống có nhiều thay đổi bên trong bản thân, bạn nên dành thời gian phát triển nội tâm, trí tuệ của mình hơn là tập trung vào những thứ bên ngoài trong giai đoạn này.</p>
                                <em id="chukyvanso_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>



                                <div style="background: #fff;padding:13px 5px 8px 5px;border-radius: 8px;">
                                    <div class="m-t-5 m-b-2 text-center">
                                        <div class="m-auto"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                            <canvas id="pychart"
                                                    style="min-height: 200px; max-width: 500px; display: block; height: 200px; width: 100%;"
                                                    width="1000" height="400" class="chartjs-render-monitor"></canvas>
                                            <b style="color: #4d4d4d; font-size: 12px;">{{$contents['lifeCycleNumberContent']->name??''}}</b>
                                        </div>
                                    </div><p class="m-t-10 text-left content font-italic">Bạn thân mến, năm 2024 có vận niên thế giới số 8 gắn liền với tiền bạc, quyền lực và ảnh hướng rất lớn toàn bộ nhân loại. Theo Thần số học, năm nay được dự báo có những dịch chuyển, biến động lớn về tài sản của con người và những thay đổi liên quan tới chính trị, quản trị, quản lý, chuyển đổi quyền lực. Về mặt tích cực, thiên thời trong năm này tạo điều kiện cho con người có nhiều cơ hội để nâng cao vị trí trong xã hội hoặc tài sản sở hữu. Về mặt trái, những hậu quả sinh ra từ các hành vi tiêu cực (nếu có) của chúng ta trước kia bắt đầu được thể hiện rõ nét, và nhân loại có xu hướng rơi vào vòng xoáy của tiền bạc và quyền lực. Chúng tôi đã nghiên cứu các chỉ số và biểu đồ thần số học của bạn để đưa ra các gợi ý về năm 2024 dành riêng cho bạn.</p>

                                    <p class="mt-2 text-left"><button id="chukyvanso_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>

                                    <p id="chukyvanso_des_2" class="content" style="display: none;"><b>Theo thần số học vào năm 2024, chỉ số vận niên thế giới là số 8 ảnh hưởng tới toàn bộ nhân loại:</b><br>Theo Thần số học, năm nay được dự báo có những dịch chuyển, biến động lớn về tài sản của con người và những thay đổi liên quan tới chính trị, quản trị, quản lý, chuyển đổi quyền lực.
                                        Về mặt tích cực, thiên thời trong năm này tạo điều kiện cho con người có nhiều cơ hội để nâng cao vị trí trong xã hội hoặc tài sản sở hữu.

                                        Những biến động về vấn đề kinh tế trong năm sẽ tạo ra nhiều cơ hội tốt cho con người để gặt hái thành tựu - những cá nhân, tổ chức đã xây dựng nền tảng sự nghiệp từ trước đó một cách chính đáng, bài bản, đúng pháp luật và đạo đức sẽ đón nhận mạnh mẽ cơ hội tăng trưởng, thành quả kinh doanh. Vì vậy, con người cần phải chuẩn bị càng sớm càng tốt cho những điều kiện đến bất ngờ. Nỗ lực làm việc chăm chỉ, hướng mục tiêu cùng các quyết định tài chính cần được đưa ra một cách thông minh sẽ là lời khuyên cho năm nay.

                                        Năm số 8 cũng gắn liền với quyền lực, công lý và pháp luật. Vì vậy, điều này cho thấy sự tập trung quan trọng vào các vấn đề pháp lý và cải cách pháp luật. Luật pháp được cơ cấu lại và các quy định trở nên chặt chẽ hơn. Chính trị sẽ có sự siết chặt về quản lý nhà nước và củng cố, phân cực quyền lực tại các quốc gia. Song song với đó, nhiều chính sách gỡ vướng cũng sẽ được đưa ra.

                                        Về lĩnh vực tài chính, năm nay là một năm hệ thống ngân hàng tại các quốc gia thực hiện nhiều quyết định xoay chuyển tiền tệ, dòng vốn trong ngân hàng trở nên dồi dào hơn nhiều so với trước. Ngoài ra, sự biến động của các vấn đề liên quan sẽ tạo tới nhiều cơ hội ở thị trường vốn (cổ phiếu, trái phiếu,...).

                                        Năm nay sẽ được đánh dấu bằng sự trưởng thành của công nghệ, đặc biệt vẫn là trí tuệ nhân tạo (AI). Nó sẽ tạo ra sự đột biến lớn trong nhiều lĩnh vực khác nhau và mở ra những phương pháp lao động hiệu suất cao.

                                        Về mặt trái, những hậu quả sinh ra từ các hành vi tiêu cực (nếu có) của chúng ta trước kia bắt đầu được thể hiện rõ nét, và nhân loại có xu hướng rơi vào vòng xoáy của tiền bạc và quyền lực.
                                        Việc tập trung cao độ vào thành tích và thành công tài chính có thể khiến nhiều người phải trả giá bằng hạnh phúc, các mối quan hệ hoặc những bài học về đạo đức. Cân bằng các mục tiêu vật chất với các khía cạnh tinh thần là rất quan trọng trong năm số 8.

                                        Một thách thức khác liên quan đến năm số 8 là khả năng xảy ra những cách tiếp cận độc đoán hoặc bất chấp để kiếm tiền hoặc gia tăng quyền lực. Mong muốn địa vị và quyền lực, nếu không được kiềm chế bằng sự bình đẳng, hợp tác và tôn trọng có thể dẫn đến xung đột. Con người có thể cần phải thận trọng trong việc duy trì sự cân bằng lành mạnh giữa việc khẳng định bản thân và tôn trọng quan điểm của người khác.

                                        Tóm lại, 2024 là một năm mang năng lượng đặc biệt gắn liền với thành tựu, tăng trưởng, tiền bạc, quyền lực và ảnh hưởng rất lớn toàn bộ nhân loại. Thiên thời trong năm này tạo điều kiện cho con người đón nhận những nhân quả đã gieo từ trước. Nếu trước đó gieo nhân tốt, năm nay thiên thời sẽ giúp chúng ta gặt hái thành quả, và nếu ngược lại, thiên thời sẽ mang đến chúng ta những hậu quả.<br><br><br><b>Bạn mang số chủ đạo là 22, đối với vận niên thế giới là 8 trong năm 2024 cần chú ý như sau:</b><br><br><em class="font-weight-bold">Xu hướng</em><br>Đây là năm bạn có tiềm năng vô cùng lớn để đạt được hầu hết những mục đích dường như bất khả thi.
                                        Năng lượng cộng hưởng của năm thế giới số 8 và số chủ đạo 22 giúp bạn có sự quyết đoán, độc lập, tự tin hơn để bắt tay vào thực hiện những kế hoạch, dự định của bản thân mà những năm trước đó chưa thực hiện được. Bạn có tầm nhìn, nhạy bén với các cơ hội, do đó thành công đến với bạn trong giai đoạn này có thể là về lĩnh vực tài chính, công việc, sự nghiệp.
                                        Sự chắc chắn trong mọi việc của số chủ đạo 22, cùng với khả năng cân đo lợi ích, hướng mục tiêu của số 8 sẽ tạo nên sự vững vàng và chắc chắn trong mọi quyết định bạn đưa ra.
                                        Số chủ đạo 22 trong năm thế giới số 8 sẽ có xu hướng năng động, hoạt bát, học hỏi nhanh và biết nắm bắt các cơ hội một cách hợp lý.
                                        Đây là giai đoạn bạn tập trung nhiều vào công việc, sự nghiệp, do đó bạn có thể ít có thời gian quan tâm đến sức khỏe của bản thân, khiến cơ thể mệt mỏi, dễ suy nhược.<br><br><em class="font-weight-bold">Lời khuyên</em><br>Bạn cần cải thiện khả năng tin tưởng vào năng lực cũng như là tin tưởng vào khả năng của người khác để không trở nên quá kiểm soát.
                                        Bạn cần chú ý hơn đến chế độ dinh dưỡng, tập thể dục thể thao thường xuyên để cân bằng sức khỏe tinh thần, thể chất.
                                        Bạn nên dành nhiều thời gian hơn cho những người xung quanh, đặc biệt là người thân yêu trong gia đình. Gia đình hạnh phúc cũng sẽ giúp bạn vững tâm xây dựng sự nghiệp.
                                        Một điều nữa bạn cũng cần lưu tâm đó là hạn chế việc suy nghĩ quá nhiều dẫn đến lo lắng, bất an và trở nên dễ từ bỏ ước mơ, hy vọng của mình.<br><br><br><b>Năm 2024 cũng là năm bạn có chỉ số năm (vận niên cá nhân) là 4, đối với vận niên thế giới là 8 trong năm 2024 cần chú ý như sau:</b><br><br><em class="font-weight-bold">Năng lượng của năm thế giới số 8 và năm cá nhân số 4 có sự cộng hưởng</em><br>Năm cá nhân số 4 đôi khi khiến bạn quá cẩn thận, thích sự ổn định, an toàn; năm thế giới số 8 thúc đẩy bạn dám nghĩ dám làm, biết nhìn ra những cơ hội không phải ai cũng thấy. Do đó, năm cá nhân số 4 trong năm thế giới số 8 sẽ đem đến cho bạn nhiều cơ hội phát triển bản thân hơn, nắm bắt được những cơ hội mới và có các trải nghiệm thú vị hơn.
                                        Cả hai nguồn năng lượng đều hướng bạn tập trung nhiều hơn vào công việc, mọi thứ cần tuân thủ quy trình, trật tự và nguyên tắc trong năm này. Đây là một năm khá nghiêm túc, đòi hỏi bạn phải làm việc chăm chỉ để đặt nền tảng cho bất cứ điều gì bạn đang làm.
                                        Sự chủ động, độc lập, hướng mục tiêu của số 8 thúc đẩy số 4 dễ dàng hơn trong việc bắt tay hành động, thực hiện công việc, ý tưởng, dự án đi vào quy củ.
                                        Năm thế giới số 8 đem đến cho bạn nhiều cơ hội để phát triển các khía cạnh tài chính, tiền bạc. Bạn cần hết sức nỗ lực, kiên trì theo đuổi chúng.
                                        Bạn có thể thấy bản thân dễ mất kết nối với mọi người do tập trung vào xây dựng sự nghiệp, vì vậy đây là lúc để bạn hướng đến trách nhiệm gia đình. Điều này có thể mang lại sự thay đổi lớn trong cuộc sống.
                                        Chìa khóa trong giai đoạn này để giúp bạn gặt hái được thành công, thành tựu là: 'Chắc chắn, ổn định, kiên trì'.<br><br><em class="font-weight-bold">Lời khuyên</em><br>Hãy điều chỉnh sức khỏe, chế độ ăn uống phù hợp, cân bằng đời sống nội tâm, tinh thần, cần có ý thức dành thời gian để chăm sóc bản thân. Hãy bắt đầu bằng những buổi mát-xa, trị liệu và nghỉ ngơi hợp lý.
                                        Hãy cân bằng giữa công việc và cuộc sống, cần có thời gian nghỉ ngơi hợp lý. Bạn có thể thử bộ môn yoga, thiền, học cắm hoa, nấu ăn,... để thư giãn đầu óc, tinh thần.
                                        Để gặt hái thành công trong năm này, bạn sẽ cần lên kế hoạch cụ thể, kiên trì thực hiện từng bước để đạt được mục tiêu.
                                        Dành thời gian làm việc chăm chỉ, nỗ lực và đẩy mạnh tính trách nhiệm cho những gì mình làm.<br></p></div></div><div class="text-left m-t-10 box-bg" id="tinhcach" style="font-size: 12px;">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">2. NHÓM TÍNH CÁCH THEO BẢN NGÃ CỦA BẠN (có thể thay đổi do luyện tập)</p>

                                <p id="tinhcach_des_1" class="font-italic m-b-8 m-t-2 description truncate" style="display: inline-block;">Đây là các nhóm tính cách có trong bản ngã (tính cách khi sinh ra đã có) của bạn. Bạn nên tập trung luyện tập những nhóm tính cách có % dao động thấp nhất. Dao động tính cách đẹp nhất khi các chỉ số % gần bằng nhau. Bạn hoàn toàn có thể luyện tập để cân bằng và kiểm soát các dao động tính cách của bạn.</p>
                                <em id="tinhcach_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>


                                <div style="background: #fff;padding:8px 5px;border-radius: 8px;">
                                    <p><b>1.1. Mạnh mẽ - Độc lập - Tự tin</b><br><span style="width:19%;" class="gen-1 loadLeftToRight">5%</span><br></p><p><b>1.2. Lắng nghe - Khéo léo - Nhạy cảm</b><br><span style="width:13%;" class="gen-2 loadLeftToRight">3%</span><br></p><p><b>1.3. Sáng tạo - Hoạt bát - Lạc quan</b><br><span style="width:25%;" class="gen-3 loadLeftToRight">7%</span><br></p><p><b>1.4. Cẩn thận - Cầu toàn - Thực tế</b><br><span style="width:67%;" class="gen-4 loadLeftToRight">21%</span><br></p><p><b>1.5. Năng động - Linh hoạt - Tò mò</b><br><span style="width:79%;" class="gen-5 loadLeftToRight">25%</span><br></p><p><b>1.6. Quan tâm - Yêu thương - Kiểm soát</b><br><span style="width:13%;" class="gen-6 loadLeftToRight">3%</span><br></p><p><b>1.7. Thông thái - Khám phá - Truyền đạt</b><br><span style="width:37%;" class="gen-7 loadLeftToRight">11%</span><br></p><p><b>1.8. Công bằng - Tập trung - Lý tưởng</b><br><span style="width:13%;" class="gen-8 loadLeftToRight">3%</span><br></p><p><b>1.9. Trách nhiệm - Rộng lượng - Hào phóng</b><br><span style="width:85%;" class="gen-9 loadLeftToRight">27%</span><br></p>
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="duongdoi">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">3. {{mb_strtoupper($contents['lifePathNumberContent']->name)??''}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">SỐ {{$contents['lifePathNumberContent']->content_number??''}} - {{$contents['lifePathNumberContent']->description??''}}</span></p>
                                <div id="duongdoi_des_1" class="m-b-8 m-t-1 description truncate" style="display: inline-block;">
                                    {!! $contents['lifePathNumberContent']->content ?? '' !!}
                                </div>
                                <em id="duongdoi_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>

                                <p class="mt-2 text-left"><button id="duongdoi_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>

                                <div id="duongdoi_des_2" class="content" style="display: none;">
                                    {!! $contents['personalityTraitsIndexContent']->content??'' !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="chukyduongdoi">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">4. @lang('CHU KỲ ĐƯỜNG ĐỜI')</p>

                                <p id="chukyduongdoi_des_1" class="font-italic m-b-8 m-t-1 description truncate" style="">Cuộc đời mỗi người thường sẽ trải qua ba giai đoạn tuyệt vời trên đường đời. Về mặt số học, ba thời kỳ này (Gieo hạt, Chín, Thu hoạch) và các đặc điểm chính của chúng được mô tả bằng các con số của chu kỳ đường đời.<br>Chu kỳ đầu tiên (chu kỳ gieo hạt) thể hiện giai đoạn đầu của cuộc sống. Nó được ví như sự lớn lên của hạt giống, là một giai đoạn mà bạn phải mò mẫm để tìm ra bản chất thật của mình; đồng thời, chúng ta đang cố gắng đối phó với những tác động mạnh mẽ xuất hiện trong môi trường sống của bạn, cha mẹ và các điều kiện kinh tế xã hội, gia đình, bạn bè. Tuy nhiên, trong thời thơ ấu, ảnh hưởng của chu kỳ này rất thấp mà nó trở nên dễ nhận thấy hơn chủ yếu từ cuối tuổi thiếu niên của bạn cho đến hết chu kỳ đầu tiên này.<br>Sự thay đổi từ Chu kỳ đầu tiên sang Chu kỳ thứ hai có thể gây ngạc nhiên vì đó là sự thay đổi đầu tiên trong cuộc đời.<br>Chu kỳ thứ hai (chu kỳ chín) có thể được so sánh với quá trình chín của trái cây. Chu kỳ này là giai đoạn giữa cuộc đời dẫn đến sự xuất hiện chậm chạp của các tài năng sáng tạo cá nhân. Vài năm đầu của chu kỳ này thể hiện một cuộc đấu tranh để tìm vị trí của bạn trên thế giới, giai đoạn còn lại của chu kỳ cho thấy bạn có mức độ làm chủ bản thân và ảnh hưởng lớn hơn đến môi trường xung quanh bạn. <br>Chu kỳ thứ hai sau đó nó bắt đầu mờ dần và ảnh hưởng của Chu kỳ thứ ba bắt đầu thể hiện. Trong quá trình chuyển đổi này, bạn có thể mong đợi một sự thay đổi lớn trong cuộc đời mình. <br>Chu kỳ thứ ba, hay chu kỳ cuối cùng, có thể được so sánh với vụ thu hoạch, đại diện cho sự phát triển nở hoa của con người bên trong chúng ta, bản chất thực sự của chúng ta cuối cùng đã thành hiện thực. Trong giai đoạn này, bạn có mức độ thể hiện bản thân và quyền lực lớn nhất. Kiến thức và kinh nghiệm tích lũy giúp bạn có nhiều khả năng hơn để đưa ra quyết định sáng suốt.</p>
                                <em id="chukyduongdoi_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>


                                <div style="background: #fff;padding:8px 5px;border-radius: 8px;overflow: hidden;">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="ckdd-img">
                                                <img src="{{asset('assets-kq1/lifecircle1.png')}}">
                                                <div class="ckdd_number ckdd-1">{{$contents['stagesOfLifeContent']['month']??''}}</div>
                                            </div>
                                            <div class="ckdd-title">
                                                <div class="ckdd-title-big ckdd-1">Chu kỳ 1 <br> GIEO HẠT</div>
                                                <div class="ckdd-title-small">Đầu đời - 32 tuổi ({{$contents['lifePathCycleContent']['one'][1]??''}})</div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="ckdd-img">
                                                <img src="{{asset('assets-kq1/lifecircle2.png')}}">
                                                <div class="ckdd_number ckdd-2">{{$contents['stagesOfLifeContent']['day']??''}}</div>
                                            </div>
                                            <div class="ckdd-title">
                                                <div class="ckdd-title-big ckdd-2">Chu kỳ 2 <br> CHÍN</div>
                                                <div class="ckdd-title-small">33 - 59 tuổi ({{$contents['lifePathCycleContent']['two'][0]??''}} - {{$contents['lifePathCycleContent']['two'][1]??''}})</div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="ckdd-img">
                                                <img src="{{asset('assets-kq1/lifecircle3.png')}}">
                                                <div class="ckdd_number ckdd-3">{{$contents['stagesOfLifeContent']['year']??''}}</div>
                                            </div>
                                            <div class="ckdd-title">
                                                <div class="ckdd-title-big ckdd-3">Chu kỳ 3 <br> THU HOẠCH</div>
                                                <div class="ckdd-title-small">60 tuổi về sau ({{$contents['lifePathCycleContent']['three'][0]??''}} trở đi)</div>
                                            </div>
                                        </div>
                                    </div>

                                    <p class="mt-2 text-left"><button id="chukyduongdoi_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>

                                    <div class="content" id="chukyduongdoi_des_2" style="display: none;">
                                    <p class="text-center">Nội dung chưa cập nhật</p>
                                    </div>
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="kimtuthap"><p class="font-weight-bold m-b-1 text-left" style="font-size:15px">5. @lang('KIM TỰ THÁP THẦN SỐ HỌC')</p>
                                <p id="kimtuthap_des_1" class="font-italic m-b-8 m-t-1 description truncate" style="">Kim tự tháp cho thấy 4 giai đoạn trong cuộc đời bạn sẽ tương ứng với đỉnh cao là số nào và thử thách là con số nào, tức là bạn nên tập trung phát triển số nào trong những năm này để đạt được nhiều thành công và hạnh phúc nhất. Từ tuổi đỉnh cao đầu tiên đến tuổi đỉnh cao cuối cùng (36 năm) chính là khoảng thời gian gặt hái nhiều thành công trong cuộc đời của bạn. <br>Mức độ thành tựu trong 4 giai đoạn được thể hiện khác nhau: Giai đoạn 1 - Đỉnh cao của tuổi trẻ, thành tựu thường lớn đối với bản thân và suy nghĩ của bạn.<br>Giai đoạn 2 - Đỉnh cao của kinh nghiệm, thành tựu thường lớn hơn giai đoạn 1 và có thể ảnh hưởng nhỏ tới xã hội.<br>Giai đoạn 3 và giai đoạn 4 - Đỉnh cao của sự trưởng thành, thành tựu có thể rất lớn và ảnh hưởng tới xã hội, môi trường xung quanh.<br>Tuy vậy, trong 4 giai đoạn này cũng sẽ có những thách thức cụ thể mà cuộc đời muốn bạn vượt qua - những con số thử thách sẽ nói lên điều đó.</p>
                                <em id="kimtuthap_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>


                                <div style="background: #fff;padding:8px 5px;border-radius: 8px;">
                                    <div class="diagonal" id="diagonal"><div class="diagonal-box" id="diagonal-box">
                                            @php
                                                $date_parts = explode("-", $view->birthday);
                                                $day = sprintf("%02d", $date_parts[0]);      // Ngày
                                                $month = sprintf("%02d", $date_parts[1]);    // Tháng
                                                $year = $date_parts[2];   // Năm
                                            @endphp
                                            <div class="diagonal-detail" id="diagonal-step-1" style="bottom: 183.571px; left: -1.42857px;">
                                                <b>Tháng {{$month}}</b>
                                            </div>
                                            <div class="diagonal-circle-small" id="diagonal-step-2" style="bottom: 215.571px; left: 15.5714px;">
                                                <b>{{$contents['stagesOfLifeContent']['month']??''}}</b>
                                            </div>
                                            <div class="diagonal-detail" id="diagonal-step-3" style="bottom: 180.571px; left: 175px;">
                                                <b>Ngày {{$day}}</b>
                                            </div>
                                            <div class="diagonal-circle-small" id="diagonal-step-4" style="bottom: 215.571px; left: 187px;">
                                                <b>{{$contents['stagesOfLifeContent']['day']??''}}</b>
                                            </div>
                                            <div class="diagonal-detail" id="diagonal-step-5" style="bottom: 183.571px; left: 346.429px;">
                                                <b>Năm {{$year}}</b>
                                            </div>
                                            <div class="diagonal-circle-small" id="diagonal-step-6" style="bottom: 215.571px; left: 358.429px;">
                                                <b>{{$contents['stagesOfLifeContent']['year']??''}}</b>
                                            </div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-7" style="bottom: 228.571px; left: 28.5714px; width: 114.286px; height: 68.5714px;"></div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-8" style="bottom: 228.571px; left: 142.857px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-9" style="bottom: 282.143px; left: 127.857px;">
                                                {{$contents['stagesOfLifeContent']['D1']['value']??0}}
                                            </div>
                                            <div class="diagonal-detail" id="diagonal-step-10" style="bottom: 252.143px; left: 112.857px;">
                                                <b>24-32 tuổi</b><br>({{$contents['stagesOfLifeContent']['D1']['range'][1]??''}})
                                            </div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-11" style="bottom: 228.571px; left: 200px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-12" style="bottom: 228.571px; left: 257.143px; width: 114.286px; height: 68.5714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-13" style="bottom: 282.143px; left: 242.143px;">
                                                {{$contents['stagesOfLifeContent']['D2']['value']??0}}
                                            </div>
                                            <div class="diagonal-detail" id="diagonal-step-14" style="bottom: 252.143px; left: 237.143px;">
                                                <b>33-41 tuổi</b><br>({{$contents['stagesOfLifeContent']['D2']['range'][1]??''}})
                                            </div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-15" style="bottom: 297.143px; left: 142.857px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-16" style="bottom: 297.143px; left: 200px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-17" style="bottom: 350.714px; left: 185px;">
                                                {{$contents['stagesOfLifeContent']['D3']['value']??0}}
                                            </div>
                                            <div class="diagonal-detail" id="diagonal-step-18" style="bottom: 320.714px; left: 175px;">
                                                <b>42-50 tuổi</b><br>({{$contents['stagesOfLifeContent']['D3']['range'][1]??''}})
                                            </div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-19" style="bottom: 228.571px; left: 28.5714px; width: 171.429px; height: 205.714px;"></div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-20" style="bottom: 228.571px; left: 200px; width: 171.429px; height: 205.714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-21" style="bottom: 419.286px; left: 185px;">
                                                {{$contents['stagesOfLifeContent']['D4']['value']??0}}
                                            </div>
                                            <div class="diagonal-detail" id="diagonal-step-22" style="bottom: 419.286px; left: 210px;">
                                                <b>51-59 tuổi</b><br>({{$contents['stagesOfLifeContent']['D4']['range'][1]??''}})
                                            </div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-23" style="bottom: 160px; left: 28.5714px; width: 114.286px; height: 68.5714px;"></div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-24" style="bottom: 160px; left: 142.857px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-25" style="bottom: 145px; left: 127.857px;">
                                                {{$contents['stagesOfLifeContent']['DD1']['value']??0}}
                                            </div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-26" style="bottom: 160px; left: 200px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-27" style="bottom: 160px; left: 257.143px; width: 114.286px; height: 68.5714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-28" style="bottom: 145px; left: 242.143px;">
                                                {{$contents['stagesOfLifeContent']['DD2']['value']??0}}
                                            </div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-29" style="bottom: 91.4286px; left: 142.857px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-30" style="bottom: 91.4286px; left: 200px; width: 57.1429px; height: 68.5714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-31" style="bottom: 76.4286px; left: 185px;">
                                                {{$contents['stagesOfLifeContent']['DD3']['value']??0}}
                                            </div>
                                            <div class="diagonal-line-top-right" id="diagonal-step-32" style="bottom: 22.8571px; left: 28.5714px; width: 171.429px; height: 205.714px;"></div>
                                            <div class="diagonal-line-bottom-right" id="diagonal-step-33" style="bottom: 22.8571px; left: 200px; width: 171.429px; height: 205.714px;"></div>
                                            <div class="diagonal-circle-big animated pulse infinite" id="diagonal-step-34" style="bottom: 7.85714px; left: 185px;">
                                                {{$contents['stagesOfLifeContent']['DD4']['value']??0}}
                                            </div>
                                        </div>
                                    </div>
                                    <p class="font-weight-bold m-b-10 text-center" style="font-size:14px; margin-top:15px;">Kim tự tháp thần số học của bạn</p>

                                    <p class="mt-2 text-left"><button id="kimtuthap_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>

                                    <div id="kimtuthap_des_2"  class="content m-b-0" style="display: none;">
                                        <p class="text-center">Nội dung chưa cập nhật</p>
                                    </div>

                                </div></div>

                            <div class="text-left m-t-10 box-bg" id="chisonam"><p class="font-weight-bold m-b-1" style="font-size:15px">6. CÁC CHỈ SỐ NĂM</p>
                                <p id="chisonam_des_1" class="font-italic m-b-8 m-t-1 description truncate" style="">Những con số này cho biết ở mỗi năm bạn nên tập trung định hướng phát triển theo con số nào. Thường thì cuộc đời sẽ tự đẩy bạn đi theo những con số này. Nếu đi lệch ra bạn thường sẽ bị cảm thấy cuộc sống mất cân bằng hoặc bất an. Còn nếu đi đúng hướng bạn thường cảm thấy rất bình an và thuận lợi. Lưu ý: Sau khi sử dụng VIP, mỗi năm bạn vào lại website tra cứu hoặc tải lại file để xem luận giải 3 năm tiếp theo và các nội dung luận giải mới nếu có!</p>
                                <em id="chisonam_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>
                                <div style="background: #fff;padding:13px 5px 8px 5px;border-radius: 8px;"><table class="result text-center">
                                        <tbody><tr>
                                            <td class="tt">Năm 2024</td>
                                            <td class="tn text-success">4</td>
                                        </tr>
                                        <tr>
                                            <td class="tt">Năm 2025</td>
                                            <td class="tn text-success">5</td>
                                        </tr>
                                        <tr>
                                            <td class="tt">Năm 2026</td>
                                            <td class="tn text-success">6</td>
                                        </tr>
                                        </tbody></table>

                                    <p class="mt-2 text-left"><button id="chisonam_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>

                                    <p id="chisonam_des_2" class="content text-left m-b-0" style="display: none;"><em class="font-weight-bold">VẬN SỐ NĂM 2024 CỦA BẠN LÀ: </em><b class="text-success">4</b><br><em class="font-weight-bold">Làm việc chăm chỉ và tiến độ chậm, nhưng ổn định</em><br>Năm cá nhân số 4 của bạn nhấn mạnh đến việc bạn cần củng cố năng lượng và nguồn lực để chúng có thể hỗ trợ cho sự phát triển của bạn trong tương lai. Bạn có thể bận tâm hơn về tiền bạc, tài sản hoặc cơ thể của mình.

                                        Xét về công việc, năm nay bạn phải làm việc một cách có tổ chức, có kế hoạch, thiết lập các mục tiêu một cách rõ ràng và quản lý tất cả các công việc của mình một cách quy củ. Bạn cũng cần luôn làm việc chăm chỉ và kiên trì. Nếu bạn lập kế hoạch tốt, thì khi các ý tưởng được thực hiện, nó sẽ cho phép bạn đạt được nhiều lợi ích trong việc củng cố sự ổn định cho tương lai. Những nỗ lực ngay từ bây giờ sẽ giúp ổn định cuộc sống của bạn trong những năm tới. Tóm lại, đây là một năm để bạn đặt nền móng vững chắc cho việc xây dựng công việc kinh doanh, thậm chí bao gồm cả tổ ấm và gia đình.

                                        Về các mối quan hệ, năm nay bạn cần củng cố lại chúng. Điều này sẽ giúp bạn hiểu rõ hơn về các mối quan hệ của mình và tạo nên sự gắn kết bền chặt. Đặc biệt, bạn sẽ có thể thu hút những người bạn đồng hành tuyệt vời trong năm nay nếu bạn cho họ thấy bạn là người đáng tin cậy, tận tâm với công việc, trung thành và có thái độ tích cực. Còn nếu bạn đang trong một mối quan hệ tình cảm, hãy mong đợi một lễ đính hôn hoặc kết hôn. Và nếu bạn đã kết hôn, hãy mong đợi có con.

                                        Về sức khỏe, năm cá nhân số 4 đặc biệt có tác động đến cơ thể vật lý của bạn. Bạn sẽ nhận thấy sự thay đổi về trọng lượng hoặc vóc dáng cơ thể, nhưng quan trọng hơn, bạn sẽ có thể xác định được bản chất của sự thay đổi này. Những thay đổi này sẽ tích cực nếu bạn chăm sóc và giữ gìn cơ thể của mình, và nó sẽ trở nên tồi tệ nếu như bạn lơ là. Năm cá nhân số 4 cũng có thể mang đến những căn bệnh được thiết kế để giữ cho bạn vững vàng. Ví dụ, bạn có thể bị thương khi tập thể dục và buộc bạn phải nghỉ ngơi. Qua sự việc này, bạn cần học cách đặt mục tiêu thực tế và tập thể dục đều đặn hơn. Bạn cũng có thể nhận thấy rằng mình cần quan tâm nhiều hơn đến việc chải chuốt cá nhân, chẳng hạn như cắt tóc hoặc trang điểm. Những yếu tố khác cũng cần chú ý là các vấn đề về thận hoặc bàng quang, căng thẳng tinh thần do suy nghĩ quá nhiều và trầm cảm.

                                        Trong khía cạnh tài chính, bạn cần phải xem xét tài sản, thỏa thuận, hợp đồng hoặc các vấn đề pháp lý, các chi tiết,... một cách kiên nhẫn và trung thực, tránh những rủi ro quá mức. Đừng đặt niềm tin vào sự may rủi hoặc cho phép bản thân bất cẩn trong năm nay. Hãy chỉ tham gia vào những dự án khi bạn chắc chắn rằng chúng có tính chất hợp lý và sẽ thành công. Đây cũng là thời điểm bạn nên tiết kiệm và bắt đầu tích lũy một số tài sản. Việc mua bán hoặc thực hiện các giao dịch liên quan đến xây dựng nhà cửa để định cư, đặt nền tảng vững chắc, thiết thực cho tương lai sẽ phù hợp trong năm này.  Còn nếu bạn muốn đầu tư vào nhà đất để sinh lời thì đây chưa thực sự là thời điểm tốt cho bạn.

                                        Liên quan đến học tập thi cử, năm nay là năm bạn cần phải thực sự nỗ lực học tập chăm chỉ để có được những kết quả tốt. Nếu bạn có dự định tham gia vào các khóa học, đi du học hoặc học lên các cấp học cao hơn thì đây là năm phù hợp.

                                        Về bản thân, đây là năm bạn sẽ cần phải bình tĩnh, ổn định, sắp xếp, tuân theo một thói quen, chịu trách nhiệm và nhìn thấu đáo mọi thứ. Bạn cũng cần dành thời gian để suy nghĩ nghiêm túc về tương lai. Hãy cố gắng tránh những cuộc cãi vã và hiểu lầm không đáng có. Năm này sẽ cho phép bạn sửa chữa một số sai lầm quan trọng mà bạn đã mắc phải trong quá khứ. Bạn có thể phải từ bỏ các kỳ nghỉ, tiệc tùng, hoặc các khoản chi tiêu khác. Từ khóa quan trọng trong năm nay là sự cân bằng. Hãy tự kỷ luật bản thân bởi vì nếu bạn làm việc chăm chỉ, kiên nhẫn, tỉnh táo và thực tế thì khi hết năm, bạn sẽ cảm thấy khá hài lòng về những điều bạn đã hoàn thành. Năm nay, bạn cũng có thể gặp những người có thể dạy bạn nhiều hơn về tình bạn. Bạn có thể phải đi công tác ngắn ngày, xa gia đình, hoặc có những xung đột trong mối quan hệ tình cảm. Những cảm xúc bị kìm hãm cần phải được giải quyết một cách cởi mở và trung thực. Bạn có thể gặp khó khăn trong việc bộc lộ cảm xúc và sẽ cần phải học cách chia sẻ cũng như kiểm soát cảm xúc với những người thân yêu của mình.

                                        Tóm lại, thành công của bạn vào năm này là xây dựng được những bước đi bài bản và chắc chắn cho một mục tiêu lớn trong cuộc đời. Để đạt được điều đó, bạn cần bỏ đi sự quan tâm tới những điều nhỏ nhặt mà hãy luôn nghĩ về một tầm nhìn dài hạn, sau đó dành nhiều thời gian kiên trì để tìm cách thực hiện nó. Sự nỗ lực và chăm chỉ trong năm nay là không thể thiếu, vậy nên hãy trở nên cần cù hơn và đổi lại sẽ có những giá trị tuyệt vời đến với bạn.<br><br><em class="font-weight-bold">VẬN SỐ NĂM 2025 CỦA BẠN LÀ: </em><b class="text-success">5</b><br><em class="font-weight-bold">Cảm thấy lỏng lẻo và tự do, nhiều thay đổi</em><br>Đây sẽ là một năm cho sự thay đổi, phát triển, vui vẻ và tự do. Năm nay có thể có một số sự thay đổi quan trọng về nơi ở, công việc, hướng đi hoặc hoàn cảnh gia đình. Bạn phải tận dụng lợi thế của sự thay đổi đó để giúp bạn tiến lên phía trước. Thách thức của bạn trong năm nay là tiếp tục tập trung vào các mục tiêu dài hạn, cố gắng sắp xếp các dự án quan trọng và xem xét chúng từ đầu đến cuối. Đôi khi bạn có thể cảm thấy bất an nhưng nhìn chung cả năm đều mang đến cơ hội thăng tiến và tiến bộ.

                                        Về công việc, không giống như năm trước khi mọi thứ có xu hướng ổn định, năm nay dường như có phần lỏng lẻo và có sự thay đổi (có thể về môi trường làm việc, công việc của bạn,...). Đôi khi bạn thấy mình mất hứng thú và trì hoãn các công việc hoặc dự án quan trọng mà bạn cần hoàn thành. Cũng có khi bạn lại sử dụng quá mức các nguồn lực của mình (có thể là sức khỏe, tiền bạc,...). Do đó, lời khuyên là bạn hãy lập kế hoạch cho một mục tiêu nhất định để thăng tiến đồng thời thực hiện các điều chỉnh và quyết định cần thiết để cải tiến. Ngoài ra, với năng lượng của năm số 5, bạn có thể theo đuổi sự nghiệp trong lĩnh vực nghệ thuật biểu diễn như ca hát, diễn xuất hoặc khiêu vũ, hoặc bạn có thể trở thành một nhà văn viết về du lịch, đi khắp thế giới để tìm kiếm những điều thú vị và phiêu lưu mới.

                                        Trong các mối quan hệ, để có được những điều tốt đẹp nhất trong năm nay, tất cả những thay đổi nên có lợi cho người khác cũng như cho chính bạn. Hãy cố gắng sống hài hòa với mọi người. Bạn có thể sẽ tham gia nhiều cuộc tụ họp xã hội hơn bình thường hoặc thậm chí tổ chức các bữa tiệc để xây dựng mối quan hệ tốt hơn và củng cố những mối quan hệ bạn đã có. Nếu đã kết hôn, hãy mong đợi sự tập trung lại vào tình yêu với niềm đam mê. Nếu còn độc thân, hãy mong đợi được gặp những người mới, đầy đam mê và tuyệt vời trên hành trình của bạn. Nếu người yêu vẫn còn đồng hành với bạn cho đến tháng 4 năm sau, họ sẽ là người bạn đời tiềm năng trong hôn nhân. Ngoài ra, hãy giải quyết mọi xích mích tiềm ẩn trong mối quan hệ gia đình của bạn. Nếu có căng thẳng trong nhà, hãy cố gắng giữ bình tĩnh và nhớ suy nghĩ kỹ trước khi nói. Cãi nhau chỉ gây ra rắc rối và có thể dẫn đến chia rẽ hoặc rắc rối pháp lý.

                                        Liên quan đến sức khỏe, đây là năm có thể có những thay đổi về sức khỏe của bạn. Dường như bạn dễ thích nghi và linh hoạt hơn, cũng như sẵn sàng thúc đẩy bản thân về mặt tinh thần và thể chất lên những tầm cao mới. Tuy nhiên, bạn cũng cần thận trọng vì năng lượng của năm số 5 có thể khiến bạn bị ốm do quá phấn khích cũng như thay đổi đột ngột.

                                        Về tài chính, hãy xem xét những gì bạn ký và suy nghĩ kỹ về việc thực hiện các thỏa thuận dài hạn. Nếu bạn không suy nghĩ thấu đáo, bạn có thể phải thực hiện các thay đổi hoặc điều chỉnh sau đó. Nếu bạn muốn đầu tư vào đất đai hoặc thực hiện các giao dịch tài chính lớn, đây không phải là năm đem lại cho bạn thành công nổi trội cho bạn.

                                        Về học tập thi cử, đây là năm bạn nên khám phá những khía cạnh, góc nhìn mới về bản thân mình. Nếu bạn có định hướng học lên các cấp học cao hơn hoặc đi du học, khám phá những nền văn hóa cũng như miền đất mới thì đây là năm phù hợp cho những dự định đó của bạn.

                                        Về bản thân, năm nay bạn cần cố gắng giữ tập trung và tránh bị phân tán năng lượng. Khi những cơ hội mới đến, bạn nên cố gắng đón nhận và có một tư duy cởi mở thì bạn sẽ có thể làm mới cuộc sống của mình, xóa bỏ những "lớp vỏ" cũ để có thêm tự do cá nhân và thăng tiến. Ngoài ra, năm nay bạn cũng nên thử một phong cách thời trang, thức ăn hoặc sở thích mới. Nếu bạn có năng khiếu viết lách, nói trước đám đông hay nghệ thuật, bạn sẽ thấy cuộc sống mang đến những cơ hội để bạn nâng cao khả năng này của mình. Ngoài ra, bạn cũng có thể học ngôn ngữ mới để tăng cơ hội tìm việc làm ở nước ngoài hoặc đi du lịch.

                                        Tóm lại, năm cá nhân số 5 đem đến những thay đổi và cuộc phiêu lưu mới, nơi bạn có thể sẽ có những trải nghiệm mới hoặc bước vào những mối quan hệ mới, hoặc mang lại sự mới mẻ cho những mối quan hệ hiện có. Hãy đón nhận những thay đổi và học cách thích nghi để tìm thấy thành công và niềm vui. Những quyết định của bạn trong năm này sẽ mang lại thay đổi rất lớn, vậy nên hãy suy tính thật kỹ càng và đưa ra những lựa chọn lý trí.<br><br><em class="font-weight-bold">VẬN SỐ NĂM 2026 CỦA BẠN LÀ: </em><b class="text-success">6</b><br><em class="font-weight-bold">Tình yêu, Gia đình, Tổ ấm và Trách nhiệm</em><br>Năm cá nhân số 6 được gọi là năm của tình yêu, hôn nhân và sự hy sinh. Cuộc sống sẽ mang đến cho bạn những sự kiện và cơ hội mà nhấn mạnh đến trách nhiệm và công việc đối nội. Năm nay, trọng tâm của bạn là gia đình, hôn nhân, trách nhiệm, phục vụ và hy sinh.

                                        Trong các mối quan hệ, năm nay hãy dành thời gian quan tâm, chia sẻ với gia đình và bạn bè, hiểu và cân nhắc nhu cầu của mọi người, đồng thời cố gắng cống hiến và không ích kỷ nhất có thể. Tuy nhiên, đôi khi những người khác có thể sẽ áp đặt bạn hoặc cố gắng lợi dụng bản chất tốt của bạn; hàng xóm, bạn bè và người thân nhờ sự trợ giúp; bạn có thể phải chăm sóc người bệnh hoặc người già. Hóa đơn vượt quá khả năng, việc sửa chữa bất ngờ phát sinh và người bạn đời của bạn có vẻ bất hợp tác. Nếu bạn chấp nhận những tình huống này như một đặc ân thay vì một gánh nặng, bạn sẽ gặt hái được trái ngọt ở cuối con đường. Phục vụ không vụ lợi, nhưng hãy cẩn thận đừng để người khác lợi dụng mong muốn được giúp đỡ của bạn. Nếu đã kết hôn, đây là năm bạn nên chia sẻ những khoảnh khắc quý giá với gia đình và bạn bè. Nếu bạn chưa lập gia đình, đây là một năm tốt để kết hôn hoặc tiếp tục hôn nhân, miễn là tình cảm được cân bằng và được thể hiện một cách cởi mở. Che giấu hoặc ngụy tạo cảm xúc sẽ chỉ gây ra nhiều vấn đề hơn. Tuy nhiên, năm nay nếu bạn đang có những căng thẳng trong hôn nhân, chúng có thể sẽ trở nên trầm trọng hơn và mặc dù bạn có thể không thoải mái, nhưng bây giờ là lúc để giải quyết triệt để. Nếu căng thẳng vẫn còn, cuộc hôn nhân có thể kết thúc, hoặc ly thân hoặc ly hôn. Nếu các vấn đề được thảo luận với sự thấu hiểu và cân nhắc về mong muốn và nhu cầu của đối phương thì cuộc hôn nhân có thể được tái sinh trên một mức độ cao hơn của tình yêu, sự tôn trọng và hợp tác lẫn nhau. Cố gắng giữ bình tĩnh và nói ra mọi chuyện nếu bạn có thể. Nếu độc thân, năm nay bạn có thể có mong muốn về sự đồng hành, gia đình và hôn nhân. Bạn có thể cảm thấy rất muốn ổn định, cả trong công việc và đối nội, và cuối năm sẽ mang lại cho bạn rất nhiều sự hài lòng về mặt này. Năm nay, dù bạn kết hôn với một người hay có một công việc mới, hãy thực sự cam kết với nó.

                                        Về công việc, hãy đảm bảo bạn là người tháo vát, quyết đoán và bảo vệ những điều bạn tin tưởng. Bạn có thể có cơ hội thăng tiến và cải thiện công việc kinh doanh hoặc tài chính của mình. Tuy nhiên, nhiều khi bạn có thể thấy mình cam kết quá mức trong công việc và không thể từ chối công việc, cơ quan vì bạn không muốn làm tổn thương hoặc làm cấp trên thất vọng. Đây là lúc bạn cần thiết lập ranh giới để tránh bị lợi dụng hoặc ngược lại. Hãy làm việc theo tốc độ của bạn, làm điều gì đó khiến bạn cảm thấy bình yên; bạn thậm chí có thể học một kỹ năng hoặc thử một thứ gì đó mang tính nghệ thuật, thiền định thường xuyên và nghe nhạc. Bạn càng cảm thấy thoải mái và thanh thản, bạn càng có thể tập trung vào công việc tốt hơn và cống hiến hết mình để đạt được kết quả tốt hơn.

                                        Xét về sức khỏe, việc trốn tránh trách nhiệm hoặc không ưu tiên các mối quan hệ của bạn, đặc biệt là với gia đình và bạn bè, về lâu dài có thể ảnh hưởng đến sức khỏe tinh thần của bạn. Nếu trách nhiệm dường như quá nhiều đối với bạn hoặc bạn cảm thấy choáng ngợp ở nơi làm việc hoặc ở nhà, hãy thử một số thói quen sẽ giúp cân bằng chúng và một khi bạn tìm thấy thói quen phù hợp với mình, hãy kiên trì thực hiện. Hãy giao một số trách nhiệm cho những người (gia đình và bạn thân) mà bạn biết họ có khả năng đảm đương. Ngoài ra, đây là một năm để bạn học cách xả stress, có niềm tin vào bản thân và những gì bạn đang làm, dành thời gian cho bản thân và thư giãn, chỉ tập trung vào những điều quan trọng nhất với bạn. Hãy nhớ rằng bạn không thể giúp đỡ hoặc nuôi dưỡng bất kỳ ai nếu bạn không khỏe mạnh về thể chất hoặc tinh thần, vì vậy hãy ưu tiên sức khỏe của bạn.

                                        Về tài chính, nếu bạn cân bằng được gia đình và công việc, trách nhiệm và bản thân thì đây là năm bạn có thể có những sự cải thiện về tài chính. Nếu bạn có điều kiện, đây là năm phù hợp cho bạn để sửa sang, trang trí lại nhà cửa hoặc mua nhà, đất để định cư, sinh sống. Nếu bạn muốn đầu tư vào tài chính, đất đai để sinh lời thì năm nay chưa thực sự tốt, vì bạn có thể bị cảm xúc và trách nhiệm khiến bạn không lý trí đưa ra quyết định đúng đắn nhất.

                                        Liên quan đến học tập thi cử, nếu bạn muốn học lên các hàm vị cao hơn thì năm nay là năm phù hợp cho quyết định đó của bạn.

                                        Về bản thân, để đạt được nhiều thành tựu nhất trong năm nay, bạn cần biết hài lòng, quan tâm đến vấn đề của người khác và thể hiện rất nhiều tình yêu thương. Bạn cũng nên cho đi một cách hào phóng hơn những gì bạn nhận được. Việc tạo ra một bầu không khí hòa hợp cũng sẽ rất quan trọng, vì tình yêu, tiền bạc, sức khỏe và tình bạn có thể dễ dàng vuột mất nếu bất hòa kéo dài. Ngoài ra, hãy giữ những lý tưởng cao đẹp và cố gắng tránh bất kỳ cảm giác bực bội vì sự bất công. Hãy sống thật với chính mình và bạn thậm chí có thể có cơ hội để truyền những lý tưởng vào trái tim và tâm trí của người khác.

                                        Tóm lại thành công của bạn vào năm này là đạt được sự thấu hiểu về con người và xây dựng được niềm tin rất lớn của mọi người với bạn. Hãy dành thời gian thật vui vẻ bên gia đình và những người thân yêu của mình. Tuy nhiên hãy nhớ tiết chế tinh thần trách nhiệm của mình ở mức vừa đủ, không cho đi quá mức và cũng không giữ lại quá mức.<br></p><p class="content text-left m-b-10 text-danger font-weight-bold">Nếu bạn muốn xem thần số học trọn đời (luận giải chi tiết từng tuổi một, từ đầu đời cho tới năm 80 tuổi), bạn có thể đăng kí để trực tiếp thầy Louis Nguyễn xem và luận giải.<br></p><p class="content text-center m-b-0 text-danger"><a href="https://tracuuthansohoc.com/thansohoctrondoi/" target="_blank"><button class="btn btn-success">ĐĂNG KÍ TẠI ĐÂY</button></a></p></div></div>

                            <div class="text-left m-t-10 box-bg" id="chisothang"><p class="font-weight-bold m-b-1" style="font-size:15px">7. CÁC CHỈ SỐ THÁNG</p>
                                <p id="chisothang_des_1" class="font-italic m-b-8 m-t-1 description truncate">Những con số này cho biết ở mỗi tháng sẽ có những điều gì có khả năng xảy ra và bạn nên tập trung làm việc như thế nào, theo con số nào nhưng ở mức độ bao quát thấp hơn chỉ số năm. Lưu ý: Sau khi sử dụng VIP, mỗi tháng bạn vào lại website tra cứu hoặc tải lại file để xem luận giải 3 tháng tiếp theo và các nội dung luận giải mới nếu có!</p>
                                <em id="chisothang_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>

                                <div style="background: #fff;padding:13px 5px 8px 5px;border-radius: 8px;"><table class="result text-center">
                                        <tbody><tr>
                                            <td class="tt">Tháng 5/2024</td>
                                            <td class="tn">9</td>
                                        </tr>
                                        <tr>
                                            <td class="tt">Tháng 6/2024</td>
                                            <td class="tn">1</td>
                                        </tr>
                                        <tr>
                                            <td class="tt">Tháng 7/2024</td>
                                            <td class="tn">2</td>
                                        </tr>
                                        </tbody></table>

                                    <p class="mt-2 text-left"><button id="chisothang_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>

                                    <p id="chisothang_des_2"  class="content text-left m-b-0" style="display: none;"><em class="font-weight-bold">- CHỈ SỐ SỐ THÁNG 5/2024 LÀ: </em><b class="text-success">9</b><br>Đây là tháng để bạn nhìn lại cuộc sống của mình trong suốt thời gian qua và xem bạn đã đạt được những gì.
                                        Liên quan đến công việc, tháng này bạn nên xem xét nghề nghiệp hiện tại có thực sự phù hợp với bạn hay không. Hoặc bạn nên đánh giá lại xem những gì bạn đã làm tốt, những gì vẫn còn thiếu sót để bắt đầu lên kế hoạch học tập và phát triển trong tháng tiếp theo. Cuối cùng là hãy hoàn thành các dự án còn dang dở và giải quyết nốt những công việc còn tồn động.
                                        Xét về sức khỏe, đây là thời điểm phù hợp để bạn đi kiểm tra sức khỏe tổng quát hằng năm. Ngoài ra, bạn cũng nên chấm dứt những thói quen xấu. Điều này sẽ giúp bạn giải phóng thời gian và năng lượng cho cuộc sống.
                                        Về bản thân, hãy tổ chức lại các kế hoạch của bạn và chuẩn bị cho những nguồn năng lượng mới đang đến. Dành thời gian để thư giãn và cầu nguyện. Nếu bạn vẫn thấy cuộc sống của mình khó khăn, bạn có thể cần suy nghĩ lại về những gì mình đang làm, phải chăng ở đâu đó trên con đường đang đi, bạn đã đầu tư năng lượng của mình sai chỗ. Đây cũng là thời điểm để bạn bừng tỉnh, đừng đắm mình trong quá khứ. Lưu ý rằng, bạn đang sống trong hiện tại và bạn cần tập trung vào cách để bản thân có thể cải thiện cuộc sống của mình cho hôm nay và ngày mai. Năng lượng mới sẽ đến trong tháng tới để bạn tạo ra một tương lai đầy hy vọng.
                                        Tóm lại, đây là tháng bạn nên nhìn lại và chấm dứt một cách tốt đẹp những việc cần kết thúc hoặc những thứ không còn phù hợp với bạn nữa. Đã đến lúc suy nghĩ về đích đến và tương lai của bạn.<br><br><em class="font-weight-bold">- CHỈ SỐ THÁNG 6/2024 LÀ: </em><b class="text-success">1</b><br>Đây là một tháng quan trọng đối với bạn.
                                        Trong công việc, tháng này là thời điểm tuyệt vời để bắt đầu một dự án mới, kế hoạch mới hoặc một công việc mới. Bạn nên suy ngẫm về các mục tiêu của mình và tập trung vào phát triển chúng. Đặc biệt, khoảng thời gian này thuận lợi cho mọi việc bạn làm, vì vậy tốt hơn hết bạn nên hoàn thành những nhiệm vụ quan trọng và có lợi cho mình chứ không phải ai khác. Ngoài ra, một điều bạn cần lưu ý là những gì bạn làm trong tháng này có thể có tác động lâu dài, vì vậy hãy làm cho nó có giá trị!
                                        Liên quan đến sức khỏe, đa phần tháng này bạn khá khỏe mạnh. Đây là tháng tốt để bạn bắt đầu thiết lập thói quen tập thể dục nếu bạn muốn có một cơ thể đẹp và sức khỏe dẻo dai sau này.
                                        Về học tập phát triển bản thân, tháng này bạn nên bắt đầu những thói quen mới giúp nâng cao giá trị bản thân. Bạn có thể tham gia vào một số khóa học, học một ngôn ngữ mới, tạo lập một thói quen tốt mới,... Những điều này sẽ giúp bạn đi đúng năng lượng của tháng và đem lại cho bạn rất nhiều giá trị trong thời gian tới.
                                        Tóm lại, đây là một tháng rất tốt để bạn bắt đầu làm những điều mới mẻ. Vì vậy, hãy nắm bắt lấy nó.<br><br><em class="font-weight-bold">- CHỈ SỐ THÁNG 7/2024 LÀ: </em><b class="text-success">2</b><br>Tháng này bạn nên tạo được sự cân bằng trong cuộc sống của mình. Trực giác của bạn sẽ trở nên nhạy bén hơn, chính nó sẽ dẫn dắt bạn đi đến những quyết định đúng đắn trong cuộc sống của mình.
                                        Về các mối quan hệ, đây là một tháng để bạn kết nối sâu sắc hơn với những người xung quanh trong cuộc sống như bạn bè, đồng nghiệp, đối tác,... Nếu gần đây bạn là người cô độc, thì đã đến lúc tìm kiếm tình yêu cùng với sự lãng mạn.
                                        Xét về công việc, tháng này đề cao sự hợp tác thay vì làm việc hoặc đưa ra những quyết định độc lập. Nếu có thể, bạn nên tham gia vào các dự án nhóm hoặc những việc đề cao tính đồng đội. Những người làm các công việc liên quan đến sáng tạo sẽ thấy công việc của mình được cải thiện nhờ trực giác của tháng số 2 này.
                                        Liên quan đến sức khỏe, đây là tháng mà bạn cần học cách kiểm soát cảm xúc của mình để giữ được sức khỏe tinh thần tốt. Sự nhạy cảm của bạn trong tháng này sẽ tăng lên, đồng nghĩa với việc bạn dễ dàng đồng cảm với cảm xúc của những người xung quanh. Thế nhưng, bạn cũng có thể dễ bị những cảm xúc tiêu cực quấn lấy, vậy nên hãy tỉnh táo hơn nhé. Về thể chất, đôi khi bạn có thể cảm thấy không được khỏe mạnh và trì hoãn trong việc tập thể dục. Bạn cần học cách chăm sóc bản thân mình và kiên trì luyện tập thể dục thể thao.
                                        Về học tập và rèn luyện, tháng này bạn nên bắt đầu hoặc duy trì thói quen đọc sách, thiền định, yoga,...Điều này sẽ giúp bạn tập trung, kiểm soát cảm xúc tốt hơn và nâng cao trí tuệ cũng như thể chất.
                                        Tóm lại, đây là tháng mà bạn nên tập trung vào sự hợp tác và phát triển giá trị của bản thân. Bạn cũng nên tin tưởng vào bản thân và cảm xúc của mình, bạn sẽ thấy rằng những điều này không bao giờ biết nói dối.<br></p></div></div>

                            <div class="text-left m-t-10 box-bg" id="sumenh">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">8. {{mb_strtoupper($contents['destinyNumberContent']->name??"")}} LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['destinyNumberContent']->content_number}}</span></p>


                                <p id="sumenh_des_1" class="font-italic m-b-8 m-t-2 description truncate" style="">Trong Thần số học, chỉ số sứ mệnh giúp bạn biết cách đạt được mục tiêu của bạn, lớn và nhỏ. --Sứ mệnh khác với số đường đời. Con số đường đời của bạn ám chỉ đến mục đích tổng thể lớn hơn của bạn. Chỉ số sứ mệnh của bạn tập trung nhiều hơn vào các đặc điểm, tính cách của bạn. Nhưng số đường đời và số mệnh của bạn có thể đi đôi với nhau. Theo thần số học, số đường đời của bạn cho bạn biết bạn đến cuộc đời này để làm gì, số sứ mệnh của bạn mô tả cách bạn tiếp tục thực hiện nó.<br>Phép tính đơn giản của chúng tôi xác định số sứ mệnh giúp bạn xác định các đặc điểm chính của mình và nơi bạn xuất sắc trong cuộc sống. Không chỉ một giai đoạn của cuộc đời, mà con số này tác động vào mọi giai đoạn của cuộc đời bạn.<br>Bạn có thể chọn một con đường đi, nhưng nó có thể không phải lúc nào cũng đúng. Chỉ số này giúp xác định con đường nào là con đường phù hợp sẽ khiến bạn hài lòng và mãn nguyện.</p>
                                <em id="sumenh_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>

                                <p class="mt-2 text-left"><button id="sumenh_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>
                                <div id="sumenh_des_2" class="content" style="display: none;">
                                    {!! $contents['destinyNumberContent']->content??'<p class="text-center">Nội dung chưa cập nhật</p>' !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="dd_sm">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">9. @lang('TƯƠNG QUAN ĐƯỜNG ĐỜI - SỨ MỆNH'): <span class="text-success" style="font-weight: bold;">{{$contents['lifePathNumberContent']->content_number??''}} &amp; {{$contents['destinyNumberContent']->content_number??''}}</span></p>

                                <p id="dd_sm_des_1" class="font-italic m-b-8 m-t-2 description truncate" style="">Chỉ số đường đời và chỉ số sứ mệnh là hai yếu tố có mối quan hệ chặt chẽ với nhau, cùng tồn tại trong một người. Chỉ số đường đời cho biết mục đích tổng thể của một người trong cuộc sống, còn chỉ số sứ mệnh cho biết cách thức một người thực hiện mục đích đó.<br>Vì vậy, khi chỉ số đường đời và sứ mệnh của bạn bổ sung hoặc cộng hưởng/tương hợp với nhau, những điểm mạnh trong tính cách của bạn sẽ được thúc đẩy và thể hiện rõ ràng hơn.<br>Ngược lại, nếu chỉ số đường đời và sứ mệnh của bạn bị đối đầu hoặc mâu thuẫn, một số năng lượng tích cực có thể bị mờ nhạt. Bên cạnh đó, bạn có thể cảm thấy bối rối và khó khăn trong việc đưa ra quyết định, nội tâm nhiều khi bị mâu thuẫn. Tuy nhiên, bạn vẫn có thể hoá giải những đối đầu, xung đột đó nếu nỗ lực học hỏi, thay đổi và phát triển bản thân.</p>
                                <em id="dd_sm_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>

                                <p class="mt-2 text-left"><button id="dd_sm_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>

                                <div id="dd_sm_des_2" class="content" style="display: none;">
                                    <p class="text-center">Nội dung chưa cập nhật</p>
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="thuthachsumenh">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">10. {{mb_strtoupper($contents['lifePathChallengeContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['lifePathChallengeContent']->content_number??''}}</span></p>

                                <p id="thuthachsumenh_des_1" class="font-italic m-b-8 m-t-2 description truncate" style="">Chỉ số này cho biết những kiểu thử thách thường gặp nhất trong suốt cuộc đời mà bạn sẽ phải vượt qua nó. Bạn hãy nhớ theo thần số học, một thách thức không phải là một mối đe dọa. Đây là thử thách bắt nguồn từ họ tên khai sinh của bạn, nó nói về những lựa chọn mà bạn có thể thực hiện sẽ cải thiện hoàn cảnh chung của mình.</p>
                                <em id="thuthachsumenh_btn_1" class="font-italic description text-primary showIndexContent" style="vertical-align: top; cursor: pointer;">(Xem thêm)</em>

                                <p class="mt-2 text-left"><button id="thuthachsumenh_btn_2" class="btn btn-primary showResultContent" style="padding: 1px 3px;font-size:13px;">Xem giải thích chi tiết</button></p>
                                <div id="thuthachsumenh_des_2" class="content" style="display: none;">
                                    {!! $contents['lifePathChallengeContent']->content??'' !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="truongthanh">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">11. {{mb_strtoupper($contents['maturityNumberContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['maturityNumberContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Số trưởng thành của bạn cho biết hướng thành công và mong muốn tiềm ẩn dần dần xuất hiện vào khoảng tuổi từ 40 trở lên (một số người sớm hơn hoặc muộn hơn). Mục tiêu cơ bản này bắt đầu xuất hiện khi bạn hiểu rõ hơn về bản thân.<br>Với sự hiểu biết về bản thân, bạn sẽ nhận thức rõ hơn về con người của mình, mục tiêu thực sự của bạn trong cuộc sống là gì và bạn muốn đặt ra hướng đi nào cho cuộc đời mình.--Tóm lại, đây là món quà của sự trưởng thành: Bạn không còn lãng phí thời gian và năng lượng cho những thứ không thuộc bản sắc đặc biệt của riêng bạn.<br>Cho dù hiện tại bạn đang ở độ tuổi nào, cuộc sống của bạn đang được chuyển sang một hướng cụ thể, hướng tới một mục tiêu rất cụ thể. Mục tiêu đó có thể được xem như một phần thưởng hoặc sự hoàn thành một lời hứa tiềm ẩn trong những nỗ lực hiện tại của bạn, mà bạn thường không biết điều đó một cách có ý thức. Trong khi các đặc điểm của con số này thường được nhìn thấy trong thời thơ ấu, chúng ta có xu hướng mất dần các khía cạnh này cho đến khi lớn lên. Nhưng dù sao thì cuộc sống của chúng ta cũng luôn bị ảnh hưởng bởi nó.<br>Số trưởng thành của bạn bắt đầu có tác động sâu sắc hơn đến cuộc sống của bạn sau 35 tuổi - khoảng thời gian bạn bước vào chu kỳ Đỉnh cao thứ hai của mình. Ảnh hưởng của số này tăng đều đặn khi bạn dần cao tuổi hơn.</p>
                                <div class="content" style="">
                                    {!! $contents['maturityNumberContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="nangluctruongthanh">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">12. {{mb_strtoupper($contents['maturityAbilityContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['maturityAbilityContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Chỉ số này cho bạn biết cần phải làm gì để có thể nhanh chóng đưa các bài học mà cuộc đời đã dạy bạn vào việc hoàn thành sứ mệnh của cuộc đời. Hay nói cách khác, nó là sự kết nối giữa đường đời của bạn và sứ mệnh của bạn. Nếu chỉ số này trùng với năng lực tự nhiên của bạn thì thật là tuyệt vời!</p>

                                <div class="content" style="">
                                    {!! $contents['maturityAbilityContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="linhhon">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">13. {{mb_strtoupper($contents['soulUrgeNumberContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['soulUrgeNumberContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Linh hồn, sự khao khát từ sâu bên trong của mỗi người. Chỉ số này hé lộ linh hồn hay sâu thẳm trong bạn mong muốn bạn trở thành con người như thế nào thì mới cảm thấy thỏa mãn và trọn vẹn. Nói cách khác, nếu bạn có những đặc điểm tích cực của chỉ số này, linh hồn bạn sẽ cảm thấy hạnh phúc.</p>

                                <div class="content" style="">
                                    {!! $contents['soulUrgeNumberContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="dd_lh">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">14. @lang('TƯƠNG QUAN ĐƯỜNG ĐỜI - LINH HỒN'): <span class="text-success" style="font-weight: bold;">{{$contents['lifePathNumberContent']->content_number??''}} &amp; {{$contents['soulUrgeNumberContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Chỉ số đường đời và chỉ số linh hồn là hai yếu tố có mối quan hệ chặt chẽ với nhau, cùng tồn tại trong một người. Chỉ số đường đời cho biết năng lượng tổng quát của một người (các điểm mạnh điểm yếu, xu hướng tính cách, các bài học trong cuộc đời, bản ngã nguyên thủy,...); còn chỉ số linh hồn cho biết mong muốn, khao khát sâu bên trong của mỗi người. Vì vậy, khi chỉ số đường đời và linh hồn của bạn bổ sung hoặc cộng hưởng/tương hợp với nhau, bạn sẽ cảm thấy hài lòng và thỏa mãn với hầu hết những gì mình đang làm; kiên trì và quyết tâm vượt qua mọi khó khăn để đạt được mục tiêu. Ngược lại, nếu chỉ số đường đời và linh hồn của bạn bị đối đầu hoặc mâu thuẫn, bạn có thể bị mắc kẹt và gặp khó khăn trong việc đưa ra quyết định, nội tâm bị mâu thuẫn, thiếu động lực và sự quyết tâm để thực hiện mục tiêu của mình. Tuy nhiên, bạn vẫn có thể hoá giải những đối đầu, xung đột đó nếu nỗ lực học hỏi, thay đổi và phát triển bản thân.</p>

                                <div class="content" style="">
                                    <p class="text-center">Nội dung chưa cập nhật</p>
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="thuthachlinhhon">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">15. {{mb_strtoupper($contents['soulChallengeContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['soulChallengeContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Linh hồn mong muốn được trải nghiệm và trưởng thành, mọi nguyên liệu cho sự trưởng thành đó không bao giờ hoàn hảo, và sự trưởng thành không bao giờ xảy ra nếu không có đấu tranh. Chỉ số thử thách linh hồn này đại diện cho những trở ngại mà linh hồn mang theo khi nhập thể để bạn bắt đầu kiếp sống này, vượt qua chúng là một nhiệm vụ quan trọng trong suốt cuộc đời. Hiểu về chỉ số linh hồn và thử thách của nó có thể nói lên nhiều điều về mục đích hoặc sứ mệnh của bạn trong cuộc sống.</p>

                                <div class="content" style="">
                                    {!! $contents['soulChallengeContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="nhancach">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">16. {{mb_strtoupper($contents['personalityNumberContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['personalityNumberContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Bạn biết bạn là ai. Bạn biết tâm trí của bạn, suy nghĩ của bạn, ý kiến của bạn và có một cảm giác về tính cách của bạn. Tuy nhiên, những người khác không nhìn bạn như cách bạn nhìn thấy chính mình. Chỉ số này sẽ giúp bạn nhìn thấy những gì mình gửi ra thế giới. Bạn sẽ hiểu tại sao một số người rời bỏ bạn, một số ở lại với bạn và một số không bao giờ cố gắng. Nó cực kỳ quan trọng về mặt số học vì nó sẽ cho biết những gì bạn gửi ra bên ngoài và từ đó người khác đánh giá và nhìn nhận bạn như thế nào.</p>

                                <div class="content" style="">
                                    {!! $contents['personalityNumberContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>"!!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="thuthachnhancach">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">17. {{mb_strtoupper($contents['personalityChallengeNumberContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['personalityChallengeNumberContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Thái độ và suy nghĩ của bất kỳ một ai về bản thân đều ảnh hưởng tới sức khỏe, sự thịnh vượng, mối quan hệ, đời sống tinh thần, ... của người đó. Chỉ số về nhân cách thể hiện phản hồi mà bên ngoài (người khác) cảm thấy chúng ta về các lựa chọn, suy nghĩ, lời nói và việc làm của chúng ta. Khi bạn không hài lòng với hoàn cảnh của mình, chính chỉ số này sẽ thể hiện phản ứng với hoàn cảnh đó - nó có thể tạo ra một kế hoạch hành động phù hợp theo khía cạnh tích cực, hoặc tạo ra suy nghĩ tiêu cực như giận dữ, tự phê bình, phòng thủ hoặc chống đối. Những suy nghĩ và cảm xúc tiêu cực này có thể phá hoại kế hoạch của bạn. Nếu không có kế hoạch nào được thực hiện, chúng sẽ thu hút những hoàn cảnh khó khăn hơn, điều này có thể dẫn đến những xáo trộn về thể chất và tinh thần. Và chỉ số thử thách nhân cách sẽ cho bạn biết những gì bạn làm để đối diện với những tình huống như vậy.</p>

                                <div class="content" style="">
                                    {!! $contents['personalityChallengeNumberContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="diemyeu">
                                @php
                                    $weeknessNumberArray = [];
                                @endphp
                                @foreach (array_values($contents['weeknessNumberContent'])[0] as $value)
                                            @php
                                                $weeknessNumberArray[] = array_key_first($value);
                                            @endphp
                                @endforeach
                                <p class="font-weight-bold m-b-1" style="font-size:15px">18. CÁC {{mb_strtoupper(getIndicatorName(array_key_first($contents['weeknessNumberContent'])))}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{implode(', ', $weeknessNumberArray)}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Các chỉ số này thể hiện những điểm yếu của bạn mà kiếp trước bạn chưa khắc phục được hoặc có thể ngay ở kiếp này còn tồn đọng. Nên sự hoàn thành của cuộc đời bạn là khắc phục được các số còn thiếu này.</p>

                                <div class="content" style="">
                                    <p class="text-center">Nội dung chưa cập nhật</p>
                                </div>
                            </div>
                            <div class="text-left m-t-10 box-bg" id="nonghiep">
                                @php
                                    $karmicDebtNumberArray = [];
                                @endphp
                                @foreach (array_values($contents['karmicDebtNumberContent'])[0] as $value)
                                    @php
                                        $karmicDebtNumberArray[] = array_key_first($value);
                                    @endphp
                                @endforeach
                                <p class="font-weight-bold m-b-1" style="font-size:15px">19. CÁC {{mb_strtoupper(getIndicatorName(array_key_first($contents['karmicDebtNumberContent'])))}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{implode(', ', $karmicDebtNumberArray)}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Chỉ số này thể hiện các bài học cụ thể mà bạn cần chinh phục trong kiếp này vì đã không được học chúng ở kiếp trước. Mỗi chỉ số nợ nghiệp lại có bài học và gánh nặng riêng. Tất cả các nghiệp này đều có số 1 đứng đầu tức là đều do cái tôi mà ra. Nếu bạn học được cách bỏ đi cái tôi của mình và đức nhẫn nhịn sẽ khắc phục được rất nhiều các nhược điểm và nợ nghiệp cho kiếp sau bạn nhé!</p>

                                <div class="content" style="">
                                    <p class="text-center">Nội dung chưa cập nhật</p>
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="bieudosucmanh"><p class="font-weight-bold m-b-1" style="font-size:15px">20. BIỂU ĐỒ SỨC MẠNH CỦA BẠN (rất quan trọng)</p>
                                <p class="font-italic m-b-8 m-t-1 description" style="">Biểu đồ này còn được gọi là biểu đồ ngày sinh do được tạo ra từ ngày sinh, biểu đồ này thể hiện sức mạnh nguyên thủy của bạn. Nó cho thấy tổng quan các ưu nhược điểm về năng lực, sức mạnh của bạn (thể chất, tinh thần, trí tuệ) và cho biết rất nhiều các điểm mạnh điểm yếu của bạn. Tóm lại biểu đồ này giống như đề bài toán mà vũ trụ gửi cho bạn, bạn cần hiểu đề để giải được bài toán thì cuộc sống của bạn sẽ cân bằng và tốt đẹp hơn nhiều. Lưu ý rằng tên thường dùng của bạn có thể bù trừ vào những điểm yếu của biểu đồ này, vì vậy hãy tìm hiểu thêm biểu đồ tổng hợp để đặt một tên mới cho bạn nếu cần thiết. Tên mới này có thể dùng ở facebook, zalo,...</p><div style="background: #fff;padding:13px 5px 8px 5px;border-radius: 8px;">
                                    <table class="m-auto text-center font-weight-bold">
                                        <tbody><tr>
                                            <td class="border-b border-r bd"></td>
                                            <td class="border-b border-r bd"></td>
                                            <td class="border-b bd">999</td>
                                        </tr>
                                        <tr>
                                            <td class="border-b border-r bd"></td>
                                            <td class="border-b border-r bd">5</td>
                                            <td class="border-b bd"></td>
                                        </tr>
                                        <tr>
                                            <td class="border-r bd">1</td>
                                            <td class="border-r bd"></td>
                                            <td class="bd">7</td>
                                        </tr>
                                        </tbody></table>
                                    <p class="font-weight-bold m-t-10 m-b-10 text-center" style="font-size:14px">Biểu đồ sức mạnh (Biểu đồ ngày sinh) của bạn</p>
                                    <div class="content text-left m-b-0" style="">
                                    <p class="text-center">Nội dung chưa cập nhật</p>
                                    </div>
                                </div>
                            </div>
                            <div class="text-left m-t-10 box-bg" id="bieudotentonghop"><p class="font-weight-bold m-b-1" style="font-size:15px">21. BIỂU ĐỒ TÊN VÀ BIỂU ĐỒ TỔNG HỢP</p><div style="background: #fff;padding:13px 5px 8px 5px;border-radius: 8px;"><div class="row text-center"><div class="col-6"><table class="m-auto font-weight-bold">
                                                <tbody><tr>
                                                    <td class="border-b border-r bd red">33</td>
                                                    <td class="border-b border-r bd red"></td>
                                                    <td class="border-b bd red"></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-b border-r bd red"></td>
                                                    <td class="border-b border-r bd red"></td>
                                                    <td class="border-b bd red"></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-r bd red">1</td>
                                                    <td class="border-r bd red"></td>
                                                    <td class="bd red">7</td>
                                                </tr>
                                                </tbody></table><p class="m-t-8 font-italic font-weight-bold m-b-0" style="font-size: 12px;">Paul</p>
                                            <p class="font-italic m-b-8 m-t-2 description" style="">Biểu đồ này cho biết những sức mạnh do tên của bạn mang lại. Chủ yếu dùng tên đó để gộp với ngày sinh tạo ra biểu đồ tổng hợp bên cạnh.</p></div><div class="col-6"><table class="m-auto font-weight-bold">
                                                <tbody><tr>
                                                    <td class="border-b border-r bd"><span class="red">33</span></td>
                                                    <td class="border-b border-r bd"><span class="red"></span></td>
                                                    <td class="border-b bd">999<span class="red"></span></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-b border-r bd"><span class="red"></span></td>
                                                    <td class="border-b border-r bd">5<span class="red"></span></td>
                                                    <td class="border-b bd"><span class="red"></span></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-r bd">1<span class="red">1</span></td>
                                                    <td class="border-r bd"><span class="red"></span></td>
                                                    <td class="bd">7<span class="red">7</span></td>
                                                </tr>
                                                </tbody></table><p class="m-t-8 font-italic font-weight-bold m-b-0" style="font-size: 12px;">Biểu đồ tổng hợp</p>
                                            <p class="font-italic m-b-8 m-t-2 description" style="">Biểu đồ này thể hiện bù trừ của tên vào ngày sinh của bạn. Các con số của tên (màu đỏ) lấp đầy các khoảng trống trong biểu đồ ngày sinh là đẹp nhất.</p></div></div>
                                    <p class="content m-t-10 text-left" style=""><b>CHÚC MỪNG BẠN VÌ TÊN <span class="text-primary">Paul</span> ĐÃ GỘP VÀO NGÀY SINH CỦA BẠN VÀ TẠO THÊM:</b><br><br><em class="font-weight-bold">-  1 TRỤC NHẠY BÉN (3-5-7)</em><br>Trục này cho thấy bạn sẽ có khả năng thấu hiểu con người và nhạy bén trong việc nắm bắt tâm lý của người khác.<br><br><br><b class="text-danger"><i class="fas fa-exclamation-triangle"></i> TÊN CỦA BẠN CÒN CÁC ĐIỂM YẾU SAU:</b><br><span class="text-danger">- Chưa bù trừ được số 2 bị thiếu - con số của sự nhẹ nhàng, hợp tác và cảm xúc (biểu đồ của bạn còn có khả năng tạo được cặp số đẹp 22).<br>- Chưa bù trừ được số 4 bị thiếu - con số của sự cẩn thận và thực tế, kiên trì và chăm chỉ.<br>- Chưa bù trừ được số 6 bị thiếu - con số của trí tuệ cảm xúc và quan tâm người khác.<br>- Chưa bù trừ được số 8 bị thiếu - con số của khả năng nhạy bén tài chính và quyền lực.<br>- Chưa đủ Trục trí tuệ 369 - Trục này đại diện cho những gì liên quan đến đầu óc của con người. Trục này phụ trách trí nhớ, suy nghĩ, tư duy phân tích, lý trí, trí tưởng tượng, óc sáng tạo, tinh thần trách nhiệm, hoài bão/tham vọng và lý tưởng sống.<br>- Chưa đủ Trục cân bằng tinh thần 258 - Trục này đại diện cho những gì liên quan đến trái tim của con người. Trục này phụ trách toàn bộ cảm giác, bao gồm trực giác, tình yêu thương, sự tự do, các cảm xúc tích cực, các biểu đạt nghệ thuật, sự độc lập về tinh thần, và trí tuệ/sự thông thái.<br>- Chưa đủ Trục thể chất 147 - Trục này đại diện cho hoạt động của con người. Trục này quản lý lời nói, ngôn ngữ hình thể, động lực, óc tổ chức, tính kiên nhẫn, tính vật chất và sự lĩnh hội thông qua mất mát (học hỏi qua những lần vấp ngã).<br>- Chưa đủ Trục lập kế hoạch 123 - Trục này cho thấy một người có khả năng lập kế hoạch tốt và thường làm theo kế hoạch vạch ra, làm việc gì thường cũng có những bước định sẵn trong đầu rõ ràng.<br>- Chưa đủ Trục ý chí 456 - Trục này cho thấy một người rất giàu ý chí trước những khó khăn và thử thách trong cuộc sống, luôn muốn làm đến cùng và không ngại khó khăn trong hầu hết mọi việc, không muốn người khác nói những điều tiêu cực làm nhụt chí.<br>- Chưa đủ Trục hành động 789 - Trục này cho thấy một người có tính hành động và chủ động cao, ít dựa dẫm vào người khác. Trong khi người khác còn đang nói hoặc nghĩ thì đã lại muốn hành động luôn.</span><br><br><b class="text-danger">Nếu bạn muốn có một tên đẹp để bù trừ cho các điểm yếu trong biểu đồ sức mạnh và tên có sự tương hỗ tốt với các chỉ số thần số học khác (như đường đời, sứ mệnh, linh hồn, trưởng thành,...), bạn có thể nhờ chuyên gia của tracuuthansohoc đặt một danh xưng vừa ý (tên thường gọi, tên facebook, zalo,..).<br> Chi tiết về đặt tên và chi phí, bạn vui lòng xem tại link <a href="https://tracuuthansohoc.com/danhxung" target="_blank" class="text-primary">https://tracuuthansohoc.com/danhxung</a> hoặc liên hệ qua hotline: 0962984269 (zalo, call). Xin cảm ơn!</b></p></div></div><div class="text-left m-t-10 box-bg" id="thaido">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">22. {{mb_strtoupper($contents['approachAttitudeIndexContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['approachAttitudeIndexContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Số thái độ là một thành phần quan trọng của số học. Nó mô tả cách bạn thể hiện mình với phần còn lại của thế giới một cách tự nhiên, đặc biệt là trong những lần gặp gỡ đầu tiên. Mặc dù không phải lúc nào bạn cũng có thể nhận thức được hành động của mình, nhưng con số thái độ của bạn mô tả cách bạn hành động với người khác một cách tự nhiên (có thể hiểu giống như bản năng của bạn). --Một khi bạn hiểu về cách bạn cư xử tự nhiên với người khác và trong cuộc sống nói chung, bạn có thể thay đổi nó nếu bạn cảm thấy thích.</p>

                                <div class="content" style="">
                                    {!! $contents['approachAttitudeIndexContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="nangluctunhien">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">23. CHỈ SỐ {{mb_strtoupper($contents['naturalAbilityNumberContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['naturalAbilityNumberContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Chỉ số này cho bạn biết năng khiếu bẩm sinh, thứ mà bạn có thể làm dễ dàng, cũng như những tài năng và năng lực cụ thể sẽ hỗ trợ bạn trên đường đời.</p>

                                <div class="content" style="">
                                    {!! $contents['naturalAbilityNumberContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>"!!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="vuotkho">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">24. {{mb_strtoupper($contents['adversityQuotientContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['adversityQuotientContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Những người khác nhau phản ứng khác nhau với những thách thức mà họ phải đối mặt trong cuộc sống. Một số người có thói quen rút lui khỏi tình huống khó khăn để suy nghĩ thấu đáo vấn đề; những người khác rút lui khỏi cảm xúc của họ, để thoát khỏi cảm giác khó chịu. Một số người bùng nổ vì cảm xúc, nhưng để cơn bùng nổ trôi qua nhanh chóng, còn người khác nán lại với cảm xúc của họ, suy nghĩ nhiều về cảm xúc đó trong nội tâm.--Chỉ số này sẽ hướng dẫn bạn cách tốt nhất để đối phó với các tình huống khó khăn hoặc đe dọa. Bạn cũng biết về cách tốt nhất để sử dụng khả năng của mình để đối mặt với những thách thức khác nhau trong cuộc sống và cách đối phó với những thời điểm khó khăn.</p>

                                <div class="content" style="">
                                  {!! $contents['adversityQuotientContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="nangluctuduy">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">25. {{mb_strtoupper($contents['cognitiveAbilityIndexContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['cognitiveAbilityIndexContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Trong thần số học, chỉ số năng lực tư duy tiết lộ cách trí óc bạn hoạt động trong các tình huống khác nhau của cuộc sống. Bằng việc biết cách suy nghĩ của bạn, mọi người có thể hiểu được tính cách của bạn. Con số này cũng tiết lộ về mức độ thông minh và khả năng tư duy logic của bạn.</p>

                                <div class="content" style="">
                                    {!! $contents['cognitiveAbilityIndexContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="dongluctiepcan">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">26. {{mb_strtoupper($contents['approachMotivationIndexContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['approachMotivationIndexContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Chỉ số này thể hiện động lực của bạn để quyết định làm điều mới (người, sự vật, sự việc mới).</p>

                                <div class="content" style="">
                                    {!! $contents['approachMotivationIndexContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div class="text-left m-t-10 box-bg" id="nangluctiepcan">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">27. {{mb_strtoupper($contents['approachAbilityIndexContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['approachAbilityIndexContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Chỉ số này thể hiện khả năng tiếp cận, thiên thướng hành động của bạn khi gặp điều mới (người, sự vật, sự việc mới).</p>

                                <p class="content" style="">
                                    {!! $contents['approachAbilityIndexContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </p>
                            </div><div class="text-left m-t-10 box-bg" id="thaidotiepcan">
                                <p class="font-weight-bold m-b-1" style="font-size:15px">28. {{mb_strtoupper($contents['approachAttitudeIndexContent']->name??'')}} CỦA BẠN LÀ: <span class="text-success" style="font-weight: bold;">{{$contents['approachAttitudeIndexContent']->content_number??''}}</span></p>

                                <p class="font-italic m-b-8 m-t-2 description" style="">Chỉ số này thể hiện cách mà mọi người thường đánh giá về phản ứng của bạn với một điều mới (người, sự vật, sự việc mới).</p>

                                <div class="content" style="">
                                    {!! $contents['approachAttitudeIndexContent']->content??"<p class='text-center'>Nội dung chưa cập nhật</p>" !!}
                                </div>
                            </div><div style="font-size:13px;margin:auto; margin-top:10px; max-width: 600px;max-height: 900px">
                                <div
                                    style="font-size:13px;margin:auto; margin-top:10px; width: 100%; display: flex; justify-content: center;">
                                    <iframe src="https://pdfobject.com/pdf/sample.pdf" width="600px" height="700px">

                                    </iframe>
                                </div>

                            </div><div class="m-t-10 m-b-10 text-center"><button class="btn btn-success pdfDownload" code="1568843ydqm9GHdk"><i class="fas fa-download"></i> Lưu dạng file có thể in ấn</button></div></div></div>

                </div>
            </section>
        </div>

    </main>
@endsection
@section('script')
    <script src="{{asset('assets-kq1/Chart.bundle.js')}}"></script>
    <script>
        const ctx = document.getElementById('pychart').getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: {!! json_encode($contents['lifeCycleNumberContent']['4b8dd969-cf5a-48cd-ab14-63e18fa10bca']['xData'])??[] !!},
                datasets: [{
                    label: 'Chu kỳ vận số của bạn',
                    data: {!! json_encode($contents['lifeCycleNumberContent']['4b8dd969-cf5a-48cd-ab14-63e18fa10bca']['yData'])??[] !!},
                    borderColor: '#73ff6b',
                    pointBackgroundColor: 'white',
                    pointBorderColor: '#73ff6b',
                    pointRadius: 4,
                    pointHoverRadius: 7,
                    fill: false,
                    tension: 0.4
                }]
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            console.log(tooltipItem.index)
                            const value = tooltipItem.yLabel; // Giá trị tại mỗi điểm
                            const year = data.labels[tooltipItem.index]; // Năm tương ứng
                            return `Số: ${value}`; // Hiển thị thông tin tùy chỉnh
                        },
                    },
                },
                legend: {
                    display: false // Ẩn chú thích
                },
                title: {
                    display: false,
                    text: 'Chu kỳ vận số của bạn'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                return ''
                            },

                        },
                        scaleLabel: {
                            display: false,
                            labelString: 'Giá trị chu kỳ vận số'
                        },
                        gridLines: {
                            color: null
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: false,
                            labelString: 'Chu kỳ vận số của bạn',
                        },
                        gridLines: {
                            color: '#333333' // Đổi màu đường kẻ dọc thành đỏ
                        }
                    }]
                }
            }
        });
    </script>
@endsection
