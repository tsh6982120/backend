<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <title>UCON Social - New Connection New Social</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <!-- FAVICON -->
    <link rel="icon" href="{{asset('images/logo-32x32.png')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="https://ucon.social/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://ucon.social/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://ucon.social/favicon/favicon-16x16.png">
    {{--    <link rel="manifest" href="https://ucon.social/favicon/site.webmanifest">--}}
    <link rel="stylesheet" href="{{asset('assets-kq1/bootstrap.min.css')}}">
    <!-- STYLESHEET -->
    <link rel="stylesheet" media="all" href="{{asset('css/web/aos.css')}}" />
    <link rel="stylesheet" media="all" href="{{asset('css/web/endless-river.css')}}" />
    <link rel="stylesheet" media="all" href="{{asset('css/web/style.css')}}" />
    <link rel="stylesheet" media="all" href="{{asset('css/web/responsive.css')}}" />
    <link rel="stylesheet" media="all" href="{{asset('css/web/slick.css')}}" />
    <link rel="stylesheet" media="all" href="{{asset('css/web/slick-theme.css')}}" />
    <link rel="stylesheet" media="all" href="{{asset('css/web/custom.css')}}" />
    <!-- Google Analytics start -->
    <!-- Google Analytics end -->
    @yield('style')
</head>

<body id="home">
<div id="wrapper">
    <header class="header">
            <a href="/" class="logo"><img src="{{asset('images/logo.png')}}" alt="" /></a>
        <div class="h-gnavi">
            <ul class="menu">
                <li class="active">
                    <a href="/">Trang chủ</a>
                </li>
                <li>
                    <a href="#tra-cuu">Tra cứu</a>
                </li>
                <li>
                    <a href="#faq">FAQ</a>
                </li>
                <li class="has">
                    <a href="/">Mua Vip</a>
                </li>
            </ul>
            <ul class="h-btn">
                <li class="select">
                    <div class="lang-menu">
                        <div class="selected-lang">
                            Vietnam
                        </div>
                        <ul>
                            <li>
                                <a href="" class="de">English</a>
                            </li>
                            <li>
                                <a href="" class="vn">Vietnam</a>
                            </li>
                        </ul>
                    </div>
                    </nav>
                </li>
                <li class="btn">
                    <a href="#"><span class="ico"><img
                                src="{{asset('images/idx-ico-user.png')}}" alt=""></span>Join</a>
                </li>
            </ul>
        </div>
        <div id="menu-toggle">
            <img src="{{asset('images/idx-toogle.png')}}" alt="" />
        </div>
    </header>
    @yield('content')
    <footer class="footer">
        <div class="box-contact">
            <div class="fx-contact">
                <div class="contact-l">
                    <h2 class="idx-h2">CONTACT US</h2>
                    <p class="txt">No matter where you are, anytime<br> For any inquiries or assistance, feel free
                        to
                        reach out to us:<br> Email: <a href="" class="__cf_email__"
                                                       data-cfemail="44373134342b36300431272b2a6a372b272d2528">[email&#160;protected]</a>
                    </p>
                </div>
                <div class="contact-r">
                    <input class="w3-input" type="text" placeholder="Email">
                    <input class="w3-input" type="text" placeholder="Phone">
                    <p class="idx-btn">
                        <a href="">SEND</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="f-box01">
            <div class="f-fx-box01">
                <div class="f-left">
                    <ul class="f-menu">
                        <li>
                            <a href="https://docs.google.com/presentation/d/1aZ5B3nP3L6wmJfo_NKDLCRcmPxkTgCXDdFrew64-DlU/edit#slide=id.p">White Paper</a>
                        </li>
                        <li>
                            <a href="https://bscscan.com/token/0x1a8ae40f498aed05e3647933fbdad54ecfd2134b">UCON Token</a>
                        </li>
                        <li>
                            <a href="https://app.solidproof.io/projects/ucon-social">Audit</a>
                        </li>
                        <!-- <li>
                        <a href="#">Terms of Service</a>
                    </li>
                    <li>
                        <a href="#">Privacy Policy</a>
                    </li> -->
                    </ul>
                    <div class="f-fx-logo">
                        <p class="f-logo">
                            <a href="">
                                <img src="{{asset('images/logo.png')}}" alt="">
                            </a>
                        </p>
                        <ul class="f-list-social">
                            <li>
                                <a href="https://galxe.com/id/0xbD9ebC220f45f6C917eD6B72C2FE22Ec0924017A"
                                   target="_blank">
                                    <img src="{{asset('images/galxe.png')}}" alt="" width="20px">
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/Ucon_SocialFi" target="_blank">
                                    <img src="{{asset('images/twitter.svg')}}" alt="" width="20px">
                                </a>
                            </li>
                            <li>
                                <a href="https://discord.gg/qt46Txbg" target="_blank">
                                    <img src="{{asset('images/discord.svg')}}" alt="" width="20px">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/profile.php?id=61556004321324" target="_blank">
                                    <img src="{{asset('images/facebook.svg')}}" alt="" width="20px">
                                </a>
                            </li>
                            <!-- <li>
                            <a href="#">
                                <img src="images/idx-ico-f03.png" alt="">
                            </a>
                        </li> -->
                        </ul>
                    </div>
                    <p class="copyright">© 2024, UCON. All rights reserved.</p>
                </div>
                <div class="f-right">
                    <div class="f-box-menu">
                        <p class="f-ttl-menu">Menu</p>
                        <ul class="f-menu02">
                            <li>
                                <a href="">Trang chủ</a>
                            </li>
                            <li>
                                <a href="#tra-cuu">Tra cứu</a>
                            </li>
                            <li>
                                <a href="#faq">FAQ</a>
                            </li>
                            <li>
                                <a href="">Mua Vip</a>
                            </li>
                            <!-- <li>
                                <a href="index.html#">Document</a>
                            </li> -->
                            <!-- <li>
                            <a href="#">Blog</a>
                        </li> -->
                        </ul>
                    </div>
                    <div class="f-box-address">
                        <p class="f-ttl-menu">CONTACT US</p>
                        <p class="address"></p>
                        <p class="address02"><a href="" class="__cf_email__"
                                                data-cfemail="7c0f090c0c130e083c091f1312520f131f151d10">[email&#160;protected]</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="to-top">
            <a href="#wrapper">
                <img src="{{asset('images/totop.png')}}" alt="">
            </a>
        </div>
    </footer>
</div>

<script data-cfasync="false" src="{{asset('cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
<script src="{{asset('js/web/jquery.js')}}"></script>
<script src="{{asset('js/web/aos.js')}}"></script>
<script src="{{asset('js/web/endless-river.js')}}"></script>
<script src="{{asset('js/web/slick.min.js')}}"></script>
<script src="{{asset('js/web/common.js')}}"></script>

<script type="rocketlazyloadscript" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-content/themes/devvn-child/js/jquery.validate.min.js?ver=1.0"
        id="devvn-validate-js-js" defer=""></script>
<script type="rocketlazyloadscript" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-content/themes/devvn-child/js/sweetalert2.all.min.js?ver=1.0"
        id="devvn-sweetalert2-js-js" defer=""></script>
<script type="text/javascript" id="devvn-js-js-extra">
    /* <![CDATA[ */
    var devvn_theme_array = { "ajaxurl": "https:\/\/tracuuthansohoc.com\/wp-admin\/admin-ajax.php" };
    /* ]]> */
</script>
<script type="rocketlazyloadscript" data-minify="1" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-content/cache/min/1/wp-content/themes/devvn-child/js/devvn-main.js?ver=1715142157"
        id="devvn-js-js" defer=""></script>
<script type="rocketlazyloadscript" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-includes/js/dist/vendor/wp-polyfill-inert.min.js?ver=3.1.2"
        id="wp-polyfill-inert-js" defer=""></script>
<script type="rocketlazyloadscript" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.14.0"
        id="regenerator-runtime-js" defer=""></script>
<script type="rocketlazyloadscript" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0"
        id="wp-polyfill-js"></script>
<script type="text/javascript" id="flatsome-js-js-extra">
    /* <![CDATA[ */
    var flatsomeVars = { "theme": { "version": "3.16.6" }, "ajaxurl": "https:\/\/tracuuthansohoc.com\/wp-admin\/admin-ajax.php", "rtl": "", "sticky_height": "70", "assets_url": "https:\/\/tracuuthansohoc.com\/wp-content\/themes\/flatsome\/assets\/js\/", "lightbox": { "close_markup": "<button title=\"%title%\" type=\"button\" class=\"mfp-close\"><svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"28\" height=\"28\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x\"><line x1=\"18\" y1=\"6\" x2=\"6\" y2=\"18\"><\/line><line x1=\"6\" y1=\"6\" x2=\"18\" y2=\"18\"><\/line><\/svg><\/button>", "close_btn_inside": false }, "user": { "can_edit_pages": false }, "i18n": { "mainMenu": "Main Menu", "toggleButton": "Toggle" }, "options": { "cookie_notice_version": "1", "swatches_layout": false, "swatches_box_select_event": false, "swatches_box_behavior_selected": false, "swatches_box_update_urls": "1", "swatches_box_reset": false, "swatches_box_reset_extent": false, "swatches_box_reset_time": 300, "search_result_latency": "0" } };
    /* ]]> */
</script>
<script data-minify="1" type="text/javascript"
        src="{{asset('wp-content/cache/min/1/wp-content/themes/flatsome/assets/js/flatsome.js?ver=1715142157')}}" id="flatsome-js-js"
        defer=""></script>
<script type="rocketlazyloadscript" data-minify="1" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-content/cache/min/1/wp-content/themes/flatsome/inc/integrations/wp-rocket/flatsome-wp-rocket.js?ver=1715142157"
        id="flatsome-wp-rocket-js" defer=""></script>
<script type="rocketlazyloadscript" data-minify="1" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-content/cache/min/1/wp-content/themes/flatsome/inc/extensions/flatsome-live-search/flatsome-live-search.js?ver=1715142157"
        id="flatsome-live-search-js" defer=""></script>
<script type="rocketlazyloadscript" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-includes/js/comment-reply.min.js?ver=6.5.3"
        id="comment-reply-js" async="async" data-wp-strategy="async"></script>

<script type="rocketlazyloadscript" data-rocket-type="text/javascript"
        data-rocket-src="https://tracuuthansohoc.com/wp-content/plugins/wp-rocket/assets/js/heartbeat.js?ver=3.15.10"
        id="heartbeat-js" defer=""></script>
<script type="rocketlazyloadscript" data-rocket-type="text/javascript" id="wpsp-script-frontend"></script>
<script>window.lazyLoadOptions = [{ elements_selector: "img[data-lazy-src],.rocket-lazyload,iframe[data-lazy-src]", data_src: "lazy-src", data_srcset: "lazy-srcset", data_sizes: "lazy-sizes", class_loading: "lazyloading", class_loaded: "lazyloaded", threshold: 300, callback_loaded: function (element) { if (element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible") { if (element.classList.contains("lazyloaded")) { if (typeof window.jQuery != "undefined") { if (jQuery.fn.fitVids) { jQuery(element).parent().fitVids() } } } } } }, { elements_selector: ".rocket-lazyload", data_src: "lazy-src", data_srcset: "lazy-srcset", data_sizes: "lazy-sizes", class_loading: "lazyloading", class_loaded: "lazyloaded", threshold: 300, }]; window.addEventListener('LazyLoad::Initialized', function (e) {
        var lazyLoadInstance = e.detail.instance; if (window.MutationObserver) {
            var observer = new MutationObserver(function (mutations) {
                var image_count = 0; var iframe_count = 0; var rocketlazy_count = 0; mutations.forEach(function (mutation) {
                    for (var i = 0; i < mutation.addedNodes.length; i++) {
                        if (typeof mutation.addedNodes[i].getElementsByTagName !== 'function') { continue }
                        if (typeof mutation.addedNodes[i].getElementsByClassName !== 'function') { continue }
                        images = mutation.addedNodes[i].getElementsByTagName('img'); is_image = mutation.addedNodes[i].tagName == "IMG"; iframes = mutation.addedNodes[i].getElementsByTagName('iframe'); is_iframe = mutation.addedNodes[i].tagName == "IFRAME"; rocket_lazy = mutation.addedNodes[i].getElementsByClassName('rocket-lazyload'); image_count += images.length; iframe_count += iframes.length; rocketlazy_count += rocket_lazy.length; if (is_image) { image_count += 1 }
                        if (is_iframe) { iframe_count += 1 }
                    }
                }); if (image_count > 0 || iframe_count > 0 || rocketlazy_count > 0) { lazyLoadInstance.update() }
            }); var b = document.getElementsByTagName("body")[0]; var config = { childList: !0, subtree: !0 }; observer.observe(b, config)
        }
    }, !1)</script>
<script data-no-minify="1" async=""
        src="{{asset('wp-content/plugins/wp-rocket/assets/js/lazyload/17.8.3/lazyload.min.js')}}"></script>
<script>function lazyLoadThumb(e, alt, l) { var t = '<img data-lazy-src="https://i.ytimg.com/vi/ID/hqdefault.jpg" alt="" width="480" height="360"><noscript><img src="https://i.ytimg.com/vi/ID/hqdefault.jpg" alt="" width="480" height="360"></noscript>', a = '<button class="play" aria-label="play Youtube video"></button>'; if (l) { t = t.replace('data-lazy-', ''); t = t.replace('loading="lazy"', ''); t = t.replace(/<noscript>.*?<\/noscript>/g, ''); } t = t.replace('alt=""', 'alt="' + alt + '"'); return t.replace("ID", e) + a } function lazyLoadYoutubeIframe() { var e = document.createElement("iframe"), t = "ID?autoplay=1"; t += 0 === this.parentNode.dataset.query.length ? "" : "&" + this.parentNode.dataset.query; e.setAttribute("src", t.replace("ID", this.parentNode.dataset.src)), e.setAttribute("frameborder", "0"), e.setAttribute("allowfullscreen", "1"), e.setAttribute("allow", "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"), this.parentNode.parentNode.replaceChild(e, this.parentNode) } document.addEventListener("DOMContentLoaded", function () { var exclusions = ["logo-color-min", "e-nha-a-20221007021752-trrzz.png"]; var e, t, p, u, l, a = document.getElementsByClassName("rll-youtube-player"); for (t = 0; t < a.length; t++)(e = document.createElement("div")), (u = 'https://i.ytimg.com/vi/ID/hqdefault.jpg'), (u = u.replace('ID', a[t].dataset.id)), (l = exclusions.some(exclusion => u.includes(exclusion))), e.setAttribute("data-id", a[t].dataset.id), e.setAttribute("data-query", a[t].dataset.query), e.setAttribute("data-src", a[t].dataset.src), (e.innerHTML = lazyLoadThumb(a[t].dataset.id, a[t].dataset.alt, l)), a[t].appendChild(e), (p = e.querySelector(".play")), (p.onclick = lazyLoadYoutubeIframe) });</script>
@yield('script')
</body>

</html>
