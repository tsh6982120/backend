@extends('web.layout')
@section('style')
    <link rel="preload" crossorigin as="font" href="{{asset('wp-content/themes/flatsome/assets/css/icons/fl-icons.woff')}}" />
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.countup.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.sticky-sidebar.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.tooltips.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.vendors-popups.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.vendors-slider.js?ver=3.16.6')}}'>
    {{--    <script type="text/javascript" src="wp-includes/js/jquery/jquery.min.js?ver=3.7.1" id="jquery-core-js"--}}
    {{--            defer=""></script>--}}
    <script type="rocketlazyloadscript" data-rocket-type="text/javascript"
            data-rocket-src="https://tracuuthansohoc.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.4.1"
            id="jquery-migrate-js" defer=""></script>

    <meta property="og:image" content="https://ucon.social/favicon/favicon-32x32.png" />

@endsection
@section('content')
    <main style="min-height: 100vh; padding-top: 77px;">
        <div class="tsh-container">
            <section class="section devvn_box_banner dark" id="section_776687739">
                <div class="bg section-bg fill bg-fill bg-loaded fix-bg">
                </div>
                <div class="section-content relative">

                    <div class="row row-small align-middle devvn_group_banner" id="row-891879135">

                        <div id="col-2135319773" class="col pb-0 medium-4 small-12 large-4">
                            <div class="col-inner">
                                <div class="devvn_absolute_banner">
                                    <div class="devvn_image_absolute absolute devvn_image_1">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_2">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_3">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_4">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_5">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_0">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_408837868">
                                    <div class="img-inner dark">
                                        <img fetchpriority="high" decoding="async" width="350" height="501"
                                             src="https://ucon.social/images/idx-img-why02.png"
                                             class="attachment-original size-original"
                                             alt="e nha a 20221007021752 trrzz"
                                        />
                                    </div>

                                    <style>
                                        #image_408837868 {
                                            width: 100%;
                                        }
                                    </style>
                                </div>

                            </div>
                        </div>
                        <div id="col-1000238957" class="col pb-0 medium-8 small-12 large-8">
                            <div class="col-inner">


                                <div class="devvn_box_content">

                                    <h1 style="text-align: center;">TRA CỨU THẦN SỐ HỌC<br>
                                        KHÁM PHÁ BẢN THÂN CÙNG THẦY<strong>UCON</strong></h1>
                                    <ul class="devvn_list_check">
                                        <li class="devvn_icon_check"><span style="font-size: 14px;">Nhà sáng lập hệ
                                                    thống thần số học được ứng dụng phổ biến tại Việt Nam</span></li>
                                        <li class="devvn_icon_check"><span style="font-size: 13.5px;">Xem chi tiết
                                                    – chính xác – tin cậy</span></li>
                                    </ul>
                                </div>

                            </div>
                        </div>


                    </div>
                    <div class="row row-large align-equal devvn_list_counts" style="margin-top: 20px;" id="row-452476521">

                        <div id="col-1345521819" class="col medium-4 small-4 large-4">
                            <div class="col-inner" style="background-color:rgb(9, 44, 73);">


                                <div class="text-center devvn_item_count">

                                    <p><span class="count">26.564.889</span>+</p>
                                    <p>LƯỢT TRA CỨU<br>
                                    </p>
                                </div>

                            </div>

                            <style>
                                #col-1345521819>.col-inner {
                                    padding: 10px 10px 10px 10px;
                                }
                            </style>
                        </div>



                        <div id="col-814272645" class="col medium-4 small-4 large-4">
                            <div class="col-inner" style="background-color:rgb(9, 44, 73);">


                                <div class="text-center devvn_item_count">

                                    <p><span class="count">896.401</span>+</p>
                                    <p>BÁO CÁO XUẤT BẢN<br>
                                    </p>
                                </div>

                            </div>

                            <style>
                                #col-814272645>.col-inner {
                                    padding: 10px 10px 10px 10px;
                                }
                            </style>
                        </div>



                        <div id="col-186366292" class="col medium-4 small-4 large-4">
                            <div class="col-inner" style="background-color:rgb(9, 44, 73);">


                                <div class="text-center devvn_item_count">

                                    <p><span class="count">79.526</span>+</p>
                                    <p>HỌC VIÊN QUA CÁC<br>
                                        KHÓA HỌC<br>
                                    </p>
                                </div>

                            </div>

                            <style>
                                #col-186366292>.col-inner {
                                    padding: 10px 10px 10px 10px;
                                }
                            </style>
                        </div>


                    </div>
                    <div id="text-292538469" class="text hidden">

                        <h2>Xem luận giải lá số tử vi thần số học miễn phí theo tên và ngày sinh</h2>
                    </div>

                    <div class="row" id="row-638246335">

                        <div id="col-1844590508" class="col devvn_box_button small-12 large-12">
                            <div class="col-inner text-center">


                                <a href="#tra-cuu" target="_self"
                                   class="button primary devvn_scroll_form_tool" style="border-radius:99px;">
                                    <span>TRA CỨU CÁC CHỈ SỐ CỦA BẠN NGAY</span>
                                    <i class="icon-angle-right" aria-hidden="true"></i></a>

                            </div>

                            <style>
                                #col-1844590508>.col-inner {
                                    padding: 20px 0px 0px 0px;
                                }
                            </style>
                        </div>



                        <div id="col-1604658718" class="col devvn_box_image_mobile small-12 large-12">
                            <div class="col-inner">



                                <div class="devvn_absolute_banner">
                                    <div class="devvn_image_absolute absolute devvn_image_1">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_2">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_3">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_4">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_5">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                    <div class="devvn_image_absolute absolute devvn_image_0">
                                        <div class="devvn_image_inner">
                                            <div class="devvn_image_background fix-bg"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_144276376">
                                    <div class="img-inner dark">
                                        <img fetchpriority="high" decoding="async" width="350" height="501"
                                             src="https://ucon.social/images/idx-img-why02.png"
                                             class="attachment-original size-original"
                                             alt="e nha a 20221007021752 trrzz"
                                        />
                                    </div>

                                    <style>
                                        #image_144276376 {
                                            width: 100%;
                                        }
                                    </style>
                                </div>

                            </div>
                        </div>



                        <div id="col-1681391560" class="col pb-0 devvn_box_desc small-12 large-12">
                            <div class="col-inner">


                                <div id="text-2202271035" class="text">

                                    <p>Những nghiên cứu về Thần số học Pitago của UCON đang mang đến
                                        một làn sóng tích cực trong đại chúng Việt Nam. Không chỉ là các phân tích
                                        để giúp mỗi người tìm ra những tiềm năng thực sự và trả lời được câu hỏi
                                        mình là ai trong cuộc đời này, những nghiên cứu sâu và rộng khắp các lĩnh
                                        vực của thầy còn giúp hàng triệu người lựa chọn được con đường đi đúng đắn,
                                        giúp cho những người làm kinh doanh tìm ra định hướng phù hợp trong chiến
                                        lược và trong quản trị doanh nghiệp, quan hệ khách hàng.</p>

                                    <style>
                                        #text-2202271035 {
                                            text-align: center;
                                        }
                                    </style>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>


                <style>
                    #section_776687739 {
                        padding-top: 40px;
                        padding-bottom: 40px;
                    }

                    #section_776687739 .ux-shape-divider--top svg {
                        height: 150px;
                        --divider-top-width: 100%;
                    }

                    #section_776687739 .ux-shape-divider--bottom svg {
                        height: 150px;
                        --divider-width: 100%;
                    }
                </style>
            </section>

            <div id="tra-cuu" ></div>
            <section class="section devvn_box_quick_search dark" id="section_2142980242">
                <div class="bg section-bg fill bg-fill bg-loaded">
                </div>

                <div class="section-content relative">

                        <span class="scroll-to" data-label="Scroll to: #tra-cuu" data-bullet="false"
                              data-link="#tra-cuu" data-title="tra-cuu"><a name="tra-cuu"></a></span>
                    <div class="mb">

                        <div class="container section-title-container white-color">
                            <span class="idx-h2 aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">Công cụ xem Thần số học Online UCON</span>
                        </div>
                    </div>

                    <div class="row devvn_form" id="row-90644007">

                        <div id="col-908735670" class="col pb-0 small-12 large-12">
                            <div class="col-inner text-left">
                                <div style="text-align:left;">
                                    <form action="{{route('web.create.view')}}" method="post" id="formRDR">
                                        @csrf
                                        <div>
                                            <p style="margin-bottom:3px;font-weight: bold;">Họ tên khai sinh:</p>
                                            <div>
                                                <input type="text" name="fullname" value="" placeholder="Nhập họ tên" class="input-control">
                                                @error('fullname')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div>
                                            <p style="margin-bottom:3px;font-weight: bold;">Tên thường dùng nếu có (VD: Louis Nguyen,...)</p>
                                            <div style="width:66.6666%;float:left;">
                                                <div style="margin-right:10px;">
                                                    <input type="text" name="usually_name" value="" placeholder="Nhập tên thường dùng" class="input-control">
                                                    @error('usually_name')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div style="width:33.3333%;float:left;">
                                                <div class="input-group">
                                                    <select class="select-control" name="gender">
                                                        <option value="0">Giới tính</option>
                                                        <option value="1">Nam</option>
                                                        <option value="2">Nữ</option>
                                                        <option value="3">Khác</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                        <div>
                                            <p style="margin-bottom:3px;font-weight: bold;">Ngày/tháng/năm sinh dương lịch:</p>
                                            <div style="width:33.3333%;float:left;">
                                                <div style="margin-right:10px;">
                                                    <select name="day" class="select-control">
                                                        <option value="1" selected>Ngày 01</option>
                                                        <option value="2">Ngày 02</option>
                                                        <option value="3">Ngày 03</option>
                                                        <option value="4">Ngày 04</option>
                                                        <option value="5">Ngày 05</option>
                                                        <option value="6">Ngày 06</option>
                                                        <option value="7">Ngày 07</option>
                                                        <option value="8">Ngày 08</option>
                                                        <option value="9">Ngày 09</option>
                                                        <option value="10">Ngày 10</option>
                                                        <option value="11">Ngày 11</option>
                                                        <option value="12">Ngày 12</option>
                                                        <option value="13">Ngày 13</option>
                                                        <option value="14">Ngày 14</option>
                                                        <option value="15">Ngày 15</option>
                                                        <option value="16">Ngày 16</option>
                                                        <option value="17">Ngày 17</option>
                                                        <option value="18">Ngày 18</option>
                                                        <option value="19">Ngày 19</option>
                                                        <option value="20">Ngày 20</option>
                                                        <option value="21">Ngày 21</option>
                                                        <option value="22">Ngày 22</option>
                                                        <option value="23">Ngày 23</option>
                                                        <option value="24">Ngày 24</option>
                                                        <option value="25">Ngày 25</option>
                                                        <option value="26">Ngày 26</option>
                                                        <option value="27">Ngày 27</option>
                                                        <option value="28">Ngày 28</option>
                                                        <option value="29">Ngày 29</option>
                                                        <option value="30">Ngày 30</option>
                                                        <option value="31">Ngày 31</option>
                                                    </select>
                                                    @error('day')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div style="width:33.3333%;float:left;">
                                                <div style="margin-right:10px;">
                                                    <select name="month" class="select-control">
                                                        <option value="1" selected>Tháng 01</option>
                                                        <option value="2">Tháng 02</option>
                                                        <option value="3">Tháng 03</option>
                                                        <option value="4">Tháng 04</option>
                                                        <option value="5">Tháng 05</option>
                                                        <option value="6">Tháng 06</option>
                                                        <option value="7">Tháng 07</option>
                                                        <option value="8">Tháng 08</option>
                                                        <option value="9">Tháng 09</option>
                                                        <option value="10">Tháng 10</option>
                                                        <option value="11">Tháng 11</option>
                                                        <option value="12">Tháng 12</option>
                                                    </select>
                                                    @error('month')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div style="width:33.3333%;float:left;">
                                                <input type="number" name="year" value="" placeholder="Năm sinh" class="input-control">
                                                @error('year')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div style="clear:both;"></div>
                                        </div>
                                        @error('birthday')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror

                                        <div style="margin-top:20px">
                                            <button class="button_form_custom" type="submit"><i
                                                    class="fas fa-search"></i> Tra cứu ngay</button>
                                        </div>
                                    </form>
                                </div>
                                <p><strong>Chú thích:</strong></p>
                                <p>Nếu ngày sinh trên giấy tờ (chứng minh thư, bằng lái, khai sinh…) của bạn
                                    khác với ngày sinh dương lịch thật thì cuộc đời bạn sẽ có sự xáo trộn từ cả 2
                                    ngày sinh này. Bạn nên tra cứu cả 2 để biết thêm chi tiết, tuy nhiên kết quả sẽ
                                    thiên về ngày sinh dương lịch thật!</p>
                                <p>Tên thường dùng là tên mà mọi người thường gọi bạn hoặc một danh xưng bạn thường
                                    dùng, tên này sẽ bù trừ vào biểu đồ ngày sinh của bạn. Nếu bạn không có tên
                                    thường dùng, hệ thống sẽ tự lấy họ tên khai sinh của bạn để tính toán trong biểu
                                    đồ tổng hợp.</p>
                                <p>Số chủ đạo tuy rất quan trọng nhưng không thể hiện hết thông tin thần số học của
                                    bạn. Để xem kết quả tra cứu chính xác, hãy kết hợp tất cả các chỉ số mà chúng
                                    tôi tính toán!</p>
                            </div>

                            <style>
                                #col-908735670>.col-inner {
                                    max-width: 960px;
                                }
                            </style>
                        </div>


                    </div>
                </div>


                <style>
                    #section_2142980242 {
                        padding-top: 180px;
                        padding-bottom: 180px;
                        background-color: rgb(0, 0, 0);
                    }

                    #section_2142980242 .section-bg.bg-loaded {
                        background-image: url({{asset('wp-content/uploads/2022/10/650-20221007033516-af-dr-1.png')}});
                    }

                    #section_2142980242 .ux-shape-divider--top svg {
                        height: 150px;
                        --divider-top-width: 100%;
                    }

                    #section_2142980242 .ux-shape-divider--bottom svg {
                        height: 150px;
                        --divider-width: 100%;
                    }
                </style>
            </section>


            <div id="faq" ></div>
            <section class="section devvn_box_share_numerology dark" id="section_971185955">
                <div class="bg section-bg fill bg-fill bg-loaded">
                </div>
                <div class="section-content relative">

                    <div class="row devvn_content_shawdown" id="row-1366923642">
                        <div id="col-649598526" class="col small-12 large-12">
                            <div class="col-inner text-center">
                                <div class="container section-title-container">
                                    <div class="section-title section-title-center" style="display: flex; justify-content: center;">
                                                    <span class="idx-h2 aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500" >ĐÔI LỜI CHIA SẺ VỀ THẦN SỐ HỌC Miễn
                                                        phí</span>
                                    </div>
                                </div>
                                <div class="accordion">
                                    <div id="accordion-3469006927" class="accordion-item"><a
                                            id="accordion-3469006927-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-3469006927-content"><button class="toggle"
                                                                                                 aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Thần số học là
                                                    gì?</span></a>
                                        <div id="accordion-3469006927-content" class="accordion-inner"
                                             aria-labelledby="accordion-3469006927-label">
                                            <p><strong>Thần số học</strong> (nhân số học) là một lĩnh vực nghiên cứu
                                                và giải mã con người trong phạm trù huyền học. Bộ môn này đi sâu
                                                nghiên cứu về ý nghĩa, mối liên hệ giữa các chữ số và con người.</p>
                                            <p>Mỗi con số đại diện cho một lĩnh vực thuộc nhận thức và kinh nghiệm
                                                của con người. Theo Nhân số học, chỉ có bộ số tự nhiên 1, 2, 3, 4,
                                                5, 6, 7, 8, 9 và hai số chính 11, 22 được sử dụng. Trong đó, con số
                                                chủ đạo nhiều hơn một chữ số đại diện cho cấp độ cao hơn về tiềm
                                                năng của con người so với các số đơn lẻ.</p>
                                            <p><em>Để hiểu hơn về nội dung này, mời bạn tham khảo bài viết:
                                                    <strong><a href="">Thần Số Học Là Gì
                                                            ? Khám Phá Bản Thân Qua Những Con Số</a></strong></em>
                                            </p>
                                        </div>
                                    </div>
                                    <div id="accordion-2461961437" class="accordion-item"><a
                                            id="accordion-2461961437-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-2461961437-content"><button class="toggle"
                                                                                                 aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Khám phá bản thân
                                                    theo nhân số học</span></a>
                                        <div id="accordion-2461961437-content" class="accordion-inner"
                                             aria-labelledby="accordion-2461961437-label">
                                            <p><strong>Thần số học là bộ môn nghiên cứu về năng lượng rung động của
                                                    các con số và tác động của chúng với cá nhân mỗi người. Đây là
                                                    lĩnh vực huyền học đơn giản để bạn có thể tiếp thu và nắm vững.
                                                </strong></p>
                                            <p>Sử dụng Nhân số học, bạn có thể khám phá điểm mạnh và điểm yếu, tài
                                                năng, nhu cầu nội tâm, phản ứng cảm xúc, cách đối nhân xử thế của
                                                một người. Bạn có thể tự cải thiện bản thân bằng cách nhận thức được
                                                tính cách của chính mình. Đồng thời, qua xem thần số học miễn phí
                                                bạn cũng có thể học cách hiểu và ứng xử với những người khác như gia
                                                đình, bạn bè, người yêu, lãnh đạo, cấp dưới. Bạn có thể tìm ra năng
                                                lượng tiềm ẩn, khó khăn, áp lực hiện đang tồn tại trong cuộc sống
                                                của mình và trong cuộc sống của những người xung quanh. Bạn cũng có
                                                thể xác định những thời điểm tốt nhất cho các dấu mốc quan trọng của
                                                cuộc đời như kết hôn, thay đổi công việc, di chuyển, suy đoán, đi du
                                                lịch.</p>
                                            <p>Trong hầu hết lĩnh vực, những nhà nghiên cứu đã tìm thấy mối tương
                                                quan giữa một số hiện tượng vật lý có thể quan sát rõ ràng và các
                                                hoạt động bên trong của con người. Nói cách khác, vũ trụ dường như
                                                đã cung cấp nhiều chìa khóa để khai mở bản chất con người. Trong
                                                chiêm tinh học, chìa khóa là mối liên hệ giữa vị trí của các hành
                                                tinh khi một người sinh ra với các đặc điểm của người đó; trong sinh
                                                trắc vân tay, chìa khóa là mối quan hệ giữa đường chỉ tay với đặc
                                                điểm tính cách; còn với Thần số học Pitago, chìa khóa này là mối
                                                liên hệ giữa tên và ngày sinh của một người với bản chất của người
                                                đó. Đáng chú ý là tất cả các bản báo cáo, nghiên cứu thuộc các lĩnh
                                                vực đều nhất quán với nhau – các báo cáo khác nhau thường có
                                                thông tin tương tự hoặc bao gồm các khía cạnh khác nhau, nhưng có
                                                liên quan đến đặc điểm của một người.</p>
                                            <p>Đây là Tracuuthansohoc.com, mục tiêu hàng đầu của chúng tôi là
                                                hỗ trợ bạn trong hành trình nhận thức sâu sắc về bản ngã
                                                và trở nên tự tin hơn. Khi xem thần số học miễn phí, bạn sẽ
                                                không chỉ tìm thấy các biểu đồ chỉ số bí mật cho riêng
                                                mình, mà còn sử dụng vô số các công cụ khác để phát triển
                                                bản thân. Đội ngũ tuyệt vời của chúng tôi bao gồm các nhà
                                                vật lý học, chuyên gia huyền học, chúng tôi ở đây để giúp
                                                bạn hiểu về Thần số học Pitago và truyền cảm hứng về bộ
                                                môn này. Từ đó bạn có thể cải thiện và phát triển bản thân tốt hơn.
                                            </p>
                                        </div>
                                    </div>
                                    <div id="accordion-1684232570" class="accordion-item"><a
                                            id="accordion-1684232570-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-1684232570-content"><button class="toggle"
                                                                                                 aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Tracuuthansohoc có
                                                    thể giúp gì cho bạn?</span></a>
                                        <div id="accordion-1684232570-content" class="accordion-inner"
                                             aria-labelledby="accordion-1684232570-label">
                                            <p>Sứ mệnh của chúng tôi ở đây là tạo ra một công cụ tính đầy
                                                đủ các chỉ số và giải thích ý nghĩa các chỉ số của
                                                thần số học. Tracuuthansohoc muốn đem một công cụ tính toán tiềm
                                                năng của bản thân đến với mọi người với một mức phí phù hợp!
                                                Tracuuthansohoc.com không chỉ tính một chỉ số! Chúng tôi có
                                                đội ngũ các chuyên gia kết hợp với nhau để chỉ ra hơn 20
                                                chỉ số và sự kết hợp của chúng cho riêng bạn.</p>
                                            <table style="border-collapse: collapse; width: 100%;">
                                                <tbody>
                                                <tr>
                                                    <td style="width: 50%; text-align: center;"><strong>Công
                                                            cụ</strong></td>
                                                    <td style="width: 50%; text-align: center;"><strong>Chi
                                                            tiết</strong></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;"><strong>Định hướng nghề nghiệp theo
                                                            Thần Số Học</strong></td>
                                                    <td style="width: 50%;">Đây là công cụ được thầy Louis
                                                        Nguyễn sáng lập với mong muốn giúp những người đang gặp
                                                        bế tắc trong công việc, chưa xác định được hướng đi cho
                                                        bản thân. Qua <strong><a
                                                                href="">thần số
                                                                học nghề nghiệp</a></strong>, họ cũng sẽ khai
                                                        phá được năng lực thực sự của bản thân, phát triển tối
                                                        đa điểm mạnh để xây dựng một sự nghiệp thành công.</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;"><strong>Xem Thần số học cho
                                                            con</strong></td>
                                                    <td style="width: 50%;">Xem <strong><a
                                                                href="">thần số học
                                                                cho con</a></strong> là sản phẩm mới và vô cùng
                                                        tuyệt vời dành cho các bậc làm cha mẹ trong việc tìm
                                                        hiểu, khai phá những ưu – nhược điểm của bé. Qua
                                                        thần số học cho bé, cha mẹ có những định hướng, lời
                                                        khuyên đúng đắn trong từng giai đoạn phát triển của
                                                        trẻ.&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;"><strong>Xem Thần Số Học trọn
                                                            đời</strong></td>
                                                    <td style="width: 50%;">Báo cáo Thần Số Học Trọn Đời là một
                                                        sản phẩm tuyệt vời của Tracuuthansohoc. Trong bản báo
                                                        này phân tích cho bạn cụ thể từng năm với các mốc đỉnh
                                                        cao và thử thách của bản thân. Từ đó, bạn có thể biết
                                                        được hướng đi của mình theo từng năm phù hợp với sự phát
                                                        triển tiềm năng của bản thân.</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;"><strong>Đặt tên khai sinh cho con
                                                            theo Thần Số Học</strong></td>
                                                    <td style="width: 50%;">Tên gọi là âm thanh được nhắc đến
                                                        thường xuyên và đi theo mỗi người chúng ta trong suốt
                                                        cuộc đời. Họ tên trong Nhân Số Học được quy đổi ra các
                                                        con số với các năng lượng sóng rung tương ứng. Khi tên
                                                        gọi kết hợp với ngày sinh sẽ tác động đến cuộc đời và
                                                        định hình số phận mỗi người sau này.
                                                        <p>Do vậy, việc lựa chọn <strong><a
                                                                    href="">đặt tên
                                                                    con trai</a></strong> hay <strong><a
                                                                    href="">đặt tên con
                                                                    gái</a> </strong>phù hợp với ngày sinh cho
                                                            con theo Nhân Số Học rất quan trọng. Với công cụ <a
                                                                href=""><strong>đặt
                                                                    tên cho con theo Thần Số Học</strong></a>
                                                            tại Tracuuthansohoc.com, bạn hoàn toàn có thể yên
                                                            tâm lựa chọn cho con cái tên ưng ý.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;"><strong>Đặt tên danh xưng Thần Số
                                                            Học</strong></td>
                                                    <td style="width: 50%;">Trong biểu đồ sức mạnh Thần số học,
                                                        nếu các con số trong ngày sinh và họ tên không tạo thành
                                                        các trục thì bạn sẽ thiếu đi các năng lực giá trị bẩm
                                                        sinh của mình. Bên cạnh việc phải rèn luyện nhiều thì
                                                        tên danh xưng có thể giúp bạn bù được các trục bị thiếu
                                                        của mình.</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row" id="row-1196481279">

                        <div id="col-603721209" class="col small-12 large-12">
                            <div class="col-inner">


                                <div class="container section-title-container">
                                    <div class="section-title section-title-center" style="display: flex; justify-content: center;">
                                            <span class="idx-h2 aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500" >KIẾN THỨC THÚ VỊ TRONG THẦN SỐ HỌC
                                                PYTHAGORAS</span>
                                    </div>
                                </div>
                                <div class="accordion">
                                    <div id="accordion-2791119067" class="accordion-item"><a
                                            id="accordion-2791119067-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-2791119067-content"><button class="toggle"
                                                                                                 aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Con số chủ đạo (Chỉ
                                                    đường đời)</span></a>
                                        <div id="accordion-2791119067-content" class="accordion-inner"
                                             aria-labelledby="accordion-2791119067-label">
                                            <p><strong><a href="">Số chủ đạo</a></strong>
                                                chỉ đơn giản là rút gọn của toàn bộ ngày sinh của bạn. Số đường đời
                                                này cho thấy bạn là ai và những đặc điểm mà bạn có ngay từ khi được
                                                sinh ra. Đường đời là con số quan trọng mà chúng ta sẽ bàn đến trong
                                                nhân số học. Đó là bản chất của hành trình cuộc đời bạn. Đa số các
                                                nhà số học tin rằng mọi người đều có tiền kiếp. Trước khi được sinh
                                                ra trong cuộc sống hiện tại này, bạn phải dành thời gian để nhìn lại
                                                cuộc sống trước đây của mình trong khi tìm ra những gì bạn muốn học
                                                trong lần này. Điều này có nghĩa là ngày sinh của bạn là sự kết hợp
                                                của các số mà bạn đã chọn. Bạn hoàn toàn có thể tìm được con số chủ
                                                đạo của mình khi xem thần số học miễn phí tại hệ thống
                                                Tracuuthansohoc.com</p>
                                        </div>
                                    </div>
                                    <div id="accordion-249779312" class="accordion-item"><a
                                            id="accordion-249779312-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-249779312-content"><button class="toggle"
                                                                                                aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Biểu đồ ngày
                                                    sinh</span></a>
                                        <div id="accordion-249779312-content" class="accordion-inner"
                                             aria-labelledby="accordion-249779312-label">
                                            <p><strong><a href="">Biểu đồ ngày
                                                        sinh</a></strong> có thể được coi như chiếc chìa khóa để mở
                                                cánh cửa khám phá tiềm năng tận cùng ở sâu bên trong mỗi người. Dựa
                                                trên nguyên tắc của nhà số học Pythagoras, mỗi ngày sinh khác nhau
                                                tương ứng với một biểu đồ ngày sinh nhất định.</p>
                                        </div>
                                    </div>
                                    <div id="accordion-1219632354" class="accordion-item"><a
                                            id="accordion-1219632354-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-1219632354-content"><button class="toggle"
                                                                                                 aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Năm cá
                                                    nhân</span></a>
                                        <div id="accordion-1219632354-content" class="accordion-inner"
                                             aria-labelledby="accordion-1219632354-label">
                                            <p>Thần số học (Nhân số học) hoạt động theo vòng chu kỳ 9 năm. Mỗi một
                                                năm trong chu kỳ 9 năm đó đều có đặc điểm và nhu cầu riêng. Do đó,
                                                mỗi người cần nắm được những ý nghĩa và giá trị ở trong từng năm để
                                                biết mình đang ở đâu và mình cần làm gì để mở ra những bí ẩn trong
                                                cuộc đời. Nói cách khác, <strong><a
                                                        href="">năm cá nhân trong
                                                        Thần Số Học</a></strong> được đúc kết từ năm sinh và chu kì
                                                vận số của từng người. Qua chỉ số này, bạn sẽ biết được năm nay bạn
                                                sẽ có tính cách như thế nào, bạn cần làm gì trong năm nay là phù
                                                hợp… Tuy nhiên, mỗi một năm thì con số năm cá nhân mỗi người đều
                                                thay đổi bởi sự ảnh hưởng của năng lượng chung của năm thế giới.</p>
                                            <p>⏩⏩⏩ Xem ngay bài viết: <a
                                                    href=""><strong>Năm Thế
                                                        Giới Thần Số Học 2023: Thức Tỉnh Linh Hồn Và Khát
                                                        Vọng</strong></a></p>
                                        </div>
                                    </div>
                                    <div id="accordion-3803615236" class="accordion-item"><a
                                            id="accordion-3803615236-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-3803615236-content"><button class="toggle"
                                                                                                 aria-label="Toggle"><i class="icon-angle-down"></i></button><span>4
                                                    đỉnh cao đời người trong thần số học Pitago</span></a>
                                        <div id="accordion-3803615236-content" class="accordion-inner"
                                             aria-labelledby="accordion-3803615236-label">
                                            <p>Biểu đồ kim tự tháp trong Thần số học tương ứng với 4 đỉnh cao của
                                                đời người theo chu kỳ 9 năm. Biểu đồ này được chia làm 3 chu kỳ, mỗi
                                                chu kỳ tương ứng với giai đoạn 27 năm. Vào mỗi cuối chu kỳ 9 năm, cá
                                                nhân mỗi người đều sẽ gặt hái được những thành công mà con số ở mỗi
                                                đỉnh kim tự tháp đem lại. Ngoài ra, biểu tượng của kim tự tháp còn
                                                thể hiện những khát vọng tiềm ẩn bên trong mỗi con người.</p>
                                        </div>
                                    </div>
                                    <div id="accordion-2404611230" class="accordion-item"><a
                                            id="accordion-2404611230-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-2404611230-content"><button class="toggle"
                                                                                                 aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Các chỉ số trong thần
                                                    số học và ý nghĩa của chúng</span></a>
                                        <div id="accordion-2404611230-content" class="accordion-inner"
                                             aria-labelledby="accordion-2404611230-label">
                                            <table style="border-collapse: collapse; width: 100%; height: 105px;">
                                                <tbody>
                                                <tr style="height: 21px;">
                                                    <td style="width: 50%; height: 21px;"><strong>Chỉ
                                                            số</strong></td>
                                                    <td style="width: 50%; height: 21px;"><strong>Ý
                                                            nghĩa</strong></td>
                                                </tr>
                                                <tr style="height: 21px;">
                                                    <td style="width: 50%; height: 21px;"><strong>Số sứ
                                                            mệnh</strong></td>
                                                    <td style="width: 50%; height: 21px;">Số sứ mệnh được tìm
                                                        thấy bằng cách sử dụng ánh xạ ra số trong tên khai sinh.
                                                        Nó tiết lộ những khuyết điểm, khả năng và tài năng của
                                                        bạn mà bạn sẽ trải qua trong cuộc đời. Tại sao sứ mệnh
                                                        lại sử dụng tên của bạn? Tên của bạn cho biết lịch sử cá
                                                        nhân của bạn cho đến thời điểm bạn sinh ra. Dù lịch sử
                                                        của bạn là gì, nó đã hun đúc bạn thành con người của
                                                        bạn. Cha mẹ của bạn, trước khi bạn đến với cuộc sống, đã
                                                        nắm bắt những rung động của bạn và chọn tên của bạn cho
                                                        phù hợp. Tên khai sinh của bạn là những gì bạn muốn nhận
                                                        và thể hiện cái tôi ra ngoài thế giới xung quanh</td>
                                                </tr>
                                                <tr style="height: 21px;">
                                                    <td style="width: 50%; height: 21px;"><strong>Chỉ số tính
                                                            cách</strong></td>
                                                    <td style="width: 50%; height: 21px;">Đây là chỉ số cho biết
                                                        ấn tượng của người khác đối với bạn là gì. Đó là biểu
                                                        hiện bên ngoài của bạn. Tính cách bên ngoài (nhân cách)
                                                        của bạn sẽ được phản ánh qua hành vi, thái độ, phản ứng
                                                        của bạn, cho dù có ý thức hay là vô thức.</td>
                                                </tr>
                                                <tr style="height: 21px;">
                                                    <td style="width: 50%; height: 21px;"><strong>Chỉ số linh
                                                            hồn</strong></td>
                                                    <td style="width: 50%; height: 21px;">Đây là con số nói lên
                                                        mong muốn của trái tim mỗi con người. Nó có tên gọi khác
                                                        là Heart’s Desire Number (Số mong muốn trái tim), hiểu
                                                        được <strong><a href="">chỉ số
                                                                linh hồn</a></strong> của một cá nhân, bạn sẽ
                                                        rất dễ kết thân với người đó. Đó là nơi ước mơ, mong
                                                        muốn, khao khát và động lực thực sự của mỗi hành động.
                                                    </td>
                                                </tr>
                                                <tr style="height: 21px;">
                                                    <td style="width: 50%; height: 21px;"><strong>Chỉ số trưởng
                                                            thành</strong></td>
                                                    <td style="width: 50%; height: 21px;">Bạn sẽ không cảm nhận
                                                        được con số trưởng thành một cách mạnh mẽ cho đến khi
                                                        bạn bước qua tuổi 40 và nó sẽ không được hiện thực hóa
                                                        cho đến khi bạn khoảng 50 tuổi. Con số này là nơi con
                                                        người thật của bạn được tìm thấy và cuối cùng có thể
                                                        giúp bạn cảm thấy thoải mái trong trái tim của chính
                                                        mình. Con số này cho biết nửa sau của cuộc đời bạn sẽ
                                                        như thế nào.</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="accordion-756099654" class="accordion-item"><a
                                            id="accordion-756099654-label" href="#" class="accordion-title plain"
                                            aria-expanded="false"
                                            aria-controls="accordion-756099654-content"><button class="toggle"
                                                                                                aria-label="Toggle"><i
                                                    class="icon-angle-down"></i></button><span>Công cụ bói tình yêu
                                                    theo thần số học</span></a>
                                        <div id="accordion-756099654-content" class="accordion-inner"
                                             aria-labelledby="accordion-756099654-label">
                                            <p>Công cụ <strong><a href="">bói tình
                                                        yêu</a></strong> theo tên và ngày sinh sử dụng phương pháp
                                                thần số học dự đoán chính xác độ hòa hợp giữa hai người chỉ bằng 2
                                                cái tên. Xem ngay!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <style>
                    #section_971185955 {
                        padding-top: 0;
                        padding-bottom: 0;
                        background-color: rgb(255, 255, 255);
                    }

                    #section_971185955 .section-bg.bg-loaded {
                        background-image: url(https://xem.tracuuthansohoc.com/uploads/images/lifepath-bg.jpg);
                    }

                    #section_971185955 .ux-shape-divider--top svg {
                        height: 150px;
                        --divider-top-width: 100%;
                    }

                    #section_971185955 .ux-shape-divider--bottom svg {
                        height: 150px;
                        --divider-width: 100%;
                    }
                </style>
            </section>
        </div>

    </main>
@endsection

