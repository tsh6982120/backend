@extends('web.layout')
@section('style')
    <link rel="preload" crossorigin as="font" href="{{asset('wp-content/themes/flatsome/assets/css/icons/fl-icons.woff')}}" />
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.countup.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.sticky-sidebar.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.tooltips.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.vendors-popups.js?ver=3.16.6')}}'>
    <link rel='prefetch' href='{{asset('wp-content/themes/flatsome/assets/js/chunk.vendors-slider.js?ver=3.16.6')}}'>
    {{--    <script type="text/javascript" src="wp-includes/js/jquery/jquery.min.js?ver=3.7.1" id="jquery-core-js"--}}
    {{--            defer=""></script>--}}
    <script type="rocketlazyloadscript" data-rocket-type="text/javascript"
            data-rocket-src="https://tracuuthansohoc.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.4.1"
            id="jquery-migrate-js" defer=""></script>

    <meta property="og:image" content="https://ucon.social/favicon/favicon-32x32.png" />

@endsection
@section('content')
    <main style="min-height: 100vh; padding-top: 300px;">
        <div class="tsh-container container">
            <div class="col-md-12 text-center text-white h-100">
                <h1>404</h1>
                <h2>Page Not Found</h2>
                <p>
                    Sorry, the page you are looking
                    for does not exist.
                </p>
            </div>
        </div>
    </main>
@endsection
