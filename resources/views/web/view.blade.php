@extends('web.layout')
@section('style')
    <link rel="stylesheet" href="{{asset('assets-kq1/bootstrap.min.css')}}">
    <link href="{{asset('assets-kq1/font-awesome-all.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-kq1/util.css')}}">
    <link href="{{asset('assets-kq1/search_style_v12.css')}}" rel="stylesheet">
{{--    <script async="" src="{{asset('assets-kq1/fbevents.js')}}"></script>--}}
{{--    <script src="{{asset('assets-kq1/sdk.js')}}" async="" crossorigin="anonymous"></script>--}}
    <script src="{{asset('assets-kq1/jQuery3-1-1.min.js')}}"></script>
@endsection
@section('content')
    <main style="min-height: 100vh; padding-top: 100px;">
        <div class="tsh-kq-1-container" style="margin-bottom: 100px;">
            <section class="content-wrapper">
                <div id="bg-nums" class="bg-nums">
                    <div class="left"
                         style="font-size: 22px; opacity: 2.42482; transform: translate3d(316.296px, 270.346px, 0.01px); visibility: inherit;">
                        2</div>
                    <div class="right"
                         style="font-size: 17px; opacity: 0.15747; transform: translate3d(1687.02px, 275.421px, 0.01px); visibility: inherit;">
                        8</div>
                    <div class="left"
                         style="font-size: 29px; opacity: 1.82863; transform: translate3d(448.882px, 398.884px, 0.01px); visibility: inherit;">
                        0</div>
                    <div class="right"
                         style="font-size: 22px; opacity: 0.333084; transform: translate3d(1597.99px, 228.303px, 0.01px); visibility: inherit;">
                        8</div>
                    <div class="left"
                         style="font-size: 18px; opacity: 0.0198429; transform: translate3d(402.674px, 413.965px, 0.01px); visibility: inherit;">
                        H</div>
                    <div class="right"
                         style="font-size: 14px; opacity: 0.843251; transform: translate3d(1439.46px, 659.934px, 0.01px); visibility: inherit;">
                        O</div>
                    <div class="left"
                         style="font-size: 14px; opacity: 0.0152504; transform: translate3d(542.112px, 491.637px, 0.01px); visibility: inherit;">
                        A</div>
                    <div class="right"
                         style="font-size: 26px; opacity: 0.294127; transform: translate3d(1296.69px, 345.637px, 0.01px); visibility: inherit;">
                        N</div>
                    <div class="left"
                         style="font-size: 28px; opacity: 0.119893; transform: translate3d(483.439px, 308.078px, 0.01px); visibility: inherit;">
                        2</div>
                    <div class="right"
                         style="font-size: 16px; opacity: 0.831457; transform: translate3d(1508.39px, 486.293px, 0.01px); visibility: inherit;">
                        0</div>
                    <div class="left"
                         style="font-size: 25px; opacity: 2.1263; transform: translate3d(108.648px, 567.151px, 0.01px); visibility: inherit;">
                        0</div>
                    <div class="right"
                         style="font-size: 16px; opacity: 0.243824; transform: translate3d(1510.51px, 308.219px, 0.01px); visibility: inherit;">
                        0</div>
                </div>
                <div id="result-content">
                    <div class="content-container greeting-lifepath-container animated fadeIn anim active m-t-20"
                         style="animation-play-state: paused; display: block;">
                        <p class="content-title fadeIn" style="opacity: 1;"><span style="opacity: 1;">Họ và tên:
                                    <b>{{$view->name}}</b></span></p>
                        <p class="content-subtitle fadeIn" style="opacity: 1;"><span style="opacity: 1;">Ngày sinh:
                                    <b>{{$view->birthday}}</b></span></p>
                    </div>
                    <div class="initial-carousel-container content-container carousel-container"
                         style="display: none">
                        <div class="signs-carousel">
                            <div class="sign-container">
                                <p class="sign-bg"></p>
                                <p class="sign-image"></p>
                                <p class="sign-title"></p>
                            </div>
                        </div>
                    </div>
                    @if($contents['lifePathNumberContent'])
                    <div class="mandalas-wrapper" style="">
                        <div class="text-white text-center" style="margin-bottom:-10px;margin-top:20px;font-size: 22px;font-weight: bold;">SỐ CHỦ ĐẠO</div>
                        <div class="mandala-wrapper-inner content-container mandala-container-1 fadeIn anim active" style="display: block;">
                            <div id="mandala-1" class="mandala-container">
                                <div class="mandala-object animated zoomIn" style="display: block;"></div>
                                <div class="mandala-shape animated flipInX" style="display: block;"></div>
                            </div>
                            <div class="mandala-number animated pulse infinite" style="display: block;">{{$contents['lifePathNumberContent']->content_number??''}}</div>
                        </div>
                        <div class="container">
                            <div class="p-2" style="border-radius: 10px; background: rgb(0 0 0 / 40%);">
                                <div class="m-t-5 m-b-10 text-center">
                                    <div class="mx-auto" style="display: inline-block;">
                                        <canvas id="pychart" style="min-height: 200px; max-width: 500px; display: block; height: 200px; width: 100%;" width="1000" height="400" class="chartjs-render-monitor"></canvas>
                                        <b style="color: #fff; font-size: 12px;">Chu kỳ vận số của bạn</b>
                                    </div>
                                </div>
                                <div class="text-white" style="font-size:13px;line-height: 1.8;padding-top:4px; color: white!important;">
                                    {!! $contents['personalityTraitsIndexContent']->content??'' !!}
                                </div>
                                <div class="text-center p-t-15" style="color:#f7d51a"><button class="button" id="viewMore">Xem chi tiết luận giải</button></div>
                            </div>
                        </div>
                    </div>
                    @else
                        <div class="col-md-12 text-center text-white h-100" style="margin-top: 300px">
                            <h1>Dữ liệu chưa được cập nhật</h1>
                            <p>
                                Chúng tôi rất tiếc chưa thể hiển thị thông tin ngay lúc này. Vui lòng quay lại sau.
                            </p>
                        </div>
                    @endif
                </div>
            </section>
        </div>
    </main>
@endsection
@section('script')
    <script src="{{asset('assets-kq1/Chart.bundle.js')}}"></script>
<script>
    document.getElementById("viewMore").addEventListener("click", ()=>{
        document.location.href = "{{route('web.view.content',$view->view_code)}}?step=2"
    })
    const ctx = document.getElementById('pychart').getContext('2d');

    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: {!! json_encode($contents['lifeCycleNumberContent']['4b8dd969-cf5a-48cd-ab14-63e18fa10bca']['xData'])??[] !!},
            datasets: [{
                label: 'Chu kỳ vận số của bạn',
                data: {!! json_encode($contents['lifeCycleNumberContent']['4b8dd969-cf5a-48cd-ab14-63e18fa10bca']['data'])??[] !!},
                borderColor: '#73ff6b',
                pointBackgroundColor: 'white',
                pointBorderColor: '#73ff6b',
                pointRadius: 4,
                pointHoverRadius: 7,
                fill: false,
                tension: 0.4,
                // yAxisID: 'y-axis-1',
            }]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        console.log(tooltipItem.index)
                        const value = tooltipItem.yLabel; // Giá trị tại mỗi điểm
                        const year = data.labels[tooltipItem.index]; // Năm tương ứng
                        return `Số: ${value}`; // Hiển thị thông tin tùy chỉnh
                    },
                },
            },
            legend: {
                display: false // Ẩn chú thích
            },
            title: {
                display: false,
                text: 'Chu kỳ vận số của bạn'
            },
            scales: {
                yAxes: [{
                    // id: 'y-axis-1',
                    // type: 'category', // Sử dụng loại category cho trục y để đảm bảo thứ tự
                    // position: 'left',
                    // labels: [1,9,2,6,3,4,5],
                    ticks: {
                        beginAtZero: false,
                        callback: function (value, index, values) {
                            return '';
                        },

                    },
                    scaleLabel: {
                        display: false,
                        labelString: 'Giá trị chu kỳ vận số'
                    },
                    gridLines: {
                        color: null
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false,
                        labelString: 'Chu kỳ vận số của bạn',
                    },
                    gridLines: {
                        color: '#333333' // Đổi màu đường kẻ dọc thành đỏ
                    }
                }]
            }
        }
    });
</script>
@endsection
