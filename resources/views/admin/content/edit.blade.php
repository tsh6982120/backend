@extends('admin.layouts.admin')

@section('title')
    @lang('Edit Content')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Edit Content')</h1>
            <a href="{{route('admin.indicator.edit',$indicator->id)}}" class="btn btn-primary"> <i class="fas fa-backward"></i> @lang('Back')</a>
        </div>
    </section>
@endsection
@section('content')
    <div class="row justify-content-center mt-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{ __('Add Content Form') }}</h6>
                </div>
                <div class="card-body">
                    <form  action="{{route('admin.indicator.update.content',$indicator->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="inp-name">{{ __('Content Number') }} <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="content_number" required value="{{$content->content_number}}" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="inp-name">{{ __('Description') }}</label>
                                <input class="form-control" type="text" name="description" value="{{$content->description}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label >@lang('Content')</label>
                            <textarea class="form-control" id="content" name="content">{{$content->content}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="is_vip">@lang('Is Vip Content')</label>
                            <label class="cswitch d-flex flex-column justify-content-between align-items-start">
                                <input id="is_vip" class="cswitch--input update" value="1" type="checkbox" name="is_vip" @if($content->is_vip ==1 )checked @endif/>
                                <span class="cswitch--trigger wrapper"></span>
                            </label>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" id="limit_words_container">

                            </div>
                        </div>
                        <div class="mt-3 text-right">
                            <button type="submit" id="submit-btn" class="btn btn-primary btn-lg">{{ __('Submit') }}</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('script')
    <script>
        "use strict"
        $('#content').summernote();
        $('#content_vip').summernote();
        if ($("#is_vip").is(':checked')) {
            $('#limit_words_container').append('<label for="limit_word">{{ __('Limit Words') }}</label><input id="limit_word" class="form-control" type="number" name="limit_words" value="{{$content->limit_words}}" required>');
        } else {
            $('#limit_words_container').empty();
        }
        $(document).ready(function() {
            $('#is_vip').on('change', function() {
                if ($(this).is(':checked')) {
                    $('#limit_words_container').append('<label for="limit_word">{{ __('Limit Words') }}</label><input id="limit_word" class="form-control" type="number" name="limit_words" value="{{$content->limit_words}}" required>');
                } else {
                    $('#limit_words_container').empty();
                }
            });
        });
    </script>
@endsection
