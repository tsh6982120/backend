@extends('admin.layouts.admin')

@section('title')
    @lang('Manage Users')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Manage Users')</h1>
        </div>
    </section>
@endsection
@section('content')
    <div class="row justify-content-center mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <div class="form-group mb-0">
                        <input class="form-control search mb-1" type="text" placeholder="@lang('Search')">
                    </div>

                    <a href="javascript:void(0)" data-toggle="modal" data-target="#translate" class="btn btn-primary add"> <i class="fas fa-plus"></i> @lang('Add New User')
                    </a>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="bg-primary mb-2">
                            <tr>
                                <th class="text-white">@lang('Uuid')</th>
                                <th class="text-white">@lang('Name')</th>
                                <th class="text-white">@lang('Email')</th>
                                <th class="text-white">@lang('Usage Count')</th>
                                <th class="text-right text-white">@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody class="custom-data">
                            @forelse ($users as $user)
                                <tr class="elements">
                                    <td data-label="@lang('Uuid')">
                                        {{$user->uuid}}
                                    </td>
                                    <td data-label="@lang('Name')" data-toggle="tooltip" title="{{$user->name}}">
                                        {{$user->name}}
                                    </td>
                                    <td data-label="@lang('Email')" data-toggle="tooltip" title="{{$user->email}}">{{$user->email}}</td>
                                    <td data-label="@lang('Usage Count')" data-toggle="tooltip">{{$user->usage_count}}</td>
                                    <td data-label="@lang('Action')" class="text-right">
                                        <a class="btn btn-primary edit mt-2" href="javascript:void(0)"><i class="fas fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty

                                <tr>
                                    <td class="text-center" colspan="100%">@lang('No Data Found')</td>
                                </tr>

                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var elements = $('.elements');
        $(document).on('input','.search',function(){
            var search = $(this).val().toUpperCase();
            var match = elements.filter(function (idx, elem) {
                return $(elem).text().trim().toUpperCase().indexOf(search) >= 0 ? elem : null;
            }).sort();
            var content = $('.custom-data');
            if (match.length == 0) {
                content.html('<tr><td colspan="100%" class="text-center">@lang('Data Not Found')</td></tr>');
            }else{
                content.html(match);
            }
        });
        {{--$('.select_category').selectpicker();--}}
        {{--$(document).on('click','.edit',function () {--}}
        {{--    const modal = $('#translate')--}}
        {{--    modal.find('.modal-title').text('@lang('Edit Product')')--}}
        {{--    modal.find('input[name=_method]').remove()--}}
        {{--    modal.find('input[name=name]').val($(this).data('name'))--}}
        {{--    modal.find('input[name=price]').val($(this).data('price'))--}}
        {{--    modal.find('input[name=price_sale]').val($(this).data('price_sale'))--}}
        {{--    modal.find('input[name=number]').val($(this).data('number'))--}}
        {{--    modal.find('select[name=category_code]').val($(this).data('category_code'))--}}
        {{--    modal.find('.select_category').selectpicker('refresh');--}}
        {{--    modal.find('form').attr('action',$(this).data('route'))--}}
        {{--    modal.find('form').prepend('@method('put')')--}}
        {{--    modal.modal('show')--}}
        {{--})--}}
        {{--$('.add').on('click',function () {--}}
        {{--    $('#translate').find('form')[0].reset();--}}
        {{--    $('#translate').find('input[name=_method]').remove()--}}
        {{--    $('#translate').find('form')[0].action = "{{route('admin.product.store')}}"--}}
        {{--    $('#translate').find('.modal-title').text('@lang('Add New Product')')--}}
        {{--})--}}
    </script>
@endsection
