@extends('admin.layouts.admin')

@section('title')
    @lang('Manage Indicators')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Manage Indicators')</h1>
            <button class="btn btn-primary bg-primary" data-toggle="modal" data-target="#modelId"> <i class="fas fa-plus"></i> @lang('Add New Indicator')</button>
        </div>
    </section>
@endsection


@section('content')

    <div class="row">
        @forelse ($indicators as $indicator)
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 currency--card">
                <div class="card card-primary">
                    <div class="card-header d-flex justify-content-between">
                        <h4><i class="fas fa-language"></i> {{$indicator->name}}</h4>
{{--                            <a href="javascript:void(0)" class="btn btn-danger btn-sm remove" data-id="{{$indicator->id}}"><i class="fas fa-trash-alt"></i></a>--}}
                    </div>
                    <div class="card-body">
                        <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between">
                                <p>@lang('Code') :<span class="font-weight-bold">{{$indicator->code}}</span></p>
                            </li>
                        </ul>

                        <a href="{{route('admin.indicator.edit',$indicator->id)}}" class="btn btn-primary btn-block"><i class="fas fa-edit"></i> @lang('Edit Indicator')</a>

                    </div>
                </div>
            </div>
        @empty
            <div class="w-100 text-center">
                <p>@lang('No Data Found')</p>
            </div>
        @endforelse
    </div>

    @if ($indicators->hasPages())
        {{ $indicators->links('admin.partials.paginate') }}
    @endif

    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{route('admin.indicator.store')}}" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('Add New Indicator')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>@lang('Indicator Name')</label>
                            <input class="form-control" type="text" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn-primary">@lang('Save')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{route('admin.indicator.destroy',$indicator->id)}}" method="POST">
                @method("delete")
                @csrf
                <input type="hidden" name="id">
                <div class="modal-content">
                    <div class="modal-body">
                        <h5 class="mt-3">@lang('Are you sure to remove?')</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn-danger">@lang('Confirm')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.remove').on('click',function () {
            $('#removeModal').find('input[name=id]').val($(this).data('id'))
            $('#removeModal').modal('show')
        })
    </script>
@endsection
