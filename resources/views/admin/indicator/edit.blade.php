@extends('admin.layouts.admin')

@section('title')
    @lang('Edit Indicator')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Edit Indicator')</h1>
            <a href="{{route('admin.indicator.index')}}" class="btn btn-primary"> <i class="fas fa-backward"></i> @lang('Back')</a>
        </div>
    </section>
@endsection

@section('content')

    <div class="row justify-content-center mt-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{ __('Edit Indicator Form') }}</h6>
                </div>
                <div class="card-body">
                    <form  action="{{route('admin.indicator.update',$indicator->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="inp-name">{{ __('Name') }}</label>
                                <input type="text" class="form-control" id="inp-name" name="name" value="{{$indicator->name}}"  placeholder="{{ __('Enter Name') }}" required>
                            </div>
                        </div>

                        <div class="mt-3 text-right">
                            <button type="submit" id="submit-btn" class="btn btn-primary btn-lg">{{ __('Submit') }}</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <div class="form-group mb-0">
                        <input class="form-control search mb-1" type="text" placeholder="@lang('Search')">
                    </div>

                    <a href="{{route('admin.indicator.add.content',$indicator->id)}}"  class="btn btn-primary add"> <i class="fas fa-plus"></i> @lang('Add New Content')
                    </a>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="bg-primary mb-2">
                            <tr>
                                <th class="text-white">@lang('Sl')</th>
                                <th class="text-white">@lang('Content Number')</th>
                                <th class="text-white">@lang('Locale')</th>
                                <th class="text-white">@lang('Description')</th>
                                <th class="text-white">@lang('Content')</th>
                                <th class="text-white">@lang('Vip Content')</th>
                                <th class="text-white">@lang('Limit Words')</th>
                                <th class="text-right text-white">@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody class="custom-data">
                            @php
                                $i = 0;
                            @endphp
                            @forelse ($contents as $content)
                                <tr class="elements">
                                    <td data-label="@lang('Sl')">
                                        {{++$i}}
                                    </td>
                                    <td data-label="@lang('Content Number')" data-toggle="tooltip" title="{{$content->content_number}}">
                                        {{$content->content_number}}
                                    </td>
                                    <td data-label="@lang('Locale')" data-toggle="tooltip" title="{{$content->locale}}">{{$content->locale}}</td>
                                    <td data-label="@lang('Description')" data-toggle="tooltip" title="{{$content->description}}">@if($content->description) {{$content->description}}@else <span class="text-danger">@lang('Not Yet Updated')</span>@endif</td>
                                    <td data-label="@lang('Content')" data-toggle="tooltip">@if($content->content){!! Str::limit(strip_tags($content->content),60) !!}@else <span class="text-danger">@lang('Not Yet Updated')</span> @endif</td>
                                    <td data-label="@lang('Vip Content')" data-toggle="tooltip">@if($content->is_vip == 1) <i class="fa fa-check-circle text-success" aria-hidden="true"></i> @else<i class="fa fa-times-circle text-danger" aria-hidden="true"></i>@endif</td>
                                    <td data-label="@lang('Limit Words')" data-toggle="tooltip">@if($content->limit_words) {{$content->limit_words}} @else @lang('No Limit')@endif</td>
                                    <td data-label="@lang('Action')" class="text-right">
                                        <a class="btn btn-primary edit mt-2"  href="{{route('admin.indicator.edit.content',$content->id)}}"><i class="fas fa-edit"></i></a>

                                        <a class="btn btn-danger remove mt-2" data-key="{{$content->id}}" data-route="{{route('admin.indicator.remove.content')}}" href="javascript:void(0)"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                            @empty

                                <tr>
                                    <td class="text-center" colspan="100%">@lang('No Data Found')</td>
                                </tr>

                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="remove" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{route('admin.indicator.remove.content')}}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-body">
                        <h5 class="mt-3">@lang('Are you sure to remove?')</h5>
                        <input type="hidden" name="id" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn-danger">@lang('Confirm')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        'use strict';
        var elements = $('.elements');
        $(document).on('input','.search',function(){
            var search = $(this).val().toUpperCase();
            var match = elements.filter(function (idx, elem) {
                return $(elem).text().trim().toUpperCase().indexOf(search) >= 0 ? elem : null;
            }).sort();
            var content = $('.custom-data');
            if (match.length == 0) {
                content.html('<tr><td colspan="100%" class="text-center">@lang('Data Not Found')</td></tr>');
            }else{
                content.html(match);
            }
        });


        $(document).on('click','.remove',function () {
            const modal = $('#remove')
            modal.find('input[name=id]').val($(this).data('key'))
            modal.modal('show')
        })
        $('.add').on('click',function () {
            $('#translate').find('form')[0].reset();
            $('#translate').find('.modal-title').text('@lang('Add New Content')')
        })
    </script>
@endsection
