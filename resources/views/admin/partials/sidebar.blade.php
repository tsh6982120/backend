<aside id="sidebar-wrapper">

    <ul class="sidebar-menu mb-5">
        <li class="menu-header text-center mb-5 mt-2"><img class="w-50" src="{{getPhoto('logo.png')}}" alt=""></li>
        <li class="menu-header">@lang('Dashboard')</li>
        <li class="nav-item {{menu('admin.dashboard')}}"><a href="{{route('admin.dashboard')}}" class="nav-link"><i class="fas fa-fire"></i><span>@lang('Dashboard')</span></a></li>
        <li class="menu-header">@lang('Manage Users')</li>
        <li class="nav-item {{menu(['admin.user*'])}}">
            <a href="{{route('admin.user.index')}}" class="nav-link"><i class="fas fa-user"></i> <span>@lang('Manage Users')</span></a>
        </li>
        <li class="menu-header">@lang('Manage Language')</li>
        <li class="nav-item {{menu(['admin.language*'])}}">
            <a href="{{route('admin.language.index')}}" class="nav-link"><i class="fas fa-language"></i> <span>@lang('Manage Language')</span></a>
        </li>
        <li class="menu-header">@lang('Manage Indicators')</li>
        <li class="nav-item {{menu(['admin.indicator*'])}}">
            <a href="{{route('admin.indicator.index')}}" class="nav-link"><i class="fas fa-newspaper"></i> <span>@lang('Manage Indicators')</span></a>
        </li>
        <li class="menu-header">@lang('Manage Categories')</li>
        <li class="nav-item {{menu(['admin.category*'])}}">
            <a href="{{route('admin.category.index')}}" class="nav-link"><i class="fas fa-th-list"></i> <span>@lang('Manage Categories')</span></a>
        </li>
        <li class="menu-header">@lang('Manage Products')</li>
        <li class="nav-item {{menu(['admin.product*'])}}">
            <a href="{{route('admin.product.index')}}" class="nav-link"><i class="fas fa-tags"></i> <span>@lang('Manage Products')</span></a>
        </li>
        <li class="menu-header">@lang('Manage Orders')</li>
        <li class="nav-item {{menu(['admin.order*'])}}">
            <a href="{{route('admin.order.index')}}" class="nav-link"><i class="fas fa-shopping-cart"></i> <span>@lang('Manage Orders')</span></a>
        </li>
        <li class="menu-header">@lang('Payment Gateways')</li>
        <li class="nav-item {{menu(['admin.payment-gateway*'])}}">
            <a href="{{route('admin.payment-gateway.index')}}" class="nav-link"><i class="fas fa-money-check-alt"></i> <span>@lang('Payment Gateways')</span></a>
        </li>
{{--        <li class="menu-header">@lang('Contacts')</li>--}}
{{--        <li class="nav-item {{menu('admin.contact')}}">--}}
{{--            <a href="{{route('admin.contact')}}" class="nav-link"><i--}}
{{--                    class="fas fa-file-contract"></i><span>@lang('Contacts')</span></a>--}}
{{--        </li>--}}
{{--        <li class="nav-item {{menu('admin.news.list')}}">--}}
{{--            <a href="{{route('admin.news.list')}}" class="nav-link"><i--}}
{{--                    class="fas fa-newspaper"></i><span>@lang('News')</span></a>--}}
{{--        </li>--}}
    </ul>
</aside>
