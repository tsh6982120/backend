<nav class="navbar navbar-expand-lg main-navbar">

    <ul class="navbar-nav mr-auto icon-menu">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg text-white"><i class="fas fa-bars"></i></a></li>
    </ul>

    <ul class="navbar-nav navbar-right">
        <li class="dropdown">
            @php
            $languages = getLanguage();
            @endphp
            <select class="selectpicker" data-size="auto" data-fit="10">
                @foreach($languages as $language)
                    <option value="{{$language->id}}" @if(session('lang')== $language->code) selected @endif>@lang($language->language)</option>
                @endforeach
            </select>
        </li>
        <li class="text-white">
            <a href="" class="nav-link nav-link-lg text-white"><i class="fas fa-home pr-1"></i></a>
        </li>
        <li class="dropdown"><a href="" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user text-white">
                <img alt="image" src="{{getPhoto(admin()->photo)}}" class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block">{{admin()->email}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item has-icon">
                    <i class="far fa-user"></i> @lang('Profile Setting')
                </a>
                <a href="" class="dropdown-item has-icon">
                    <i class="fas fa-key"></i> @lang('Change Password')
                </a>

                <div class="dropdown-divider"></div>
                <a href="{{route('admin.logout')}}" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> @lang('Logout')
                </a>
            </div>
        </li>
    </ul>
</nav>
