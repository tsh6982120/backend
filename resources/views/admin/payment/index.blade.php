@extends('admin.layouts.admin')

@section('title')
    @lang('Manage Payment Gateways')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Manage Payment Gateways')</h1>
            <a href="{{route('admin.payment-gateway.create')}}"  class="btn btn-primary add"> <i class="fas fa-plus"></i> @lang('Add New Payment Gateway')</a>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        @foreach ($paymentGateways as $paymentGateway)
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 currency--card">
                <div class="card card-primary">
                    <div class="card-header d-flex justify-content-between">
                        <h4><i class="fas fa-credit-card"></i> @lang($paymentGateway->title)</h4>
                            <a href="javascript:void(0)" class="btn btn-danger btn-sm remove" data-id="{{$paymentGateway->id}}"><i class="fas fa-trash-alt"></i></a>
                    </div>
                    <div class="card-body">
                        <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between">@lang('Name') :
                                <span class="font-weight-bold">{{$paymentGateway->name}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">@lang('Active') :
                                <label class="cswitch d-flex justify-content-between align-items-center">
                                    <input class="cswitch--input update" value="{{$paymentGateway->id}}" type="checkbox" {{$paymentGateway->active == 1 ? 'checked' : ''}} />
                                    <span class="cswitch--trigger wrapper"></span>
                                </label>
                            </li>
                        </ul>

                        <a href="{{route('admin.payment-gateway.edit',$paymentGateway->id)}}" class="btn btn-primary btn-block"><i class="fas fa-edit"></i> @lang('Edit Payment Gateway')</a>

                    </div>
                </div>
            </div>
        @endforeach
    </div>

    @if ($paymentGateways->hasPages())
        {{ $paymentGateways->links('admin.partials.paginate') }}
    @endif
    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{route('admin.remove.payment')}}" method="POST">
                @csrf
                <input type="hidden" name="id">
                <div class="modal-content">
                    <div class="modal-body">
                        <h5 class="mt-3">@lang('Are you sure to remove?')</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn-danger">@lang('Confirm')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.update').on('change', function () {
            var url = "{{route('admin.update-status.payment')}}"
            var val = $(this).val()
            var data = {
                id:val,
                _token:"{{csrf_token()}}"
            }
            $.post(url,data,function(response) {
                if(response.error){
                    toast('error',response.error)
                    return false;
                }
                toast('success',response.success)
                setTimeout(function () {
                    location.reload(); // Reload trang sau 300ms
                }, 300);
            })
        });
        $('.remove').on('click',function () {
            $('#removeModal').find('input[name=id]').val($(this).data('id'))
            $('#removeModal').modal('show')
        })
    </script>
@endsection
