@extends('admin.layouts.admin')

@section('title')
    @lang('Add Payment Gateway')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Add Payment Gateway')</h1>
            <a href="{{route('admin.payment-gateway.index')}}" class="btn btn-primary"> <i class="fas fa-backward"></i> @lang('Back')</a>
        </div>
    </section>
@endsection
@section('content')
    <div class="row justify-content-center mt-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{ __('Add Payment GateWay Form') }}</h6>
                </div>
                <div class="card-body">
                    <form  action="{{route('admin.payment-gateway.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="inp-name">{{ __('Title') }} <span class="text-danger">*</span> </label>
                                <input class="form-control" type="text" name="title" required value="{{old('title')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="inp-name">{{ __('Name') }} <span class="text-danger">*</span> </label>
                                <input class="form-control" type="text" name="name" required value="{{old('name')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="inp-name">{{ __('Detail') }}</label>
                                <textarea class="form-control" id="detail" name="detail">{{old('detail')}}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="inp-name">{{ __('Settings') }}</label>
                                <button class="btn btn-primary d-block mb-5" id="add-row" type="button">@lang('Add Config')</button>
                                <div id="key-value-container">
                                    <div class="key-value-row row mb-3">
                                        <div class="col-md-6">
                                            <label for="">@lang('Key')</label>
                                        <input class="form-control" type="text" name="keys[]">
                                        </div>
                                        <div class="col-md-5">
                                            <label for="">@lang('Value')</label>
                                            <input class="form-control" type="text" name="values[]">
                                        </div>
                                        <div class="col-md-1 d-flex align-items-end">
                                            <button class="btn btn-danger remove-row" type="button"><i class="fas fa-trash-alt"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="active">@lang('Active')</label>
                            <label class="cswitch d-flex flex-column justify-content-between align-items-start">
                                <input id="active" class="cswitch--input update" value="1" type="checkbox" name="active" checked/>
                                <span class="cswitch--trigger wrapper"></span>
                            </label>
                        </div>
                        <div class="mt-3 text-right">
                            <button type="submit" id="submit-btn" class="btn btn-primary btn-lg">{{ __('Submit') }}</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('script')
    <script>
        "use strict"
        $(document).ready(function() {
            $('#add-row').click(function() {
                var container = $('#key-value-container');
                var row = $('<div>').addClass('key-value-row row mb-3');
                row.html('<div class="col-md-6"><label for="">@lang('Key')</label>'+'<input class="form-control" type="text" name="keys[]">'+'</div>' +
                    '<div class="col-md-5"><label for="">@lang('Value')</label>'+'<input class="form-control" type="text" name="values[]">'+'</div>' +'<div class="col-md-1 d-flex align-items-end">' +
                '<button class="btn btn-danger remove-row" type="button"><i class="fas fa-trash-alt"></i></button>' +
                '</div>');
                container.append(row);
            });
        });
        $(document).on('click', '.remove-row', function() {
            $(this).closest('.key-value-row').remove();
        });
    </script>
@endsection
