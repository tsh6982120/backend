@extends('admin.layouts.admin')

@section('title')
    @lang('Manage Products')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Manage Products')</h1>
        </div>
    </section>
@endsection
@section('content')
    <div class="row justify-content-center mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <div class="form-group mb-0">
                        <input class="form-control search mb-1" type="text" placeholder="@lang('Search')">
                    </div>

                    <a href="javascript:void(0)" data-toggle="modal" data-target="#translate" class="btn btn-primary add"> <i class="fas fa-plus"></i> @lang('Add New Product')
                    </a>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="bg-primary mb-2">
                            <tr>
                                <th class="text-white">@lang('Sl')</th>
                                <th class="text-white">@lang('Code')</th>
                                <th class="text-white">@lang('Name')</th>
                                <th class="text-white">@lang('Price')</th>
                                <th class="text-white">@lang('Price Sale')</th>
                                <th class="text-white">@lang('Usage Count')</th>
                                <th class="text-white">@lang('Category')</th>
                                <th class="text-right text-white">@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody class="custom-data">
                            @php
                                $i = 0;
                            @endphp
                            @forelse ($products as $product)
                                <tr class="elements">
                                    <td data-label="@lang('Sl')">
                                        {{++$i}}
                                    </td>
                                    <td data-label="@lang('Code')" data-toggle="tooltip" title="{{$product->code}}">
                                        {{$product->code}}
                                    </td>
                                    <td data-label="@lang('Name')" data-toggle="tooltip" title="{{$product->name}}">{{$product->name}}</td>
                                    <td data-label="@lang('Price')" data-toggle="tooltip" title="{{$product->price}}">{{$product->price}}</td>
                                    <td data-label="@lang('Price Sale')" data-toggle="tooltip" title="Giảm {{100 - round(($product->price_sale / $product->price) * 100)}} %">{{$product->price_sale}}</td>
                                    <td data-label="@lang('Usage Count')" data-toggle="tooltip">{{$product->number}}</td>
                                    <td data-label="@lang('Category')" data-toggle="tooltip">{{$product->category->name}}</td>
                                    <td data-label="@lang('Action')" class="text-right">
                                        <a class="btn btn-primary edit mt-2" data-name="{{$product->name}}" data-price="{{$product->price ?? 0}}" data-price_sale ="{{$product->price_sale}}" data-number="{{$product->number}}" data-category_code="{{$product->category_code}}" data-route="{{route('admin.product.update',$product->id)}}" href="javascript:void(0)"><i class="fas fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty

                                <tr>
                                    <td class="text-center" colspan="100%">@lang('No Data Found')</td>
                                </tr>

                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="translate" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{route('admin.product.store')}}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('Add New Product')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label >@lang('Name')</label>
                            <input class="form-control" type="text" name="name" required value="{{old('name')}}">
                        </div>
                        <div class="form-group">
                            <label >@lang('Price')</label>
                            <input class="form-control price" type="number" name="price"  required value="{{old('price')}}">
                        </div>
                        <div class="form-group">
                            <label >@lang('Price Sale')</label>
                            <input class="form-control price"  type="number" name="price_sale"  required value="{{old('price_sale')}}">
                        </div>
                        <div class="form-group">
                            <label >@lang('Usage Count')</label>
                            <input class="form-control"  type="number" name="number"  required value="{{old('number')}}">
                        </div>
                        <div class="form-group">
                            <label >@lang('Select Category') : </label>
                            <select class="select_category" name="category_code" data-size="auto" data-fit="10" required>
                                @forelse($categories as $category)
                                    <option value="{{$category->code}}">@lang($category->name)</option>
                                @empty

                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn-primary">@lang('Save')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var elements = $('.elements');
        $(document).on('input','.search',function(){
            var search = $(this).val().toUpperCase();
            var match = elements.filter(function (idx, elem) {
                return $(elem).text().trim().toUpperCase().indexOf(search) >= 0 ? elem : null;
            }).sort();
            var content = $('.custom-data');
            if (match.length == 0) {
                content.html('<tr><td colspan="100%" class="text-center">@lang('Data Not Found')</td></tr>');
            }else{
                content.html(match);
            }
        });
        $('.select_category').selectpicker();
        $(document).on('click','.edit',function () {
            const modal = $('#translate')
            modal.find('.modal-title').text('@lang('Edit Product')')
            modal.find('input[name=_method]').remove()
            modal.find('input[name=name]').val($(this).data('name'))
            modal.find('input[name=price]').val($(this).data('price'))
            modal.find('input[name=price_sale]').val($(this).data('price_sale'))
            modal.find('input[name=number]').val($(this).data('number'))
            modal.find('select[name=category_code]').val($(this).data('category_code'))
            modal.find('.select_category').selectpicker('refresh');
            modal.find('form').attr('action',$(this).data('route'))
            modal.find('form').prepend('@method('put')')
            modal.modal('show')
        })
        $('.add').on('click',function () {
            $('#translate').find('form')[0].reset();
            $('#translate').find('input[name=_method]').remove()
            $('#translate').find('form')[0].action = "{{route('admin.product.store')}}"
            $('#translate').find('.modal-title').text('@lang('Add New Product')')
        })
    </script>
@endsection
