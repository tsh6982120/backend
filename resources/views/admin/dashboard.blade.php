@extends('admin.layouts.admin')
@section('title')
    @lang('Dashboard')
@endsection
@section('breadcrumb')
    <section class="section">
        <div class="section-header">
            <h1>@lang('Dashboard')</h1>
        </div>
    </section>
@endsection
@section('content')
    <section class="brand-story">
        <div class="fx-brand">
            <div class="brand-l">
                <h2 class="idx-h2">@lang('Welcome to Numerology')</h2>
                <p class="img sp">
                    <img src="{{asset('images/idx-img-brand.png')}}" alt="">
                </p>
                <p class="txt">@lang('Journey Into Self-Discovery: Explore the Power of Numerology to Illuminate Your Path')</p>
            </div>
            <div class="brand-r">
                <p class="img">
                    <img src="{{asset('images/idx-img-brand.png')}}" alt="">
                </p>
            </div>
        </div>
    </section>
{{--    <div class="row">--}}
{{--        <div class="col-md-3">--}}
{{--            <a href="">--}}
{{--                <div class="card-counter primary">--}}
{{--                    <i class="fa fa-code-fork"></i>--}}
{{--                    <span class="count-numbers"></span>--}}
{{--                    <span class="count-name">Users</span>--}}
{{--                </div>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--        <div class="col-md-3">--}}
{{--            <a href="">--}}
{{--                <div class="card-counter danger">--}}
{{--                    <i class="fa fa-code-fork"></i>--}}
{{--                    <span class="count-numbers"></span>--}}
{{--                    <span class="count-name">Contacts</span>--}}
{{--                </div>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
