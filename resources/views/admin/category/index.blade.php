@extends('admin.layouts.admin')

@section('title')
    @lang('Manage Categories')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Manage Categories')</h1>
        </div>
    </section>
@endsection
@section('content')
    <div class="row justify-content-center mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <div class="form-group mb-0">
                        <input class="form-control search mb-1" type="text" placeholder="@lang('Search')">
                    </div>

                    <a href="javascript:void(0)" data-toggle="modal" data-target="#translate" class="btn btn-primary add"> <i class="fas fa-plus"></i> @lang('Add New Category')
                    </a>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="bg-primary mb-2">
                            <tr>
                                <th class="text-white">@lang('Sl')</th>
                                <th class="text-white">@lang('Code')</th>
                                <th class="text-white">@lang('Name')</th>
                                <th class="text-white">@lang('Status')</th>
                                <th class="text-right text-white">@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody class="custom-data">
                            @php
                                $i = 0;
                            @endphp
                            @forelse ($categories as $category)
                                <tr class="elements">
                                    <td data-label="@lang('Sl')">
                                        {{++$i}}
                                    </td>
                                    <td data-label="@lang('Code')" data-toggle="tooltip" title="{{$category->code}}">
                                        {{$category->code}}
                                    </td>
                                    <td data-label="@lang('Name')" data-toggle="tooltip" title="{{$category->name}}">{{$category->name}}</td>
                                    <td data-label="@lang('Status')" data-toggle="tooltip">
                                        <label class="cswitch">
                                            <input class="cswitch--input update" value="{{$category->id}}" type="checkbox" {{$category->status == 1 ? 'checked' : ''}} />
                                            <span class="cswitch--trigger wrapper"></span>
                                        </label>
                                    </td>
                                    <td data-label="@lang('Action')" class="text-right">
                                        <a class="btn btn-primary edit mt-2" data-key="{{$category->name}}" data-value="{{$category->parent_code ?? 0}}" data-route="{{route('admin.category.update',$category->id)}}" href="javascript:void(0)"><i class="fas fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty

                                <tr>
                                    <td class="text-center" colspan="100%">@lang('No Data Found')</td>
                                </tr>

                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="translate" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{route('admin.category.store')}}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('Add New Category')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label >@lang('Name')</label>
                            <input class="form-control" type="text" name="name" required value="{{old('name')}}">
                        </div>
                        <div class="form-group">
                            <label >@lang('Parent Code') : </label>
                            <select class="select_parent_code" name="parent_code" data-size="auto" data-fit="10">
                                    <option value="0" selected>@lang('Choose Parent Category Code')</option>
                                @forelse($categories as $category)
                                    <option value="{{$category->code}}">@lang($category->name)</option>
                                @empty

                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn-primary">@lang('Save')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        'use strict';
        var elements = $('.elements');
        $(document).on('input','.search',function(){
            var search = $(this).val().toUpperCase();
            var match = elements.filter(function (idx, elem) {
                return $(elem).text().trim().toUpperCase().indexOf(search) >= 0 ? elem : null;
            }).sort();
            var content = $('.custom-data');
            if (match.length == 0) {
                content.html('<tr><td colspan="100%" class="text-center">@lang('Data Not Found')</td></tr>');
            }else{
                content.html(match);
            }
        });
        $('.select_parent_code').selectpicker();
        $(document).on('click','.edit',function () {
            const modal = $('#translate')
            modal.find('input[name=_method]').remove()
            modal.find('.modal-title').text('@lang('Edit Category')')
            modal.find('input[name=name]').val($(this).data('key'))
            modal.find('select[name=parent_code]').val($(this).data('value'))
            modal.find('.select_parent_code').selectpicker('refresh');
            modal.find('form').attr('action',$(this).data('route'))
            modal.find('form').prepend('@method('put')')
            modal.modal('show')
        })
        $('.update').on('change', function () {
            var url = "{{route('admin.update-status.category')}}"
            var val = $(this).val()
            var data = {
                id:val,
                _token:"{{csrf_token()}}"
            }
            $.post(url,data,function(response) {
                if(response.error){
                    toast('error',response.error)
                    return false;
                }
                toast('success',response.success)
            })

        });
        $('.add').on('click',function () {
            $('#translate').find('form')[0].reset();
            $('#translate').find('input[name=_method]').remove()
            $('#translate').find('form')[0].action = "{{route('admin.category.store')}}"
            $('#translate').find('.modal-title').text('@lang('Add New Category')')
        })
    </script>
@endsection
