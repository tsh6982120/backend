@extends('admin.layouts.admin')

@section('title')
    @lang('Manage Orders')
@endsection

@section('breadcrumb')
    <section class="section">
        <div class="section-header justify-content-between">
            <h1>@lang('Manage Orders')</h1>
        </div>
    </section>
@endsection
@section('content')
    <div class="row justify-content-center mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header justify-content-between">
                    <div class="form-group mb-0">
                        <input class="form-control search mb-1" type="text" placeholder="@lang('Search')">
                    </div>

                    <a href="javascript:void(0)" data-toggle="modal" data-target="#translate" class="btn btn-primary add"> <i class="fas fa-plus"></i> @lang('Add New Order')
                    </a>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="bg-primary mb-2">
                            <tr>
                                <th class="text-white">@lang('Code')</th>
                                <th class="text-white">@lang('User')</th>
                                <th class="text-white">@lang('Category')</th>
                                <th class="text-white">@lang('Product')</th>
                                <th class="text-white">@lang('Usage Count')</th>
                                <th class="text-white">@lang('Payment Method')</th>
                                <th class="text-white">@lang('Status')</th>
                                <th class="text-right text-white">@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody class="custom-data">
                            @forelse ($orders as $order)
                                <tr class="elements">
                                    <td data-label="@lang('Code')">
                                        {{$order->order_code}}
                                    </td>
                                    <td data-label="@lang('User')" data-toggle="tooltip" title="{{$order->user->name}}">
                                        {{$order->user->name}}
                                    </td>
                                    <td data-label="@lang('Category')" data-toggle="tooltip" title="{{$order->product->category->name}}">{{$order->product->category->name}}</td>
                                    <td data-label="@lang('Product')" data-toggle="tooltip" title="{{$order->product->name}}">
                                        {{$order->product->name}}
                                    </td>
                                    <td data-label="@lang('Usage Count')" data-toggle="tooltip" title="{{$order->usage_count}}">
                                        {{$order->usage_count}}
                                    </td>
                                    <td data-label="@lang('Payment Method')" data-toggle="tooltip" title="{{$order->payment_gateway->name}}">
                                        {{$order->payment_gateway->name}}
                                    </td>
                                    <td data-label="@lang('Status')" data-toggle="tooltip">
                                        @if($order->status == 0)
                                            <span class="badge badge-info">@lang('Processing')</span>
                                        @elseif($order->status == 1)
                                            <span class="badge badge-success">@lang('Completed')</span>
                                        @else
                                            <span class="badge badge-danger">@lang('Rejected')</span>
                                        @endif
                                    </td>
                                    <td data-label="@lang('Action')" class="text-right">
                                        @if($order->status == 0)
                                        <a class="btn btn-primary update-status mt-2" data-id="{{$order->id}}" data-status="1" href="javascript:void(0)"><i class="fas fa-check"></i></a>
                                        <a class="btn btn-danger update-status mt-2"  data-id="{{$order->id}}" data-status="-1" href="javascript:void(0)"><i class="fas fa-ban"></i></a>
                                        @else
                                        <p class="btn btn-success mt-2"  href="javascript:void(0)"><i class="fas fa-check-circle"></i></p>
                                        @endif
                                    </td>
                                </tr>
                            @empty

                                <tr>
                                    <td class="text-center" colspan="100%">@lang('No Data Found')</td>
                                </tr>

                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="translate" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{route('admin.order.store')}}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('Add New Category')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label >@lang('User')</label>
                            <select class="select_user" name="user_uuid" data-size="auto" data-fit="10">
                                <option value="0" selected>@lang('Choose User')</option>
                                @forelse($users as $user)
                                    <option value="{{$user->uuid}}">{{$user->name}}</option>
                                @empty

                                @endforelse
                            </select>
                        </div>
                        <div class="form-group">
                            <label >@lang('Category') : </label>
                            <select class="select_category" id="select_category" name="category_code" data-size="auto" data-fit="10">
                                @forelse($categories as $category)
                                    <option value="{{$category->code}}">@lang($category->name)</option>
                                @empty

                                @endforelse
                            </select>
                        </div>
                            <div class="form-group">
                                <label >@lang('Product') : </label>
                                <select class="select_product" id="select_product" name="product_code" data-size="auto" data-fit="10">

                                </select>
                            </div>
                        <div class="form-group">
                            <label >@lang('Method Payment') : </label>
                            <select class="payment_method" id="payment_method" name="payment_method" data-size="auto" data-fit="10">
                                @forelse($payment_gateways as $payment_gateway)
                                    <option value="{{$payment_gateway->id}}">@lang($payment_gateway->title) - @lang($payment_gateway->name)</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn-primary">@lang('Save')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        'use strict'
        var elements = $('.elements');
        $(document).on('input','.search',function(){
            var search = $(this).val().toUpperCase();
            var match = elements.filter(function (idx, elem) {
                return $(elem).text().trim().toUpperCase().indexOf(search) >= 0 ? elem : null;
            }).sort();
            var content = $('.custom-data');
            if (match.length == 0) {
                content.html('<tr><td colspan="100%" class="text-center">@lang('Data Not Found')</td></tr>');
            }else{
                content.html(match);
            }
        });
        $('.update-status').on('click',function (){
            let id = $(this).data('id')
            let status = $(this).data('status')
            var data = {
                id:id,
                status:status,
                _token:"{{csrf_token()}}"
            }
            let url = "{{route('admin.update-status.order')}}"
            $.post(url,data,function(response) {
                if(response.error){
                    toast('error',response.error)
                    return false;
                }
                toast('success',response.success)
                setTimeout(function () {
                    location.reload(); // Reload trang sau 500ms
                }, 200);
            })
        })
        $('.select_user').selectpicker();
        $('.select_category').selectpicker();
        $('.select_product').selectpicker();
        $('.payment_method').selectpicker();
            // Lấy giá trị của select và log ra console

        $(document).ready(function(){
            var category = $('#select_category');
            let url = "{{route('admin.get-list-product.order')}}"
            let category_code = category.val();
            var data = {
                category_code:category_code
            }
            $.get(url, data, function(res) {
                let data = res.data;
                $('#select_product').empty();
                data.forEach(function(item) {
                    $('#select_product').append('<option value="' + item.code + '">' + item.name + '</option>');
                });
                // Cập nhật bootstrap-select sau khi đã thêm tất cả các option mới vào select
                $('#select_product').selectpicker('refresh');
                $('.select_product').selectpicker('render');
            });
            category.on('change',function (){
                let url = "{{route('admin.get-list-product.order')}}"
                let category_code = $(this).val();
                var data = {
                    category_code:category_code
                }
                $.get(url,data,function(res){
                    let data = res.data;
                    $('#select_product').empty();
                    data.forEach(function(item){
                        $('#select_product').append($('<option>',{
                            value: item.code,
                            text: item.name
                        }))
                    })
                    $('.select_product').selectpicker('refresh')
                    $('.select_product').selectpicker('render');
                })
            })
        })
    </script>
@endsection
