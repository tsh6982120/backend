<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
{{--    <link rel="shortcut icon" type="image/png" href="{{getPhoto('gt2.png')}}">--}}
    <link rel="icon" href="{{asset('images/logo-32x32.png')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="https://ucon.social/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://ucon.social/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://ucon.social/favicon/favicon-16x16.png">
    <link rel="stylesheet" href="{{asset('css/admin/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/font-awsome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/custom.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/components.css')}}">
    @stack('style')
    <style>
        body:before{
            content: "";
            position: absolute;
            left: 0;
            right: 0;
            bottom: 190px;
            top: 0;
            margin: auto;
            z-index: -1;
            background: url("{{asset('images/main-bg.png')}}") no-repeat center;
            background-size: 210%;
            opacity: 23%;
        }
    </style>
    <!-- Favicon -->
</head>
<body>
<div id="app">
    <section class="section">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </section>
</div>
<script src="{{asset('js/admin/jquery.min.js')}}"></script>
<script src="{{asset('js/admin/popper.min.js')}}"></script>
<script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/admin/scripts.js')}}"></script>
@include('notify.alert')

</body>
</html>
