<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="icon" href="{{asset('images/logo-32x32.png')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="https://ucon.social/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://ucon.social/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://ucon.social/favicon/favicon-16x16.png">
    <link rel="stylesheet" href="{{asset('css/admin/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/font-awsome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/selectric.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/tagify.css') }}">
    <link rel="stylesheet" href="{{asset('css/admin/summernote.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/bootstrap-iconpicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/colorpicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/components.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/custom.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/bootstrap-select.min.css')}}">
    @stack('style')
</head>
<body>

<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        @include('admin.partials.topbar')
        <div class="main-sidebar">
            @include('admin.partials.sidebar')
        </div>

        <!-- Main Content -->
        <div class="main-content">
            @yield('breadcrumb')
            @yield('content')
        </div>

    </div>
</div>


@include('notify.alert')
<script src="{{asset('js/admin/jquery.min.js')}}"></script>
<script src="{{asset('js/admin/popper.min.js')}}"></script>
<script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/admin/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/admin/nicescroll.min.js')}}"></script>
<script src="{{asset('js/admin/summernote.js')}}"></script>
<script src="{{asset('js/admin/select2.min.js')}}"></script>
<script src="{{ asset('js/admin/tagify.js') }}"></script>
<script src="{{asset('js/admin/sortable.js') }}"></script>
<script src="{{asset('js/admin/moment-a.js')}}"></script>
<script src="{{asset('js/admin/stisla.js')}}"></script>
<script src="{{asset('js/admin/bootstrap-iconpicker.bundle.min.js')}}"></script>
<script src="{{asset('js/admin/colorpicker.js')}}"></script>
<script src="{{asset('js/admin/jquery.uploadpreview.min.js')}}"></script>
<script src="{{asset('js/admin/chart.min.js')}}"></script>
<script src="{{asset('js/admin/bootstrap-select.min.js')}}"></script>
{{--<script src="{{asset('js/admin/i18n/defaults-'.session('lang').'_'.strtoupper(session('lang')).'.min.js')}}"></script>--}}
<script src="{{asset('js/admin/scripts.js')}}"></script>
<script src="{{asset('js/admin/custom.js')}}"></script>


<script>

    var form_error   = "{{ __('Please fill all the required fields') }}";
    var mainurl = "{{ url('/') }}";
    var lang  = {
        'new': '{{ __('ADD NEW') }}',
        'edit': '{{ __('EDIT') }}',
        'details': '{{ __('DETAILS') }}',
        'update': '{{ __('Status Updated Successfully.') }}',
        'sss': '{{ __('Success !') }}',
        'active': '{{ __('Activated') }}',
        'deactive': '{{ __('Deactivated') }}',
        'loading': '{{ __('Please wait Data Processing...') }}',
        'submit': '{{ __('Submit') }}',
        'enter_name': '{{ __('Enter Name') }}',
        'enter_price': '{{ __('Enter Price') }}',
        'per_day': '{{ __('Per Day') }}',
        'per_month': '{{ __('Per Month') }}',
        'per_year': '{{ __('Per Year') }}',
        'one_time': '{{ __('One Time') }}',
        'enter_title': '{{ __('Enter Title') }}',
        'enter_content': '{{ __('Enter Content') }}',
        'extra_price_name' : '{{__('Enter Name')}}',
        'extra_price' : '{{__('Enter Price')}}',
        'policy_title' : '{{__('Enter Title')}}',
        'policy_content' : '{{__('Enter Content')}}',
    };

</script>


<script>
    'use strict'
    $(function(){
        $('.clear').on('click',function(e){
            e.preventDefault();
            const modal = $('#cleardb');
            modal.find('form').attr('action',$(this).data('href'))
            modal.modal('show');
        })

    })

    $('.summernote').summernote()
    $('.note-codable').on('blur', function() {
        var codeviewHtml        = $(this).val();
        var $summernoteTextarea = $(this).closest('.note-editor').siblings('textarea');
        $summernoteTextarea.val(codeviewHtml);
    });
    $('.selectpicker').on('change', function () {
        var url = "{{route('admin.update-status.language')}}"
        var val = $(this).val()
        var data = {
            id:val,
            _token:"{{csrf_token()}}"
        }
        $.post(url,data,function(response) {
            if(response.error){
                toast('error',response.error)
                return false;
            }
            toast('success',response.success)
            setTimeout(function () {
                location.reload(); // Reload trang sau 500ms
            }, 500);
        })
    })
</script>
@yield('script')

</body>
</html>
