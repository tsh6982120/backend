// WINDOW LOAD
$(window).bind("load", function () {
  "use strict";
  // ANCHOR LINK
  function anchorLink(el) {
    var p = $(el).offset();
    var offset_PC = 160;
    var offset_SP = 100;
    if ($(window).width() > 750) {
      $("html,body").animate(
        {
          scrollTop: p.top - offset_PC,
        },
        400
      );
    } else {
      $("html,body").animate(
        {
          scrollTop: p.top - offset_SP,
        },
        400
      );
    }
  }

  // ANCHOR FROM OTHER PAGE
  var hash = location.hash;
  if (hash && $(hash).length > 0) {
    anchorLink(hash);
  }

  // ANCHOR IN PAGE
  $('a[href^="#"]').click(function () {
    var get_ID = $(this).attr("href");
    if ($(get_ID).length) {
      anchorLink(get_ID);
      // CLOSE SP NAV
      if ($("body").hasClass("open-nav")) {
        $("#menu-toggle").trigger("click");
      }
      return false;
    }
  });
  // =========== END - ANCHOR LINK ============
  // =========== END - SCROLL TO MAIL FORM ============

  // LAZY LOAD RESOURCE
  $("[data-href]").each(function () {
    var _this = $(this);
    var href = $(this).data("href");
    setTimeout(function () {
      _this.attr("href", href);
    }, 3000);
  });
  $("[data-src]").each(function () {
    var _this = $(this);
    var src = $(this).data("src");
    setTimeout(function () {
      _this.attr("src", src);
    }, 3000);
  });
  // =========== END - LAZY LOAD RESOURCE ============
});

// WINDOW LOAD/SCROLL
$(window).bind("load scroll", function () {
  // TO-TOP
  const ww = $(window).width();
  if ($(this).scrollTop() >= 500) {
    $(".to-top").addClass("show");
  } else {
    $(".to-top").removeClass("show");
  }
  // =========== END - TO-TOP ============
});
$(window).bind("load scroll", function () {
  // TO-TOP
  const ww = $(window).width();
  if ($(this).scrollTop() > 0) {
    $("header").addClass("fixed");
  } else {
    $("header").removeClass("fixed");
  }
  // =========== END - TO-TOP ============
});

// DOCUMENT READY
$(document).ready(function () {
  "use strict";

  // MENU TOGGLE SP
  $("#menu-toggle").click(function () {
    $(this).toggleClass("open");
    $("body").toggleClass("open-nav");
  });
  // =========== END - MENU TOGGLE SP ============

  // SUB NAV SP SLIDE TOGGLE
  // $('.hnav-hv .hnav-menu-linksub').click(function() {
  //     var screenWidth = $(window).width();
  //     if (screenWidth <= 750) {
  //         $(this).toggleClass('open');
  //         $(this).next().stop().slideToggle(200);
  //     }
  // });
  // =========== END - MENU TOGGLE SP ============

  // SUB NAV SP SLIDE TOGGLE
  // $('.menu-hd-list .has-sub .label_sub').click(function() {
  //     var screenWidth = $(window).width();
  //     if (screenWidth <= 750) {
  //         $(this).toggleClass('open');
  //         $(this).next().stop().slideToggle(200);
  //     }
  // });
  // =========== END - MENU TOGGLE SP ============

  // =========== END - SUB NAV SP SLIDE TOGGLE ============

  // CHANGE Link full box
  // =========== END - CHANGE TAB ============
  const ww = $(window).width();
  if (ww < 751) {
    $(".media-list").slick({
      slidesToShow: 2,
      slidesToScroll: 2,
      infinite: true,
      autoPlay: true,
      dots: true,
    });
  }
  if ($(".idx-list-news").length > 0) {
    $(".idx-list-news").slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      centerMode: false,
      responsive: [
        {
          breakpoint: 750,
          settings: {
            slidesToShow: 2,
          },
        },
      ],
    });
  }
  if ($(".idx-list-team").length > 0) {
    $(".idx-list-team").slick({
      infinite: true,
      slidesToScroll: 1,
      slidesToShow: 3,
      arrows: false,
      responsive: [
        {
          breakpoint: 1260,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 750,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    });
  }
  AOS.init({
    duration: 600,
    disable: "mobile",
    once: true,
  });
  // =========== END - ACCORDION ============

});


document.getElementById("chukyvanso_btn_2").addEventListener("click", function (e) {
  document.getElementById("chukyvanso_des_2").style.display = "block";
  document.getElementById("chukyvanso_btn_2").style.display = "none";
})

document.getElementById("chukyvanso_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("chukyvanso_des_1").className.split(" ")
  document.getElementById("chukyvanso_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("chukyvanso_btn_1").style.display = "none";
})


document.getElementById("tinhcach_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("tinhcach_des_1").className.split(" ")
  document.getElementById("tinhcach_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("tinhcach_btn_1").style.display = "none";
})


document.getElementById("duongdoi_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("duongdoi_des_1").className.split(" ")
  document.getElementById("duongdoi_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("duongdoi_btn_1").style.display = "none";
})

document.getElementById("duongdoi_btn_2").addEventListener("click", function (e) {
  document.getElementById("duongdoi_des_2").style.display = "block";
  document.getElementById("duongdoi_btn_2").style.display = "none";
})


document.getElementById("chukyduongdoi_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("chukyduongdoi_des_1").className.split(" ")
  document.getElementById("chukyduongdoi_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("chukyduongdoi_btn_1").style.display = "none";
})

document.getElementById("chukyduongdoi_btn_2").addEventListener("click", function (e) {
  document.getElementById("chukyduongdoi_des_2").style.display = "block";
  document.getElementById("chukyduongdoi_btn_2").style.display = "none";
})


document.getElementById("kimtuthap_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("kimtuthap_des_1").className.split(" ")
  document.getElementById("kimtuthap_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("kimtuthap_btn_1").style.display = "none";
})

document.getElementById("kimtuthap_btn_2").addEventListener("click", function (e) {
  document.getElementById("kimtuthap_des_2").style.display = "block";
  document.getElementById("kimtuthap_btn_2").style.display = "none";
})


document.getElementById("chisonam_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("chisonam_des_1").className.split(" ")
  document.getElementById("chisonam_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("chisonam_btn_1").style.display = "none";
})

document.getElementById("chisonam_btn_2").addEventListener("click", function (e) {
  document.getElementById("chisonam_des_2").style.display = "block";
  document.getElementById("chisonam_btn_2").style.display = "none";
})



document.getElementById("chisothang_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("chisothang_des_1").className.split(" ")
  document.getElementById("chisothang_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("chisothang_btn_1").style.display = "none";
})

document.getElementById("chisothang_btn_2").addEventListener("click", function (e) {
  document.getElementById("chisothang_des_2").style.display = "block";
  document.getElementById("chisothang_btn_2").style.display = "none";
})



document.getElementById("sumenh_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("sumenh_des_1").className.split(" ")
  document.getElementById("sumenh_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("sumenh_btn_1").style.display = "none";
})

document.getElementById("sumenh_btn_2").addEventListener("click", function (e) {
  document.getElementById("sumenh_des_2").style.display = "block";
  document.getElementById("sumenh_btn_2").style.display = "none";
})



document.getElementById("dd_sm_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("dd_sm_des_1").className.split(" ")
  document.getElementById("dd_sm_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("dd_sm_btn_1").style.display = "none";
})

document.getElementById("dd_sm_btn_2").addEventListener("click", function (e) {
  document.getElementById("dd_sm_des_2").style.display = "block";
  document.getElementById("dd_sm_btn_2").style.display = "none";
})



document.getElementById("thuthachsumenh_btn_1").addEventListener("click", function (e) {
  let classNameList =  document.getElementById("thuthachsumenh_des_1").className.split(" ")
  document.getElementById("thuthachsumenh_des_1").className = classNameList.filter(item => item!== "truncate").join(" ")
  document.getElementById("thuthachsumenh_btn_1").style.display = "none";
})

document.getElementById("thuthachsumenh_btn_2").addEventListener("click", function (e) {
  document.getElementById("thuthachsumenh_des_2").style.display = "block";
  document.getElementById("thuthachsumenh_btn_2").style.display = "none";
})
