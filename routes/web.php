<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\LanguageController;
//use App\Http\Controllers\Admin\ContentController;
use  App\Http\Controllers\Admin\IndicatorController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PaymentGatewayController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Web\FrontendController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::prefix('admin')->name('admin.')->group(function(){
    Route::get('login',[AuthController::class,'getLogin'])->name('login');
    Route::post('login',[AuthController::class,'login'])->name('postLogin');

    Route::middleware('admin')->group(function(){
        Route::get('/',[DashboardController::class,'index'])->name('dashboard');
        Route::get('logout',[AuthController::class,'logout'])->name('logout');


        // MULTI LANGUAGES
        Route::resource('language',LanguageController::class);
        Route::post('language/remove', [LanguageController::class, 'destroy'])->name('remove.language');
        Route::post('language/status-update', [LanguageController::class, 'statusUpdate'])->name('update-status.language');
        Route::post('add-translate/{id}', [LanguageController::class, 'storeTranslate'])->name('translate.store');
        Route::post('update-translate/{id}', [LanguageController::class, 'updateTranslate'])->name('translate.update');
        Route::post('remove-translate', [LanguageController::class, 'removeTranslate'])->name('translate.remove');

        // INDICATORS
        Route::resource('indicator',IndicatorController::class);
        Route::get('add-content/{id}',[IndicatorController::class,'addContent'])->name('indicator.add.content');
        Route::post('store-content/{id}',[IndicatorController::class,'storeContent'])->name('indicator.store.content');
        Route::get('edit-content/{id}',[IndicatorController::class,'editContent'])->name('indicator.edit.content');
        Route::post('update-content/{id}',[IndicatorController::class,'updateContent'])->name('indicator.update.content');
        Route::post('remove-content',[IndicatorController::class,'removeContent'])->name('indicator.remove.content');

        // CATEGORIES

        Route::resource('category',CategoryController::class);
        Route::post('category/status-update', [CategoryController::class, 'statusUpdate'])->name('update-status.category');

        // PRODUCTS
        Route::resource('product',ProductController::class);

        // ORDERS
        Route::resource('order',OrderController::class);
        Route::post('order/update-status',[OrderController::class,'updateStatus'])->name('update-status.order');
        Route::get('list-product',[OrderController::class,'getListProduct'])->name('get-list-product.order');


        // PAYMENT GATEWAYS
        Route::resource('payment-gateway',PaymentGatewayController::class);
        Route::post('payment-gateway/update-status',[PaymentGatewayController::class,'updateStatus'])->name('update-status.payment');
        Route::post('remove-payment-gateway',[PaymentGatewayController::class,'remove'])->name('remove.payment');

        // USER

        Route::resource('user',UserController::class);
    });
});

Route::get('/', [FrontendController::class,'index'])->name('web.index');
Route::post('create-view',[FrontendController::class,'createView'])->name('web.create.view');
Route::get('view/{view_code}',[FrontendController::class,'view'])->name('web.view.content');
