<?php
namespace App\Helpers;
use DateTime;
use Symfony\Polyfill\Intl\Normalizer\Normalizer;

class Numerology {

    //loại bỏ dấu thanh Tiếng Việt
    public static function removeVietnameseAccents($str) {
        $str = Normalizer::normalize($str, Normalizer::FORM_D);
        $str = preg_replace('/[\x{0300}-\x{036f}]/u', '', $str);
        $str = str_replace('đ', 'd', $str);
        $str = str_replace('Đ', 'D', $str);
        return $str;
    }
    // thu gọn tổng số thành một chữ số
    public static function reduceNumber($num, $exceptions = null) {
        while ($num > 9 && (!isset($exceptions) || !in_array($num, $exceptions))) {
            $digits = str_split((string)$num);
            $num = array_reduce($digits, function($sum, $digit) {
                return $sum + (int)$digit;
            }, 0);
        }
        return $num;
    }


    // chỉ số vận mệnh
    public static function calcDestinyNumber($fullname){
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);
        $letterToNumber = [
            'A' => 1, 'J' => 1, 'S' => 1,
            'B' => 2, 'K' => 2, 'T' => 2,
            'C' => 3, 'L' => 3, 'U' => 3,
            'D' => 4, 'M' => 4, 'V' => 4,
            'E' => 5, 'N' => 5, 'W' => 5,
            'F' => 6, 'O' => 6, 'X' => 6,
            'G' => 7, 'P' => 7, 'Y' => 7,
            'H' => 8, 'Q' => 8, 'Z' => 8,
            'I' => 9, 'R' => 9,
        ];
        $fullname = strtoupper(str_replace(' ', '', $fullname));
        $sum = 0;
        for ($i = 0; $i < strlen($fullname); $i++) {
            $char = $fullname[$i];
            if (isset($letterToNumber[$char])) {
                $sum += $letterToNumber[$char];
            }
        }

        return self::reduceNumber($sum, [11, 22, 33]);
    }


    // chỉ số chủ đạo
   public static function calcLifePathNumber($birthday) {
        $d = new DateTime($birthday);
        $day = $d->format('d');
        $month = $d->format('m');
        $year = $d->format('Y');
        $day = self::reduceNumber($day);
        $month = self::reduceNumber($month);
        $year = self::reduceNumber($year);
        $text = $day . $month . $year;
        return self::reduceNumber((int)$text, [10, 11, 22, 33]);
    }
    // chỉ số linh hồn
    public static function calcSoulUrgeNumber($fullname) {
        $fullname =self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);

        // Định nghĩa giá trị cho các nguyên âm
        $vowelValues = [
            'a' => 1,
            'e' => 5,
            'i' => 9,
            'o' => 6,
            'u' => 3,
        ];

        // Biến đổi tên đầy đủ thành mảng của các ký tự
        $characters = str_split($fullname);

        // Lọc ra các nguyên âm và tính tổng giá trị của chúng
        $total = array_reduce($characters, function($sum, $char) use ($vowelValues) {
            if (isset($vowelValues[$char])) {
                return $sum + $vowelValues[$char];
            }
            return $sum;
        }, 0);

        return $total;
    }


    // chỉ số phản ứng
    public static function calcReactionIndex($birthday) {
        $d = new DateTime($birthday);
        $day = $d->format('d');
        $month = $d->format('m');

        $text = $day . $month;

        return self::reduceNumber((int)$text);
    }

    // 4 giai đoạn của cuộc đời
    public static function calcStagesOfLife($birthday) {
        $d = new DateTime($birthday);

        $lifePathNumber = self::reduceNumber(self::calcLifePathNumber($birthday));

        $month = self::reduceNumber((int)$d->format('m'));
        $day = self::reduceNumber((int)$d->format('d'));
        $year = self::reduceNumber((int)$d->format('Y'));

        $D1 = self::reduceNumber($month + $day);
        $D2 = self::reduceNumber($day + $year);
        $D3 = self::reduceNumber($D1 + $D2, [10, 11]);
        $D4 = self::reduceNumber($month + $year);

        $DD1 = abs($month - $day);
        $DD2 = abs($day - $year);
        $DD3 = abs($DD1 - $DD2);
        $DD4 = abs($month - $year);

        $data = [
            'month' => $month,
            'day' => $day,
            'year' => $year,
            'D1' => ['value' => 0, 'range' => []],
            'D2' => ['value' => 0, 'range' => []],
            'D3' => ['value' => 0, 'range' => []],
            'D4' => ['value' => 0, 'range' => []],
            'DD1' => ['value' => 0, 'range' => []],
            'DD2' => ['value' => 0, 'range' => []],
            'DD3' => ['value' => 0, 'range' => []],
            'DD4' => ['value' => 0, 'range' => []],
        ];

        $data['D1']['value'] = $D1;
        $data['D2']['value'] = $D2;
        $data['D3']['value'] = $D3;
        $data['D4']['value'] = $D4;

        $data['DD1']['value'] = $DD1;
        $data['DD2']['value'] = $DD2;
        $data['DD3']['value'] = $DD3;
        $data['DD4']['value'] = $DD4;

        $D1_age = 36 - $lifePathNumber;
        $data['D1']['range'] = [$d->format('Y'), $d->format('Y') + $D1_age];

        $data['D2']['range'] = [$data['D1']['range'][1] + 1, $data['D1']['range'][1] + 9];
        $data['D3']['range'] = [$data['D2']['range'][1] + 1, $data['D2']['range'][1] + 9];
        $data['D4']['range'] = [$data['D3']['range'][1] + 1, $data['D3']['range'][1] + 9];

        return $data;
    }
    // chu kỳ vận số
    public static function  lifeCycleNumber($birthday) {
        $d = new DateTime($birthday);
        $date = $d->format('d');
        $month = $d->format('m');
        $year = $d->format('Y');

        $yearNumber = self::reduceNumber(date('Y') - 5);
        $dayAndMonthNumber = self::reduceNumber((int)($date . $month));
        $result = self::reduceNumber($yearNumber + $dayAndMonthNumber);
        $data = [$result];

        for ($i = 0; $i < 11; $i++) {
            $data[$i + 1] = self::reduceNumber($data[$i] + 1);
        }

        $sample = [0, 6.5, 4, 2, 1, 2, 3.5, 2, 5, 6.5];

        $yData = [];
        $c = 0;

        for ($i = 0; $i < 11; $i++) {
            $yData[] = $sample[$data[$i]] + $c;
            if ($data[$i] == 1) {
                $c += 2;
            }
        }

        $xData = [];

        for ($i = 0; $i < 11; $i++) {
            $xData[] = date('Y') - 5 + $i;
        }

        $obj = [
            'data' => $data,
            'xData' => $xData,
            'yData' => $yData,
        ];

        return $obj;
    }
    // chỉ số năm cá nhân
    public static function personalYearNumber($birthday) {
        $d = new DateTime($birthday);
        $date = $d->format('d');
        $month = $d->format('m');

        $yearCurrent = (new DateTime())->format('Y');

        return self::reduceNumber((int)($date . $month . $yearCurrent));
    }
    // chỉ số tháng cá nhân
    public static function personalMonthNumber($birthday) {
        $personalYearNumber = self::personalYearNumber($birthday);
        $currentMonth = (new DateTime())->format('n');

        return self::reduceNumber((int)($currentMonth . $personalYearNumber));
    }
    // Thử thách sứ mệnh
    public static function lifePathChallenge($fullname) {
        return abs(
            self::soulChallenge($fullname) - self::personalityChallengeNumber($fullname)
        );
    }

    // thử thách linh hồn
    public static function soulChallenge($fullname) {
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);

        $vowelValues = [
            'a' => 1,
            'e' => 5,
            'i' => 9,
            'o' => 6,
            'u' => 3,
        ];

        $text = "";
        $characters = str_split($fullname);
        foreach ($characters as $char) {
            if (isset($vowelValues[$char])) {
                $text .= $char;
            }
        }

        return abs($vowelValues[$text[0]] - $vowelValues[$text[strlen($text) - 1]]);
    }

    // chỉ số nhân cách
    public static function personalityNumber($fullname) {
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);

        $vowelValues = [
            'a' => 1,
            'e' => 5,
            'i' => 9,
            'o' => 6,
            'u' => 3,
        ];

        $letterToNumber = [
            'A' => 1, 'J' => 1, 'S' => 1,
            'B' => 2, 'K' => 2, 'T' => 2,
            'C' => 3, 'L' => 3, 'U' => 3,
            'D' => 4, 'M' => 4, 'V' => 4,
            'E' => 5, 'N' => 5, 'W' => 5,
            'F' => 6, 'O' => 6, 'X' => 6,
            'G' => 7, 'P' => 7, 'Y' => 7,
            'H' => 8, 'Q' => 8, 'Z' => 8,
            'I' => 9, 'R' => 9,
        ];

        $text = preg_replace('/[^a-z]/', '', $fullname);
        $text = str_split($text);
        $text = array_filter($text, function($item) use ($vowelValues) {
            return !isset($vowelValues[$item]) && trim($item);
        });

        $numbers = array_map(function($item) use ($letterToNumber) {
            return $letterToNumber[strtoupper($item)];
        }, $text);

        return self::reduceNumber((int)implode('', $numbers), [11]);
    }


    // chỉ số thử thách nhân cách
    public static function personalityChallengeNumber($fullname) {
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);

        $vowelValues = [
            'a' => 1,
            'e' => 5,
            'i' => 9,
            'o' => 6,
            'u' => 3,
        ];

        $letterToNumber = [
            'A' => 1, 'J' => 1, 'S' => 1,
            'B' => 2, 'K' => 2, 'T' => 2,
            'C' => 3, 'L' => 3, 'U' => 3,
            'D' => 4, 'M' => 4, 'V' => 4,
            'E' => 5, 'N' => 5, 'W' => 5,
            'F' => 6, 'O' => 6, 'X' => 6,
            'G' => 7, 'P' => 7, 'Y' => 7,
            'H' => 8, 'Q' => 8, 'Z' => 8,
            'I' => 9, 'R' => 9,
        ];

        $text = preg_replace('/[^a-z]/', '', $fullname);
        $text = str_split($text);
        $text = array_filter($text, function($item) use ($vowelValues) {
            return !isset($vowelValues[$item]) && trim($item);
        });

        $firstChar = strtoupper($text[0]);
        $lastChar = strtoupper(end($text));

        return abs($letterToNumber[$firstChar] - $letterToNumber[$lastChar]);
    }
    // chỉ số trưởng thành
    public static function maturityNumber($fullname, $birthday) {
        $destinyNumber = self::calcDestinyNumber($fullname);
        $lifePathNumber = self::calcLifePathNumber($birthday);

        return self::reduceNumber($destinyNumber + $lifePathNumber, [11, 22]);
    }


    // chỉ số điểm yếu
    public static function weaknessNumber($fullname) {
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);

        $letterToNumber = [
            'A' => 1, 'J' => 1, 'S' => 1,
            'B' => 2, 'K' => 2, 'T' => 2,
            'C' => 3, 'L' => 3, 'U' => 3,
            'D' => 4, 'M' => 4, 'V' => 4,
            'E' => 5, 'N' => 5, 'W' => 5,
            'F' => 6, 'O' => 6, 'X' => 6,
            'G' => 7, 'P' => 7, 'Y' => 7,
            'H' => 8, 'Q' => 8, 'Z' => 8,
            'I' => 9, 'R' => 9,
        ];

        $nums = array_values(array_unique(array_map(function($item) use ($letterToNumber) {
            return $letterToNumber[strtoupper($item)] ?? null;
        }, str_split($fullname))));

        $result = [];
        for ($i = 1; $i <= 9; $i++) {
            if (!in_array($i, $nums)) {
                $result[] = $i;
            }
        }

        return $result;
    }


    // Chỉ số nợ nghiệp
    public static function karmicDebtNumber($birthday, $fullname) {
        $d = new DateTime($birthday);
        $date = $d->format('d');
        $month = $d->format('m');
        $year = $d->format('Y');

        $defaults = [13, 14, 16, 19];
        $result = [];

        // 1. Ngày sinh
        if (in_array($date, $defaults)) {
            $result[] = $date;
        }

        // 2. Tổng ngày tháng năm sinh
        $numBirthday = self::reduceNumber((int)($date . $month . $year));
        if (in_array($numBirthday, $defaults)) {
            $result[] = $numBirthday;
        }

        // 3.1 Ngày sinh và tháng sinh
        $d1 = self::reduceNumber((int)($date . $month));
        if (in_array($d1, $defaults)) {
            $result[] = $d1;
        }

        // 3.2 Tháng sinh và năm sinh
        $d2 = self::reduceNumber((int)($year . $month));
        if (in_array($d2, $defaults)) {
            $result[] = $d2;
        }

        // 3.3 Năm sinh và ngày sinh
        $d3 = self::reduceNumber((int)($year . $date));
        if (in_array($d3, $defaults)) {
            $result[] = $d3;
        }

        // 4. chủ đạo, chỉ số phản ứng, năng lực tự nhiên, chỉ số sứ mệnh chỉ số linh hồn là chỉ số nhân cách
        $a = self::calcLifePathNumber($birthday);
        $b = self::calcReactionIndex($birthday);
        $c = self::naturalAbilityNumber($birthday);
        $dd = self::calcDestinyNumber($fullname);
        $e = self::calcSoulUrgeNumber($fullname);
        $f = self::personalityNumber($fullname);

        foreach ([$a, $b, $c, $dd, $e, $f] as $val1) {
            foreach ([$a, $b, $c, $dd, $e, $f] as $val2) {
                if ($val1 !== $val2) {
                    $number = self::reduceNumber((int)($val1 . $val2));
                    if (in_array($number, $defaults)) {
                        $result[] = $number;
                    }
                }
            }
        }
        return array_unique($result);
    }


    // chỉ số năng lực tự nhiên
    public static function naturalAbilityNumber($birthday) {
        $d = new DateTime($birthday);
        $date = $d->format('d');

        return self::reduceNumber((int)$date);
    }


    // biểu đồ sức mạnh (ngày sinh)
    public static function strengthChart($birthday) {
        $d = new DateTime($birthday);
        $date = $d->format('d');
        $month = $d->format('m');
        $year = $d->format('Y');

        $matrix = [
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
        ];

        $text = $date . $month . $year;
        $chars = str_split($text);

        foreach ($chars as $char) {
            $num = (int)$char;
            if ($num > 0) {
                $matrix[$num] += 1;
            }
        }

        $listArrowsShow = [];
        $listArrowsHide = [];
        $listCellHasNumber = [];
        $listCellNotNumber = [];

        $listArrowsY = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ];

        $listArrowsX = [
            [1, 4, 7],
            [2, 5, 8],
            [3, 6, 9],
        ];

        $listArrowsCross = [
            [1, 5, 9],
            [3, 5, 7],
        ];

        $lists = array_merge($listArrowsY, $listArrowsX, $listArrowsCross);

        foreach ($lists as $item) {
            if ($matrix[$item[0]] > 0 && $matrix[$item[1]] > 0 && $matrix[$item[2]] > 0) {
                $listArrowsShow[] = $item;
            } elseif ($matrix[$item[0]] == 0 && $matrix[$item[1]] == 0 && $matrix[$item[2]] == 0) {
                $listArrowsHide[] = $item;
            }
        }

        foreach ($matrix as $key => $value) {
            if ($value > 0) {
                $nn = intval(str_repeat('1', $value));
                $listCellHasNumber[] = $key * $nn;
            } else {
                $listCellNotNumber[] = $key;
            }
        }

        return [
            'listArrowsShow' => $listArrowsShow,
            'listArrowsHide' => $listArrowsHide,
            'listCellHasNumber' => $listCellHasNumber,
            'listCellNotNumber' => $listCellNotNumber,
        ];
    }


    // chu kỳ đường đời
    public static function lifePathCycle($birthday) {
        $data = self::calcStagesOfLife($birthday);

        $result = [
            'one' => $data['D1']['range'],
            'two' => [$data['D2']['range'][0], $data['D4']['range'][1]],
            'three' => [$data['D4']['range'][1] + 1],
        ];

        return $result;
    }


    // năng lực trưởng thành
    public static function maturityAbility($fullname, $birthday) {
        return self::reduceNumber(
            abs(self::personalityNumber($fullname) - self::maturityNumber($fullname, $birthday))
        );
    }


    // chỉ số vượt khó
    public static function adversityQuotient($fullname) {
        $fullname = self::removeVietnameseAccents($fullname);
        $arr = explode(" ", $fullname);

        // Bảng chuyển đổi chữ cái thành số
        $letterToNumber = [
            'A' => 1, 'J' => 1, 'S' => 1,
            'B' => 2, 'K' => 2, 'T' => 2,
            'C' => 3, 'L' => 3, 'U' => 3,
            'D' => 4, 'M' => 4, 'V' => 4,
            'E' => 5, 'N' => 5, 'W' => 5,
            'F' => 6, 'O' => 6, 'X' => 6,
            'G' => 7, 'P' => 7, 'Y' => 7,
            'H' => 8, 'Q' => 8, 'Z' => 8,
            'I' => 9, 'R' => 9,
        ];

        $total = 0;
        foreach ($arr as $item) {
            $initial = strtoupper($item[0]);
            if (isset($letterToNumber[$initial])) {
                $total += $letterToNumber[$initial];
            }
        }

        return self::reduceNumber($total);
    }


    // chỉ số năng lực và tư duy
    public static function cognitiveAbilityIndex($fullname, $birthday) {
        return abs(
            self::adversityQuotient($fullname) - self::maturityAbility($fullname, $birthday)
        );
    }


    // chỉ số động lực tiếp cận
    public static function approachMotivationIndex($fullname) {
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);
        $arr = explode(" ", $fullname);
        $vowelValues = [
            'a' => 1,
            'e' => 5,
            'i' => 9,
            'o' => 6,
            'u' => 3,
        ];
        if (count($arr) <= 2) {
            $text = $arr[count($arr) - 1];
        } else {
            $text = $arr[count($arr) - 2] . $arr[count($arr) - 1];
        }
        $result = array_map(function($item) use ($vowelValues) {
            return $vowelValues[$item] ?? 0;
        }, str_split($text));
        return self::reduceNumber(array_sum($result));
    }


    // chỉ số năng lực tiếp cận
    public static function approachAbilityIndex($fullname) {
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);
        $arr = explode(" ", $fullname);
        $text = $arr[count($arr) - 1];

        // Bảng chuyển đổi chữ cái thành số
        $letterToNumber = [
            'A' => 1, 'J' => 1, 'S' => 1,
            'B' => 2, 'K' => 2, 'T' => 2,
            'C' => 3, 'L' => 3, 'U' => 3,
            'D' => 4, 'M' => 4, 'V' => 4,
            'E' => 5, 'N' => 5, 'W' => 5,
            'F' => 6, 'O' => 6, 'X' => 6,
            'G' => 7, 'P' => 7, 'Y' => 7,
            'H' => 8, 'Q' => 8, 'Z' => 8,
            'I' => 9, 'R' => 9,
        ];

        $result = array_map(function($item) use ($letterToNumber) {
            return $letterToNumber[$item] ?? 0;
        }, str_split(strtoupper($text)));

        return self::reduceNumber(array_sum($result), [11]);
    }


    // chỉ số thái độ tiếp cận
    public static function approachAttitudeIndex($fullname) {
        return abs(self::approachAbilityIndex($fullname) - self::approachMotivationIndex($fullname));
    }


    // xử lý biểu đồ
    public static function handleChart($nums) {
        $matrix = [
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0
        ];

        foreach ($nums as $item) {
            $num = (int)$item;
            if ($num > 0) {
                $matrix[$num] += 1;
            }
        }

        $listArrowsShow = [];
        $listArrowsHide = [];
        $listCellHasNumner = [];
        $listCellNotNumber = [];

        $listArrowsY = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ];
        $listArrowsX = [
            [1, 4, 7],
            [2, 5, 8],
            [3, 6, 9]
        ];
        $listArrowsCross = [
            [1, 5, 9],
            [3, 5, 7]
        ];

        $allArrows = array_merge($listArrowsY, $listArrowsX, $listArrowsCross);
        foreach ($allArrows as $item) {
            if ($matrix[$item[0]] > 0 && $matrix[$item[1]] > 0 && $matrix[$item[2]] > 0) {
                $listArrowsShow[] = $item;
            } elseif ($matrix[$item[0]] == 0 && $matrix[$item[1]] == 0 && $matrix[$item[2]] == 0) {
                $listArrowsHide[] = $item;
            }
        }

        foreach ($matrix as $key => $value) {
            if ($value > 0) {
                $n = $value;
                $nn = (int)str_repeat('1', $n);
                $listCellHasNumner[] = (int)$key * $nn;
            } else {
                $listCellNotNumber[] = (int)$key;
            }
        }

        return [
            'listArrowsShow' => $listArrowsShow,
            'listArrowsHide' => $listArrowsHide,
            'listCellHasNumner' => $listCellHasNumner,
            'listCellNotNumber' => $listCellNotNumber
        ];
    }



    // biểu đồ tên và biểu đồ tổng hợp
    public static function nameChartAndCompositeChart($fullname, $birthday) {
        $fullname = self::removeVietnameseAccents($fullname);
        $fullname = strtolower($fullname);
        $fullname = explode(" ", $fullname);
        $fullname = end($fullname);

        $d = new DateTime($birthday);
        $date = $d->format('d');
        $month = $d->format('m');
        $year = $d->format('Y');

        $letterToNumber = [
            'A' => 1, 'J' => 1, 'S' => 1,
            'B' => 2, 'K' => 2, 'T' => 2,
            'C' => 3, 'L' => 3, 'U' => 3,
            'D' => 4, 'M' => 4, 'V' => 4,
            'E' => 5, 'N' => 5, 'W' => 5,
            'F' => 6, 'O' => 6, 'X' => 6,
            'G' => 7, 'P' => 7, 'Y' => 7,
            'H' => 8, 'Q' => 8, 'Z' => 8,
            'I' => 9, 'R' => 9
        ];

        $text = str_split($fullname);
        $text = array_map(function ($item) use ($letterToNumber) {
            return $letterToNumber[strtoupper($item)];
        }, $text);

        $text2 = str_split($date.$month.$year);
        $text2 = array_map('intval', $text2);

        return [
            'name' => self::handleChart($text),
            'composite' => self::handleChart(array_merge($text2, $text))
        ];
    }

    // generate kết quả
    public static function generate($data=[]){
        return [
            'destiny_number'=>self::calcDestinyNumber($data['fullname']),
            'life_path_number'=>self::calcLifePathNumber($data['birthday']),
            'soul_urge_number'=>self::calcSoulUrgeNumber($data['fullname']),
            'reaction_index'=>self::calcReactionIndex($data['birthday']),
            'stages_of_life'=>json_encode(self::calcStagesOfLife($data['birthday'])),
            'life_cycle_number'=>json_encode(self::lifeCycleNumber($data['birthday'])),
            'personal_year_number'=>self::personalYearNumber($data['birthday']),
            'personal_month_number'=>self::personalMonthNumber($data['birthday']),
            'life_path_challenge'=>self::lifePathChallenge($data['fullname']),
            'soul_challenge'=>self::soulChallenge($data['fullname']),
            'personality_number'=>self::personalityNumber($data['fullname']),
            'personality_challenge_number'=>self::personalityChallengeNumber($data['fullname']),
            'maturity_number'=>self::maturityNumber($data['fullname'],$data['birthday']),
            'weekness_number'=>json_encode(self::weaknessNumber($data['fullname'])),
            'karmic_debt_number'=>json_encode(self::karmicDebtNumber($data['birthday'],$data['fullname'])),
            'natural_ability_number'=>self::naturalAbilityNumber($data['birthday']),
            'strength_chart'=>json_encode(self::strengthChart($data['birthday'])),
            'life_path_cycle'=>json_encode(self::lifePathCycle($data['birthday'])),
            'maturity_ability'=>self::maturityAbility($data['fullname'],$data['birthday']),
            'adversity_quotient'=>self::adversityQuotient($data['fullname']),
            'cognitive_ability_index'=>self::cognitiveAbilityIndex($data['fullname'],$data['birthday']),
            'approach_motivation_index'=>self::approachMotivationIndex($data['fullname']),
            'approach_ability_index'=>self::approachAbilityIndex($data['fullname']),
            'approach_attitude_index'=>self::approachAttitudeIndex($data['fullname']),
            'name_chart_and_composite_chart'=>json_encode(self::nameChartAndCompositeChart($data['fullname'],$data['birthday'])),
        ];
    }
}
