<?php
use Illuminate\Support\Facades\Auth;
use App\Models\LogUsage;
use App\Models\User;
use App\Models\Indicator;
use App\Models\IndicatorLanguage;
function admin()
{
    return auth()->guard('admin')->user();
}
function getPhoto($filename)
{
    if ($filename) {
        if (file_exists('images' . '/' . $filename)) return asset('images/' . $filename);
        else return asset('images/default.png');
    } else {
        return asset('images/default.png');
    }
}
function menu($route)
{
    if (is_array($route)) {
        foreach ($route as $value) {
            if (request()->routeIs($value)) {
                return 'active';
            }
        }
    } elseif (request()->routeIs($route)) {
        return 'active';
    }
}
function getLanguage(){
    $language = \App\Models\Language::all();
    return $language;
}
function updateUsageCount($user_uuid){
    $logNewestUsageCount = LogUsage::where('user_uuid',$user_uuid)->orderByDesc('used_at')->first();
    $user = User::where('uuid',$user_uuid)->first();
    $user->usage_count = $logNewestUsageCount->current_usage_count??$user->usage_count;
    $user->save();
}
function getIndicatorName($indicator_code){
    $name = IndicatorLanguage::where('indicator_code', $indicator_code)->where('locale', session('lang') ?? 'vi')->value('name');

    // Nếu không có trong indicator_language, lấy từ bảng indicator
    if (!$name) {
        $name = Indicator::where('code',$indicator_code)->value('name');
    }
    return $name;
}
