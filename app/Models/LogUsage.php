<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogUsage extends Model
{
    use HasFactory;
    protected $table = 'log_usage';
    protected $fillable = [
      'user_uuid','category_code','product_code','order_code','before_usage_count','current_usage_count','used_at','comment','log_at'
    ];
    public $timestamps = false;
}
