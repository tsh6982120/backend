<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndicatorLanguage extends Model
{
    use HasFactory;
    protected $table = 'indicators_language';
    protected $fillable = [
        'indicator_code','locale','name'
    ];
}
