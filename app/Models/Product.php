<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'code','name','price','price_sale','number','category_code'
    ];
    public function category(){
        return $this->belongsTo(Category::class,'category_code','code');
    }
}
