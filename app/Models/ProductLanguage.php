<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLanguage extends Model
{
    use HasFactory;
    protected $table = 'products_language';
    protected $fillable =[
      'product_code','locale','name'
    ];
}
