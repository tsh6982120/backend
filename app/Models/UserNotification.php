<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    use HasFactory;
    protected $table = 'user_notifications';
    protected $fillable = [
        'notify_id','user_uuid','title','message','is_read'
    ];
    public function notify($uuid = '', $data = ['title'=>'','message'=>'']){
        $data = [
            'notify_id'=>'',
            'user_uuid'=>$uuid,
            'title'=>$data['title'],
            'message'=>$data['message']
        ];
        UserNotification::insert($data);
    }
}
