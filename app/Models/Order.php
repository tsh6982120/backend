<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable = [
      'order_code','user_uuid','category_code','product_code','payment_method','usage_count'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_uuid','uuid');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_code','code');
    }
    public function payment_gateway(){
        return $this->belongsTo(PaymentGateway::class,'payment_method','id');
    }
}
