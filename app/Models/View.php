<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class View extends Model
{
    use HasFactory;
    protected $table ='view';
    protected $fillable = [
        'view_code',
        'name',
        'usually_name',
        'birthday',
        'gender',
        'user_id',
        'destiny_number',
        'life_path_number',
        'personality_traits_index',
        'soul_urge_number',
        'reaction_index',
        'stages_of_life',
        'life_cycle_number',
        'personal_year_number',
        'personal_month_number',
        'life_path_challenge',
        'soul_challenge',
        'personality_number',
        'personality_challenge_number',
        'maturity_number',
        'weekness_number',
        'karmic_debt_number',
        'natural_ability_number',
        'strength_chart',
        'life_path_cycle',
        'maturity_ability',
        'adversity_quotient',
        'cognitive_ability_index',
        'approach_motivation_index',
        'approach_ability_index',
        'approach_attitude_index',
        'name_chart_and_composite_chart',
        'use_vip',
    ];
    protected function decodeNestedJson($value)
    {
        $decoded = json_decode($value, true);
        if (is_array($decoded)) {
            foreach ($decoded as $key => $val) {
                if (is_string($val) && $this->isJson($val)) {
                    $decoded[$key] = $this->decodeNestedJson($val);
                }
            }
        }
        return $decoded;
    }
    protected function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    public function getDestinyNumberAttribute($value) // chỉ số vận mệnh
    {
        return $this->decodeNestedJson($value);
    }

    public function getLifePathNumberAttribute($value) // Chỉ số chủ đạo
    {
        return $this->decodeNestedJson($value);
    }

    public function getLifeCycleNumberAttribute($value) // Chu kỳ vận số
    {
        return $this->decodeNestedJson($value);
    }
    public function  getPersonalityTraitsIndexAttribute($value) // chỉ số tính cách
    {
        return $this->decodeNestedJson($value);
    }
    public function getSoulUrgeNumberAttribute($value) // chỉ số linh hồn
    {
        return $this->decodeNestedJson($value);
    }

    public function getReactionIndexAttribute($value) // chỉ số phản ứng
    {
        return $this->decodeNestedJson($value);
    }
    public function getStagesOfLifeAttribute($value) // 4 giai đoạn của cuộc đời
    {
        return $this->decodeNestedJson($value);
    }
    public function getPersonalYearNumberAttribute($value) // Chỉ số năm cá nhân
    {
        return $this->decodeNestedJson($value);
    }
    public function getPersonalMonthNumberAttribute($value) // Chỉ số tháng cá nhân
    {
        return $this->decodeNestedJson($value);
    }
    public function getLifePathChallengeAttribute($value) // thử thách sứ mệnh
    {
        return $this->decodeNestedJson($value);
    }
    public function getSoulChallengeAttribute($value) // thử thách linh hồn
    {
        return $this->decodeNestedJson($value);
    }
    public function getPersonalityNumberAttribute($value) //chỉ số nhân cách
    {
        return $this->decodeNestedJson($value);
    }
    public function getPersonalityChallengeNumberAttribute($value) // chỉ số thử thách nhân cách
    {
        return $this->decodeNestedJson($value);
    }
    public function getMaturityNumberAttribute($value) // chỉ số trưởng thành
    {
        return $this->decodeNestedJson($value);
    }
    public function getWeeknessNumberAttribute($value) // chỉ số điểm yếu
    {
        return $this->decodeNestedJson($value);
    }
    public function getKarmicDebtNumberAttribute($value) // chỉ số nợ nghiệp
    {
        return $this->decodeNestedJson($value);
    }
    public function getNaturalAbilityNumberAttribute($value) // chỉ số năng lực tự nhiên
    {
        return $this->decodeNestedJson($value);
    }

    public function getStrengthChartAttribute($value) // biểu đồ sức mạnh
    {
        return $this->decodeNestedJson($value);
    }

    public function getLifePathCycleAttribute($value) // chu kì đường đời
    {
        return $this->decodeNestedJson($value);
    }

    public function getMaturityAbilityAttribute($value) // năng lực trưởng thành
    {
        return $this->decodeNestedJson($value);
    }

    public function getAdversityQuotientAttribute($value) // chỉ số vượt khó
    {
        return $this->decodeNestedJson($value);
    }
    public function getCognitiveAbilityIndexAttribute($value)
    {
        return $this->decodeNestedJson($value);
    }
    public function getapproachMotivationIndexAttribute($value) // chỉ số động lực tiếp cận
    {
        return $this->decodeNestedJson($value);
    }
    public function getApproachAbilityIndexAttribute($value) // chỉ số năng lực tiếp cận
    {
        return $this->decodeNestedJson($value);
    }

    public function getApproachAttitudeIndexAttribute($value) // chỉ số thái độ tiếp cận
    {
        return $this->decodeNestedJson($value);
    }
    public function getNameChartAndCompositeChartAttribute($value) // biểu đồ tên và biểu đồ tổng hợp
    {
        return $this->decodeNestedJson($value);
    }

    public function getContent($indicator, $number)
    {
        $content = Content::where('indicator_code', $indicator)->where('content_number', $number)->where('locale',session('lang')??'vi')->first();
        // Lấy tên từ bảng indicator_language
        $name = DB::table('indicators_language')
            ->where('indicator_code', $indicator)
            ->where('locale', session('lang') ?? 'vi')
            ->value('name');

        // Nếu không có trong indicator_language, lấy từ bảng indicator
        if (!$name) {
            $name = DB::table('indicators')
                ->where('code', $indicator)
                ->value('name');
        }

        // Thêm trường name vào kết quả
        if (!$content) {
            $content = new Content();
            $content->content_number = $number;
        }

        // Thêm trường name vào kết quả
        $content->name = $name;

        return $content;
    }

    public function getDestinyNumberContent() // nội dung chỉ số vận mệnh
    {
        $decoded = $this->destiny_number;
        $indicator = array_key_first($decoded);
        $number = $decoded[$indicator];
        return $this->getContent($indicator,$number);
    }
    public function getLifePathNumberContent(){ // nội dung chỉ số chủ đạo
        $decoded = $this->life_path_number;
        if ($decoded) {
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getLifeCycleNumberContent(){ // nội dung chu kỳ vận số
        $decoded = $this->life_cycle_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getPersonalityTraitsIndexContent(){
        $decoded = $this->personality_traits_index;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getSoulUrgeNumberContent(){
        $decoded = $this->soul_urge_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getReactionIndexContent(){
        $decoded = $this->reaction_index;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getPersonalYearNumberContent(){
        $decoded = $this->personal_year_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getPersonalMonthNumberContent(){
        $decoded = $this->personal_month_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getLifePathChallengeContent(){
        $decoded = $this->life_path_challenge;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getSoulChallengeContent()
    {
        $decoded = $this->soul_challenge;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getPersonalityNumberContent()
    {
        $decoded = $this->personality_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getPersonalityChallengeNumberContent()
    {
        $decoded = $this->personality_challenge_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getMaturityNumberContent()
    {
        $decoded = $this->maturity_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getWeeknessNumberContent(){
        $decoded = $this->weekness_number;
        if ($decoded) {
            $contents = [];
            if (is_array($decoded)) {
                foreach ($decoded as $indicator_code => $numbers) {
                    if (is_array($numbers)) {
                        foreach ($numbers as $key => $number) {
                            $content = $this->getContent($indicator_code, $number);
                            if ($content) {
                                // Thêm nội dung vào mảng với số làm key
                                $contents[$indicator_code][$key] = [$number => $content->content];
                            } else {
                                // Trường hợp không có nội dung
                                $contents[$indicator_code][$key] = [$number => null];
                            }
                        }
                    }
                }
            }
            return $contents;
        }
        return null;
    }
    public function getKarmicDebtNumberContent(){
        $decoded = $this->karmic_debt_number;
        if ($decoded) {
            $contents = [];
            if (is_array($decoded)) {
                foreach ($decoded as $indicator_code => $numbers) {
                    if (is_array($numbers)) {
                        if (empty($numbers)) {
                            // Trường hợp mảng numbers rỗng
                            $contents[$indicator_code] = [];
                        } else {
                            foreach ($numbers as $key => $number) {
                                $content = $this->getContent($indicator_code, $number);
                                if ($content) {
                                    // Thêm nội dung vào mảng với số làm key
                                    $contents[$indicator_code][$key] = [$number => $content->content];
                                } else {
                                    // Trường hợp không có nội dung
                                    $contents[$indicator_code][$key] = [$number => null];
                                }
                            }
                        }
                    }
                }
            }
            return $contents;
        }
        return null;
    }

    public function getNaturalAbilityNumberContent()
    {
        $decoded = $this->natural_ability_number;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getAdversityQuotientContent()
    {
        $decoded = $this->adversity_quotient;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getCognitiveAbilityIndexContent(){
        $decoded = $this->cognitive_ability_index;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }

    public function getApproachMotivationIndexContent()
    {
        $decoded = $this->approach_motivation_index;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }

    public function getApproachAbilityIndexContent()
    {
        $decoded = $this->approach_ability_index;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }

    public function getApproachAttitudeIndexContent()
    {
        $decoded = $this->approach_attitude_index;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
    public function getMaturityAbilityContent(){
        $decoded = $this->maturity_ability;
        if ($decoded){
            $indicator = array_key_first($decoded);
            $number = $decoded[$indicator];
            return $this->getContent($indicator,$number);
        }
        return null;
    }
}
