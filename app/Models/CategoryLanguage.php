<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryLanguage extends Model
{
    use HasFactory;
    protected $table = 'categories_language';
    protected $fillable = [
      'category_code','locale','name','created_at','updated_at'
    ];
}
