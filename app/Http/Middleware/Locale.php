<?php

namespace App\Http\Middleware;

use App\Models\Language;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $code = 'en';
        if (session()->has('lang')) $code = session('lang');
        else{
            $lang = Language::where('is_default', 1)->first();
            $code = $lang ? $lang->code : 'en';
        }
        session()->put('lang', $code);
        app()->setLocale(session('lang',  $code));
        return $next($request);
    }
}
