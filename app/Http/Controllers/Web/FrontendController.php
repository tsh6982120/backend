<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Numerology;
use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use function Symfony\Component\String\s;

class FrontendController extends Controller
{
   public function index(){
        return view('web.index');
   }
    public function createView(Request $request){
        // Xác thực dữ liệu đầu vào
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'day' => 'required|integer|min:1|max:31',
            'month' => 'required|integer|min:1|max:12',
            'year' => 'required|integer|min:1900|max:' . date('Y'),
        ]);
        $validator->after(function ($validator) use ($request) {
            $day = $request->input('day');
            $month = $request->input('month');
            $year = $request->input('year');
//            $birthday = $day . '-' . $month . '-' . $year;
            if (!checkdate($month, $day, $year)) {
                $validator->errors()->add('birthday', 'Birthday Invalid');
            }
        });
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $birthday = $request->input('day') . '-' . $request->input('month') . '-' . $request->input('year');
        $existView = View::where('name',$request->input('fullname'))->where('birthday',$birthday)->first();

        if ($existView){
            return redirect(route('web.view.content',$existView->view_code));
        }
        $data = [
            'fullname' => $request->input('fullname'),
            'birthday' => $birthday
        ];

        $result = Numerology::generate($data);
        $view_insert = [
            'destiny_number'=>json_encode(['9836f952-00bc-4391-bf97-0281dfd212b4'=>$result['destiny_number']]),// Chỉ số vận mệnh
            'life_path_number'=>json_encode(['f4c67dd8-10fc-46c8-9adf-dfb8f298b1b3'=>$result['life_path_number']]),// Chỉ số chủ đạo
            'personality_traits_index'=>json_encode(['96c47dfd-dfbc-4a67-a3c5-71d76fd793ab'=>$result['life_path_number']]),// Chỉ số tính cách theo số chủ đạo
            'soul_urge_number'=>json_encode(['f1a2f049-d97e-499c-93fa-07cf358e5bda'=>$result['soul_urge_number']]),// Chỉ số linh hồn
            'reaction_index'=>json_encode(['3aacdd9b-8e0f-4a98-901f-b7bfc51ebbf7'=>$result['reaction_index']]),// Chỉ số phản ứng
            'stages_of_life'=>$result['stages_of_life'],// 4 giai đoạn của cuộc đời
            'life_cycle_number'=>json_encode(['4b8dd969-cf5a-48cd-ab14-63e18fa10bca'=>$result['life_cycle_number']]),// Chu kỳ vận số
            'personal_year_number'=>json_encode(['d0539b9f-cc40-46b2-a683-4f374ff7e9bb'=>$result['personal_year_number']]), // Chỉ số năm cá nhân
            'personal_month_number'=>json_encode(['7e68588a-7696-4ced-b1b9-23e0ec1718cb'=>$result['personal_month_number']]), // Chỉ số tháng cá nhân
            'life_path_challenge'=>json_encode(['97ef92d1-df33-405c-ad47-c5e24d1685b6'=>$result['life_path_challenge']]), // Thử thách sứ mệnh
            'soul_challenge'=>json_encode(['cfba58d4-595f-45e2-861a-7af01f2b07f8'=>$result['soul_challenge']]), // Thử thách linh hồn
            'personality_number'=>json_encode(['7de04f6d-19db-42b9-8ac0-dc8cdf68fe68'=>$result['personality_number']]), // Chỉ số nhân cách
            'personality_challenge_number'=>json_encode(['15d1d58f-ac7a-4095-b939-51b252c1e923'=>$result['personality_challenge_number']]), // Chỉ số thử thách nhân cách
            'maturity_number'=>json_encode(['51bef965-0463-4769-9452-1e962c02f795'=>$result['maturity_number']]), // Chỉ số trưởng thành
            'weekness_number'=>json_encode(['9f2c14d2-6d36-4f02-86eb-19a4b1b5a7c8'=>$result['weekness_number']]), // Chỉ số điểm yếu
            'karmic_debt_number'=>json_encode(['1fb84ccf-6be9-46fe-a38e-4a8990b0b98a'=>$result['karmic_debt_number']]), // Chỉ số nợ nghiệp
            'natural_ability_number'=>json_encode(['a6481985-50d2-4032-b255-294214eb0199'=>$result['natural_ability_number']]), // Chỉ số năng lực tự nhiên
            'strength_chart'=>$result['strength_chart'], // Biểu đồ sức mạnh
            'life_path_cycle'=>$result['life_path_cycle'], // Chu kỳ đường đời
            'maturity_ability'=>json_encode(['e5915f4b-0b14-4671-9086-93115d49dfd6'=>$result['maturity_ability']]), // Năng lực trưởng thành
            'adversity_quotient'=>json_encode(['e7097ca7-a9a5-4b30-8396-d76de0b1b9ff'=>$result['adversity_quotient']]), // Chỉ số vượt khó
            'cognitive_ability_index'=>json_encode(['f5dc35ee-2cc7-41f4-bce6-6969ec1f23b4'=>$result['cognitive_ability_index']]), // Chỉ số năng lực và tư duy
            'approach_motivation_index'=>json_encode(['e300dc28-8de4-4ce9-862c-5b1d42cdf21d'=>$result['approach_motivation_index']]), // Chỉ số động lực tiếp cận
            'approach_ability_index'=>json_encode(['d07d14b9-3291-45ef-b8db-508b4ffc5057'=>$result['approach_ability_index']]), // Chỉ số năng lực tiếp cận
            'approach_attitude_index'=>json_encode(['a899330a-3791-4cf2-bcd0-515781c1ab37'=>$result['approach_attitude_index']]), // Chỉ số thái độ tiếp cận
            'name_chart_and_composite_chart'=>$result['name_chart_and_composite_chart'], // Biểu đồ tên và biểu đồ tổng hợp
        ];
        $view_code = Str::random(10);
        $view_insert['view_code'] = $view_code;
        $view_insert['name'] = $request->input('fullname');
        $view_insert['usually_name'] = $request->input('usually_name');
        $view_insert['birthday'] = $birthday;
        $view_insert['gender'] = $request->input('gender');
        if (Auth::check()){
            $user = Auth::user();
            $view_insert['user_id'] = $user->uuid;
        }
        $view = View::create($view_insert);
        return redirect(route('web.view.content',$view->view_code));
    }

   public function view(Request $request,$view_code){
       $view = View::where('view_code',$view_code)->first();
       if (!$view){
           return view('web.404');
       }
       $personalityTraitsIndexContent = $view->getPersonalityTraitsIndexContent();  //Chỉ số tính cách
       $lifePathNumberContent = $view->getLifePathNumberContent();//Chỉ số chủ đạo
       $lifeCycleNumberContent = $view->life_cycle_number; // Chu kỳ vận số
       $destinyNumberContent = $view->getDestinyNumberContent(); // Chỉ số vận mệnh
       $soulUrgeNumberContent = $view->getSoulUrgeNumberContent(); // Chỉ số linh hồn
       $reactionIndexContent = $view->getReactionIndexContent();// Chỉ số phản ứng
       $stagesOfLifeContent = $view->stages_of_life; // 4 giai đoạn của cuộc đời
       $personalYearNumberContent = $view->getPersonalYearNumberContent(); // chỉ số năm cá nhân
       $personalMonthNumberContent = $view->getPersonalMonthNumberContent(); // chỉ số tháng cá nhân
       $lifePathChallengeContent = $view->getLifePathChallengeContent(); // thử thách sứ mệnh
       $soulChallengeContent = $view->getSoulChallengeContent(); // thử thách linh hồn
       $personalityNumberContent = $view->getPersonalityNumberContent(); // chỉ số nhân cách
       $personalityChallengeNumberContent = $view->getPersonalityChallengeNumberContent(); // chỉ số thử thách nhân cách
       $maturityNumberContent = $view->getMaturityNumberContent(); // chỉ số trưởng thành
       $weeknessNumberContent = $view->getWeeknessNumberContent(); // chỉ số điểm yếu
       $karmicDebtNumberContent = $view->getKarmicDebtNumberContent(); // chỉ số nợ nghiệp
       $naturalAbilityNumberContent = $view->getNaturalAbilityNumberContent(); // chỉ số năng lực tự nhiên
       $strengthChartContent = $view->strength_chart; // biểu đồ sức mạnh
       $lifePathCycleContent = $view->life_path_cycle; // chu kỳ đường đời
       $maturityAbilityContent = $view->getMaturityAbilityContent(); // năng lực trưởng thành
       $adversityQuotientContent = $view->getAdversityQuotientContent(); // chỉ số vượt khó
       $cognitiveAbilityIndexContent = $view->getCognitiveAbilityIndexContent(); // chỉ số năng lực và tư duy
       $approachMotivationIndexContent = $view->getApproachMotivationIndexContent(); // chỉ số động lực tiếp cận
       $approachAbilityIndexContent = $view->getApproachAbilityIndexContent(); // chỉ số năng lực tiếp cận
       $approachAttitudeIndexContent = $view->getApproachAttitudeIndexContent(); // Chỉ số thái độ tiếp cận
       $nameChartAndCompositeChart = $view->name_chart_and_composite_chart; // biểu đồ tên và biểu đồ tổng hợp
       $contents = [
            'personalityTraitsIndexContent'=>$personalityTraitsIndexContent ?? null,
            'lifePathNumberContent'=>$lifePathNumberContent ?? null,
            'lifeCycleNumberContent'=>$lifeCycleNumberContent ?? null,
            'destinyNumberContent' => $destinyNumberContent??null,
            'soulUrgeNumberContent' => $soulUrgeNumberContent??null,
            'reactionIndexContent' => $reactionIndexContent??null,
            'stagesOfLifeContent'=>$stagesOfLifeContent??null,
            'personalYearNumberContent'=>$personalYearNumberContent??null,
            'personalMonthNumberContent'=>$personalMonthNumberContent??null,
            'lifePathChallengeContent'=>$lifePathChallengeContent??null,
            'soulChallengeContent'=>$soulChallengeContent??null,
            'personalityNumberContent'=>$personalityNumberContent??null,
            'personalityChallengeNumberContent'=>$personalityChallengeNumberContent??null,
            'maturityNumberContent'=>$maturityNumberContent??null,
            'weeknessNumberContent'=>$weeknessNumberContent??null,
            'karmicDebtNumberContent'=>$karmicDebtNumberContent??null,
            'naturalAbilityNumberContent'=>$naturalAbilityNumberContent??null,
            'strengthChartContent'=>$strengthChartContent??null,
            'lifePathCycleContent'=>$lifePathCycleContent??null,
            'maturityAbilityContent'=>$maturityAbilityContent??null,
            'adversityQuotientContent'=>$adversityQuotientContent??null,
            'cognitiveAbilityIndexContent'=>$cognitiveAbilityIndexContent??null,
            'approachMotivationIndexContent'=>$approachMotivationIndexContent??null,
            'approachAbilityIndexContent'=>$approachAbilityIndexContent??null,
            'approachAttitudeIndexContent'=>$approachAttitudeIndexContent??null,
            'nameChartAndCompositeChart'=>$nameChartAndCompositeChart??null,
       ];
//       dd($contents);
       if ($request->get('step')==2){
           return view('web.view2',compact('view','contents'));
       }
       return view('web.view',compact('view','contents'));
   }
}
