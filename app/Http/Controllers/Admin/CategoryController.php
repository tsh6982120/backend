<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryLanguage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class CategoryController extends Controller
{
    //
    public function index(){
        $categories = Category::select('categories.*', DB::raw('
        IFNULL(
            (SELECT categories_language.name FROM categories_language WHERE categories_language.category_code = categories.code AND categories_language.locale = "'.session('lang').'"),
            categories.name
        ) AS name
    '))
            ->latest()->paginate(12);
        return view('admin.category.index',compact('categories'));
    }
    public function store(Request $request){
        $request->validate(['name'=>'required|unique:categories,name']);
        $data = new Category();
        $code = Uuid::uuid4()->toString();
        $data->code = $code;
        $data->name = $request->name;
        $parent_code = null;
        if ($request->parent_code !=0){
            $parent_code = $request->parent_code;
        }
        $data->parent_code = $parent_code;
        $data->save();
        // add language
        $categoryLanguage = new CategoryLanguage();
        $categoryLanguage->category_code = $code;
        $categoryLanguage->locale = session('lang');
        $categoryLanguage->name = $request->name;
        $categoryLanguage->save();
        return back()->with('success','Category added successfully');
    }
    public function update(Request $request, Category $category){
        $request->validate(['name'=>'required']);
        $existCategoryLanguage = CategoryLanguage::where('category_code',$category->code)->where('locale',session('lang'))->first();
        //cập nhật bản dịch
        if (!is_null($existCategoryLanguage)){
            $existCategoryLanguage->name=$request->name;
            $existCategoryLanguage->save();
        }else{
        $data = new CategoryLanguage();
            $data->category_code = $category->code;
            $data->locale = session('lang');
            $data->name = $request->name;
            $data->save();
        }
        // update category
        $category->name = $request->name;
        $parent_code = null;
        if ($request->parent_code !=0){
            $parent_code = $request->parent_code;
        }
        $category->parent_code = $parent_code;
        $category->save();
        return back()->with('success','Category updated successfully');
    }
    public function statusUpdate(Request $request)
    {
        $category = Category::findOrFail($request->id);
        $category->status = $category->status==1 ? 0 : 1;
        $category->update();
        return response()->json(['success' => __('Status has been changed.')]);
    }
}
