<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\LogUsage;
use App\Models\Order;
use App\Models\PaymentGateway;
use App\Models\Product;
use App\Models\User;
use App\Models\UserNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class OrderController extends Controller
{
    //
    public function index(){
        $orders = Order::with([
            'user',
            'product.category'=>function($query){
                $query->select('categories.*',DB::raw(
                    'IFNULL(
                                (SELECT categories_language.name FROM categories_language WHERE categories_language.category_code = categories.code AND categories_language.locale = "'.session('lang').'"),
                                categories.name
                            ) AS name'
                ));
            },
            'product'=>function($query){
                $query->select('products.*', DB::raw('
                            IFNULL(
                                (SELECT products_language.name FROM products_language WHERE products_language.product_code = products.code AND products_language.locale = "'.session('lang').'"),
                                products.name
                            ) AS name
                        '));
            },
            'payment_gateway'
        ])->paginate(12);
        $users = User::all();
        $categories = Category::select('categories.*', DB::raw('
        IFNULL(
            (SELECT categories_language.name FROM categories_language WHERE categories_language.category_code = categories.code AND categories_language.locale = "'.session('lang').'"),
            categories.name
        ) AS name
    '))
            ->where('status',1)->get();
        $payment_gateways = PaymentGateway::where('active',1)->latest()->get();
        return view('admin.order.index',compact('orders','users','categories','payment_gateways'));
    }
    public function store(Request $request){
        $request->validate([
            'user_uuid'=>'required',
            'category_code'=>'required',
            'product_code'=>'required',
            'payment_method'=>'required'
        ]);
        if ($request->user_uuid == 0){
            return back()->with('error','Please choose user');
        }
        $product = Product::where('code',$request->product_code)->first();
        if (!$product){
            return back()->with('error','Product not exist');
        }
        $category = Category::where('code',$request->category_code)->first();
        if ($product->category_code != $category->code){
            return back()->with('error','Product not belong to this category');
        }
        $payment_method = PaymentGateway::findOrFail($request->payment_method);
        if (!$payment_method){
            return back()->with('error','Payment method not found');
        }
        $data = new Order();
        $code = Uuid::uuid4()->toString();
        $data->order_code = $code;
        $data->user_uuid = $request->user_uuid;
        $data->category_code = $request->category_code;
        $data->product_code = $request->product_code;
        $data->payment_method = $request->payment_method;
        $usage_count = $product->number;
        $data->usage_count = $usage_count;
        $data->status = 0;
        $data->save();
        // add notification
//        UserNotification::notify($request->user_uuid,['title'=>'Thanh toán thành công','message'=>'Bạn vừa đăng ký thành công gói '])


        return back()->with('success','Order added successfully');
    }
    public function updateStatus(Request $request){
        $request->validate([
           'id'=>'required',
           'status'=>'required'
        ]);
        $order = Order::findOrFail($request->id);
        $order->status = $request->status;
        $order->update();
        if ($request->status!= -1){
            $user = User::where('uuid',$order->user_uuid)->first();
            LogUsage::insert([
                'user_uuid'=>$user->uuid,
                'view_code'=>null,
                'before_usage_count'=>$user->usage_count,
                'amount'=>$order->usage_count,
                'current_usage_count'=>$user->usage_count + $order->usage_count,
                'used_at'=>Carbon::now(),
                'comment'=>"Mua thêm lượt VIP",
                'log_at'=>Carbon::now()
            ]);
            updateUsageCount($user->uuid);
        }
        return response()->json(['success' => __('Status has been changed.')]);
    }
    public function getListProduct(Request $request){
        $category_code = $request->category_code;
        $products = Product::select('products.*', DB::raw('
            IFNULL(
                (SELECT products_language.name FROM products_language WHERE products_language.product_code = products.code AND products_language.locale = "'.session('lang').'"),
                products.name
            ) AS name
        '))->where('category_code',$category_code)->latest()->get();
        return response()->json([
            'data'=>$products
        ]);
    }
}
