<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Indicator;
use App\Models\IndicatorLanguage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class IndicatorController extends Controller
{
    public function index(){
        $indicators = Indicator::select('indicators.*', DB::raw('
        IFNULL(
            (SELECT indicators_language.name FROM indicators_language WHERE indicators_language.indicator_code = indicators.code AND indicators_language.locale = "'.session('lang').'"),
            indicators.name
        ) AS name
    '))
            ->paginate(12);


        return view ('admin.indicator.index',compact('indicators'));
    }
    public function store(Request $request){
        $request->validate(['name'=>'required|unique:indicators,name']);
        $data = new Indicator();
        $code = Uuid::uuid4()->toString();
        $data->name = $request->name;
        $data->code = $code;
        $data->save();
        // add language
        $indicatorLang= new IndicatorLanguage();
        $indicatorLang->indicator_code = $code;
        $indicatorLang->locale = session('lang');
        $indicatorLang->name = $request->name;
        $indicatorLang->save();
        return back()->with('success','Indicator added successfully');
    }
    public function edit(Indicator $indicator){
        $indicatorLangExist = IndicatorLanguage::where('indicator_code',$indicator->code)->where('locale',session('lang'))->first();
        if (!is_null($indicatorLangExist)){
            $indicator->name = $indicatorLangExist->name;
        }
        $contents = Content::where('indicator_code',$indicator->code)->where('locale',session('lang')??'en')->latest()->get();
        return view('admin.indicator.edit',compact('indicator','contents'));
    }
    public function update(Request $request, Indicator $indicator){
        $request->validate(['name'=>'required|unique:indicators_language,name']);
        $existIndicatorLanguage = IndicatorLanguage::where('indicator_code',$indicator->code)->where('locale',session('lang'))->first();
        if (!is_null($existIndicatorLanguage)){
            $existIndicatorLanguage->name=$request->name;
            $existIndicatorLanguage->save();
        }else{
            $data = new IndicatorLanguage();
            $data->indicator_code = $indicator->code;
            $data->locale = session('lang');
            $data->name = $request->name;
            $data->save();
        }
        return back()->with('success','Indicator updated successfully');
    }
    public function destroy(Request $request,Indicator $indicator){
        $request->validate(['id' => 'required']);
        $id = $request->id;
        $content = Content::where('indicator_code',$indicator->code)->delete();
        $indicatorLang = IndicatorLanguage::where('indicator_code',$indicator->code)->delete();
        $indicator->delete();
        return back()->with('success', "Indicator has been removed");
    }
    public function addContent($id){
        $indicator = Indicator::findOrFail($id);
        return view('admin.content.add',compact('indicator'));
    }
    public function storeContent(Request $request,$id)
    {
        $request->validate([ 'content_number' => 'required']);
        $indicator = Indicator::findOrFail($id);
        $existContent = Content::where('content_number',$request->content_number)->where('indicator_code',$indicator->code)->where('locale',session('lang'))->first();
        if (!is_null($existContent)) {
            return back()->with('error', "Content of number ".$request->content_number." already exist");
        } else {
            $data = new Content();
            $data->indicator_code = $indicator->code;
            $data->content_number = $request->content_number;
            $data->locale = session('lang');
            $data->description = $request->description;
            $data->content = $request->content;
            if ($request->is_vip){
                $data->is_vip = $request->is_vip;
                $data->limit_words = $request->limit_words;
            }
            $data->save();
            return redirect(route('admin.indicator.edit',$indicator->id))->with('success', "New content has been added");
        }
    }
    public function editContent(Request $request,$id){
        $content= Content::findOrFail($id);
        $indicator = Indicator::where('code',$content->indicator_code)->first();
        return view('admin.content.edit',compact('content','indicator'));
    }
    public function updateContent(Request $request, $id){
        $indicator = Indicator::findOrFail($id);
        $content = Content::where('content_number',$request->content_number)->where('locale',session('lang'))->first();
        $content->content = $request->content;
        $content->description = $request->description;
        if ($request->is_vip){
            $content->is_vip = $request->is_vip;
            $content->limit_words = $request->limit_words;
        }else{
            $content->is_vip = 0;
            $content->limit_words = null;
        }
        $content->save();
        return redirect(route('admin.indicator.edit',$indicator->id))->with('success', 'Content updated successfully');
    }
    public function removeContent(Request $request)
    {
        $request->validate(['id' => 'required']);
        $content = Content::findOrFail($request->id)->delete();
        return back()->with('success', "Content has been removed");
    }
}
