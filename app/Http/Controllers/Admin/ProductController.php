<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductLanguage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class ProductController extends Controller
{
    //
    public function index(){
        $products = Product::with(['category' => function ($query) {
            $query->select('categories.*', DB::raw('
        IFNULL(
            (SELECT categories_language.name FROM categories_language WHERE categories_language.category_code = categories.code AND categories_language.locale = "'.session('lang').'"),
            categories.name
        ) AS name
    '));
        }])->select('products.*', DB::raw('
    IFNULL(
        (SELECT products_language.name FROM products_language WHERE products_language.product_code = products.code AND products_language.locale = "'.session('lang').'"),
        products.name
    ) AS name
'))->paginate(12);

        $categories = Category::select('categories.*', DB::raw('
        IFNULL(
            (SELECT categories_language.name FROM categories_language WHERE categories_language.category_code = categories.code AND categories_language.locale = "'.session('lang').'"),
            categories.name
        ) AS name
    '))->where('status',1)->get();
        return view('admin.product.index',compact('products','categories'));
    }
    public function store (Request $request){
        $request->validate([
           'name'=>'required',
           'price'=>'required|numeric|min:1000|max:1000000000',
           'price_sale'=>'required|numeric|min:1000|max:1000000000|lte:price',
           'number'=>'required|numeric|min:1|max:1000000000',
           'category_code'=>'required',
        ],[
            'price_sale.lte'=>"Price sale must be less than or equal price"
        ]);
        $data = new Product();
        $code = Uuid::uuid4()->toString();
        $data->code = $code;
        $data->name = $request->name;
        $data->price = $request->price;
        $data->price_sale = $request->price_sale;
        $data->number = $request->number;
        $data->category_code = $request->category_code;
        $data->save();
        // add lang
        $productLanguage = new ProductLanguage();
        $productLanguage->product_code = $code;
        $productLanguage->locale = session('lang');
        $productLanguage->name  = $request->name;
        $productLanguage->save();
        return back()->with('success','Product added successfully');
    }
    public function update(Request $request, Product $product){
        $request->validate([
            'name'=>'required',
            'price'=>'required|numeric|min:1000|max:1000000000',
            'price_sale'=>'required|numeric|min:1000|max:1000000000|lte:price',
            'number'=>'required|numeric|min:1|max:1000000000',
            'category_code'=>'required',
        ],[
            'price_sale.lte'=>"Price sale must be less than or equal price"
        ]);
        // update product
        $product->name = $request->name;
        $product->price = $request->price;
        $product->price_sale = $request->price_sale;
        $product->number = $request->number;
        $product->save();
        //cập nhật bản dịch
        $existProductLanguage = ProductLanguage::where('product_code',$product->code)->where('locale',session('lang'))->first();
        if (!is_null($existProductLanguage)){
            $existProductLanguage->name = $request->name;
            $existProductLanguage->save();
        }
        else{
            $productLanguage = new ProductLanguage();
            $productLanguage->product_code = $product->code;
            $productLanguage->locale = session('lang');
            $productLanguage->name  = $request->name;
            $productLanguage->save();
        }
        return back()->with('success','Product updated successfully');
    }
}
