<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentGateway;
use Illuminate\Http\Request;

class PaymentGatewayController extends Controller
{
    public function index(){
        $paymentGateways = PaymentGateway::latest()->paginate(12);
        return view('admin.payment.index',compact('paymentGateways'));
    }
    public function create(){
        return view('admin.payment.add');
    }
    public function store(Request $request){
        $request->validate([
           'title'=>'required',
           'name'=>'required'
        ]);
        $keys = $request->input('keys');
        $values = $request->input('values');
        $title = $request->title;
        $name = $request->name;
        $detail = $request->detail;
        $settings = [];
        foreach ($keys as $index => $key) {
            if (!empty($key)) {
                $settings[$key] = $values[$index];
            }
        }
        $settingsJson = json_encode($settings);
        $gateway = new PaymentGateway();
        $gateway->title = $title;
        $gateway->name = $name;
        $gateway->detail = $detail;
        $gateway->settings = $settingsJson;
        $gateway->save();
        return redirect(route('admin.payment-gateway.index'))->with('success','New Gateway Added Successfully');
    }
    public function edit(PaymentGateway $paymentGateway){
        return view('admin.payment.edit',compact('paymentGateway'));
    }
    public function update(Request $request, PaymentGateway $paymentGateway){
        $request->validate([
            'title'=>'required',
            'name'=>'required'
        ]);
        $keys = $request->input('keys');
        $values = $request->input('values');
        $title = $request->title;
        $name = $request->name;
        $detail = $request->detail;
        $settings = [];
        foreach ($keys as $index => $key) {
            if (!empty($key)) {
                $settings[$key] = $values[$index];
            }
        }
        $settingsJson = json_encode($settings);
        $paymentGateway->title = $title;
        $paymentGateway->name = $name;
        $paymentGateway->detail = $detail;
        $paymentGateway->settings = $settingsJson;
        if (!$request->active){
            $paymentGateway->active = 0;
        }
        $paymentGateway->save();
        return redirect(route('admin.payment-gateway.index'))->with('success','Gateway Updated Successfully');
    }
    public function updateStatus(Request $request){
        $request->validate([
            'id'=>'required',
        ]);
        $paymentGateway = PaymentGateway::findOrFail($request->id);
        $paymentGateway->active = $paymentGateway->active == 1 ? 0 : 1;
        $paymentGateway->save();
        return response()->json(['success' => __('Status has been changed.')]);
    }
    public function remove(Request $request){
        $request->validate([
            'id'=>'required',
        ]);
        $paymentGateway = PaymentGateway::findOrFail($request->id);
        $paymentGateway->delete();
        return redirect(route('admin.payment-gateway.index'))->with('success','Gateway has been removed.');
    }
}
