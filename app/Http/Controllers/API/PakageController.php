<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\LogUsage;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PakageController extends Controller
{
    //
    public function useVip(Request $request){
        $request->validate([
           'view_code'=>'required'
        ]);
        $user = $request->user();
        $view_code = $request->view_code;
        // log lượt vip
        if ($user->usage_count == 0){
            return response()->json([
               'message'=>'Bạn chưa đăng ký gói vip hoặc đã hết lượt sử dụng',
                'current_usage_count'=>$user->usage_count,
               'status'=>false
            ],422);
        }
        LogUsage::insert([
            'user_uuid'=>$user->uuid,
            'view_code'=>$request->view_code,
            'before_usage_count'=>$user->usage_count,
            'amount'=>-1,
            'current_usage_count'=>$user->usage_count -1,
            'used_at'=>Carbon::now(),
            'comment'=>"Sử dụng lượt vip cho view có code $view_code",
            'log_at'=>Carbon::now()
        ]);
        updateUsageCount($user->uuid);
        return response()->json([
            'message'=>'Sử dụng gói vip thành công',
            'current_usage_count'=>$user->usage_count -1,
            'status'=>true
        ]);
    }
}
