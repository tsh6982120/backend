<?php

namespace App\Http\Controllers\API;

use App\Helpers\Numerology;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NumerologyController extends Controller
{
    public function getNum(Request $request){
        $request->validate([
           'fullname' =>'required',
            'birthday'=>'required'
        ]);
        $data = [
            'fullname'=>$request->get('fullname'),
            'birthday'=>$request->get('birthday'),
        ];
        $results = Numerology::generate($data);
        return $results;
    }
}
