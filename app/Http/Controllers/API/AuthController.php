<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class AuthController extends Controller
{
    //
    public function register(Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required|confirmed',
            'password_confirmation'=>'required'
        ]);
        $uuid = Uuid::uuid4()->toString();
        $data = [
            'uuid'=>$uuid,
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
        ];
        $user = User::create($data);
        return response()->json(['data'=>$user,'message'=>'success'],200);
    }
    public function login(Request $request){
        $request->validate([
           'email'=>'required',
           'password'=>'required'
        ]);
        if (Auth::guard('web')->attempt($request->only('email', 'password'))) {
            $user = User::where('email', $request->email)->first();
            $user->tokens()->delete();
            $token = $user->createToken('tsh api')->plainTextToken;
            return response()->json(['token' => $token,'data'=>$user], 200);
        }
        return response()->json(['message' => 'Unauthorized'], 401);
    }
}
